# Written Documents

The majority of this coursework consisted of writing up research, planning and designing my system.

This coursework was worth 20% of my overall grade, in which I had 84/100 Marks.
Along with the other exams, this equated to an A*. 

As per the WJEC (the exam board) specification, there are meant to be 8 written sections within this NEA (Non-Exam Assessment).
However, as I completed this between 2019 and 2021, the course was affected by the Covid-19 pandemic.
Due to this, 2 of the 8 sections were removed for this year, meaning I only had to complete 6 of them.
Below are the list of all the intended sections, indicating which were removed.

- Discussion
- Investigation
- Design
- ~~Prototype~~
- ~~Post-prototype refinement of design~~
- Software Development
- Testing
- Evaluation

## Marking Grid

The [MarkingGrid.docx](MarkingGrid.docx) file in this directory contains the marking critera for the coursework, the "bands" for each
section, and the number of marks I achieved for each section.

## Discussion (5/5 Marks)

This section is the first section for the project, where the problem itself is described, the justification of
a computational approach, and the proposed programming languages and technologies to solve the problem. I also
had created a list of aims and objectives which should be met by the end of the project to consider it a success.
Finally, I had created a short PowerPoint presentation for the problem to present to a few peers to gain some
feedback to improve my aims and objectives.

## Investigation (10/10 Marks)

Within this section I had to investigate the current problem, and solutions that already exist to come up with
a list of features and functionality that my system must have to be considered a success once complete. 
The main purpose of this section was for me to research what solutions already exist to get an idea of what they
are missing, and what they do well. Using this information, I was able to form a success critera to ensure my
system was as successful as possible within the timeframe.

## Design (15/15 Marks)

This section involves me creating the first designs of all my system's pages, the pseudocode for each page, and
the MySQL database schema. 
The database schema has an ERD diagram, a standard notation schema, and a data dictionary for each table.
As this was the first schema, before I touched any code, there were a few changes that I made further down the line.
Designing each individual page within Paint.NET was by far the most tedious and time-consuming process of this
entire project, having to also include screenshots of the page being made in Paint.NET, as well as annotated pages,
because the WJEC are so picky.

## Prototype (8/10 Marks)

*This section was removed from this year's coursework due to the Covid-19 pandemic.*

It was supposed to contain documentation about a "scaled-down" version of my system that carries out all the
main functions of the program, essentially acting as a PoC (Proof of Concept) for the entire system.

## Post-prototype refinement of design (4/5 Marks)

*This section was removed from this year's coursework due to the Covid-19 pandemic.*

This section was supposed to contain feedback from "competent third parties" and used to refine the prototype
based on these suggestions, as well as self evalation in the previous section.

## Software Development (22/25 Marks)

3 giant documents later... it's finally time to code. Details regarding the system itself can be found in the
root directory README.
This document simply contains the latest and up-to-date ERD, standard notation and data dictionary. Each final
page has also been screenshotted and included. The rest of the document contains every single line of code in
every single file, which can already be found in this repo.

## Testing (8/10 Marks)

For this section, I had created a testing plan to manually test each part of my system to ensure it was all
working as intended. I then painfully went through my entire system, taking screenshots of every step to
provide evidence that my tests pass. 

## Evaluation (12/15 Marks)

This is the final section where I evaluate my final system against my aims and objectives that I created in 
the previous section. This involved comparing my system to some existing solutions, suggesting improvements
to my approach, my strengths and weaknesses and a final summary.
