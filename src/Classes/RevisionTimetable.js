/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class RevisionTimetable {
	revisionTimetableId;
	userId;
	timetableJson;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = RevisionTimetable;
