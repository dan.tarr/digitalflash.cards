/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class Flashcard {
	flashcardId;
	title;
	body;
	confidenceLevel;
	userId;
	subjectId;
	createdAt;
	updatedAt;
	views;

	media;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = Flashcard;
