/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class Question {
	questionId;
	title;
	subjectId;
	userId;
	createdAt;
	views;

	answers;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = Question;
