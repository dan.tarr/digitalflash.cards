/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class QuestionAnswer {
	questionAnswerId;
	questionId;
	text;
	correctAnswer;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = QuestionAnswer;
