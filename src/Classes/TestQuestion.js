/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class TestQuestion {
	testQuestionId;
	testId;
	questionId;
	answeredCorrectly;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = TestQuestion;
