/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class FlashcardMedia {
	flashcardMediaId;
	mediaType;
	path;
	size;
	uploadedAt;
	originalFileName;
	flashcardId;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = FlashcardMedia;
