/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class User {
	userId;
	username;
	email;
	password;
	verifiedEmail;
	emailVerificationCode;
	finishedAccountSetup;
	twoFactorEnabled;
	twoFactorSecret;
	twoFactorBackupCodes;
	lastDataDownload;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = User;
