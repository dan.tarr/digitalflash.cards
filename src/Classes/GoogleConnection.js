/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class GoogleConnection {
	googleConnectionId;
	userId;
	googleId;
	googleEmail;
	accessToken;
	refreshToken;
	expiresAt;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = GoogleConnection;
