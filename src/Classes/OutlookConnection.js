/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class OutlookConnection {
	outlookConnectionId;
	userId;
	outlookId;
	outlookDisplayName;
	accessToken;
	refreshToken;
	expiresAt;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = OutlookConnection;
