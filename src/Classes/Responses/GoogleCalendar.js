/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class GoogleCalendar {
	kind;
	etag;
	id;
	summary;
	timeZone;
	conferenceProperties;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = GoogleCalendar
