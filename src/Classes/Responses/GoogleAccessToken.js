/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// noinspection JSUnresolvedVariable
class GoogleAccessToken {
	accessToken;
	refreshToken;
	expiresIn;
	scope;
	tokenType;
	idToken;

	constructor(data) {
		this.accessToken = data.access_token;
		this.refreshToken = data.refresh_token;
		this.expiresIn = data.expires_in;
		this.scope = data.scope;
		this.tokenType = data.token_type;
		this.idToken = data.id_token;
	}
}

module.exports = GoogleAccessToken;
