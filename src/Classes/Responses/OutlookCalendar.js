/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class OutlookCalendar {
	id;
	name;
	color;
	hexColor;
	isDefaultCalendar;
	changeKey;
	canShare;
	canViewPrivateItems;
	canEdit;
	allowedOnlineMeetingProviders;
	defaultOnlineMeetingProvider;
	isTallyingResponses;
	isRemovable;
	owner;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = OutlookCalendar;
