/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// noinspection JSUnresolvedVariable
class GoogleUser {
	id;
	email;
	verifiedEmail;
	name;
	givenName;
	familyName;
	picture;
	locale;

	constructor(data) {
		this.id = data.id;
		this.email = data.email;
		this.verifiedEmail = data.verified_email;
		this.name = data.name;
		this.givenName = data.given_name;
		this.familyName = data.family_name;
		this.picture = data.picture;
		this.locale = data.locale;
	}
}

module.exports = GoogleUser;
