/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// noinspection JSUnresolvedVariable
class OutlookAccessToken {
	tokenType;
	scope;
	expiresIn;
	accessToken;
	refreshToken;

	constructor(data) {
		this.tokenType = data.token_type;
		this.scope = data.scope;
		this.expiresIn = data.expires_in;
		this.accessToken = data.access_token;
		this.refreshToken = data.refresh_token;
	}
}

module.exports = OutlookAccessToken
