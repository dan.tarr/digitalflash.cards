/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class OutlookUser {
	displayName;
	surname;
	givenName;
	id;
	userPrincipalName;
	businessPhones;
	jobTitle;
	mail;
	mobilePhone;
	officeLocation;
	preferredLanguage;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = OutlookUser;
