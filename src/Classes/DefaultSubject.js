/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class DefaultSubject {
	defaultSubjectId;
	name;
	yearGroup;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = DefaultSubject;
