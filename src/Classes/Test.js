/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class Test {
	testId;
	takenAt;
	complete;
	userId;

	questions;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = Test;
