/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

class UserSubject {
	userSubjectId;
	name;
	colour;
	userId;

	constructor(data) {
		Object.assign(this, data);
	}
}

module.exports = UserSubject;
