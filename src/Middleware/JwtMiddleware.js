/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Helpers
const jwtHelper = require("../Helpers/JwtHelper");

module.exports = async (req, res, next) => {
	try {
		// Gets the token cookie from the request
		let tokenCookie = req.cookies["digitalflashcards.token"];

		// If no token was provided, the user is logged out
		if (!tokenCookie) {
			req.session = {
				authState: "LOGGED_OUT"
			};

			return next();
		}

		// Set the requests session data to be the decoded payload of the cookie
		req.session = await jwtHelper.verifyAsync(tokenCookie);

		return next();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		// If an error is thrown while verifying the JWT, the JWT was invalid, so the user is logged out
		req.session = {
			authState: "LOGGED_OUT"
		};

		return next();
	}
};
