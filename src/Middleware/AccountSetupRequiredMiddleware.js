/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

module.exports = async (req, res, next) => {
	// If the user is logged in but requires account setup, and they aren't
	// already on the account setup page and the request isn't a request for an
	// asset, or API endpoint redirect the request to the account setup page
	if (req.session.authState === "REQUIRES_ACCOUNT_SETUP" &&
		req.path !== "/accountsetup" &&
		!req.path.startsWith("/assets") &&
		!req.path.startsWith("/api"))
		return res.redirect("/accountsetup");

	return next();
};
