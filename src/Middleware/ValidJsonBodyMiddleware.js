/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const bodyParser = require("body-parser");

module.exports = (req, res, next) => {
	// Set a limit for the maxiumum characters allowed in a single body
	bodyParser.json({
		limit: 10000
	})(req, res, (err) => {
		if (err) {
			// If the error was provided because the body is too large
			if (err.type === "entity.too.large") {
				return res.status(413).send({
					error: true,
					displayMessage: "The provided body is too large",
					code: "MALFORMED_BODY"
				});
			}

			// If the error wasnt entity.too.large, the body was malformed and unable to be parsed
			return res.status(400).send({
				error: true,
				displayMessage: "The provided body is malformed",
				code: "MALFORMED_BODY"
			});
		}

		return next();
	});
};
