/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const jwt = require("jsonwebtoken");
const fs = require("fs");
const ms = require("ms");

// Variables
const secureSession = process.env.SECURE_SESSION === "true";
const publicKey = fs.readFileSync(process.env.JWT_PUBLIC, "utf-8");
const privateKey = fs.readFileSync(process.env.JWT_PRIVATE, "utf-8");

/**
 * @param {Object} payload - The payload object for the JWT token
 * @param {string} expires - The time until the token expires
 * @returns {Promise<string>} - The token string
 */
module.exports.signAsync = (payload, expires) => {
	return new Promise((resolve, reject) => {
		// Create an object for the object to use for the signingOptions of the JWT
		let signingOptions = {
			issuer: "digitalflash.cards",
			audience: "https://digitalflash.cards",
			expiresIn: expires,
			algorithm: "RS256"
		};

		// Create a JWT and sign it with the private key
		jwt.sign(payload, privateKey, signingOptions, function (err, token) {
			if (err)
				return reject(err);

			return resolve(token);
		});
	});
};

/**
 * @param {string} token - The token string to verify
 * @returns {Promise<Object>} - The payload of the JWT token
 */
module.exports.verifyAsync = (token) => {
	return new Promise((resolve, reject) => {
		// Create an object containing the options for verifying the JWT
		let verifyOptions = {
			issuer: "digitalflash.cards",
			audience: "https://digitalflash.cards",
			algorithms: ["RS256"]
		};

		// Verify the JWT was created by the the private key using the public key
		jwt.verify(token, publicKey, verifyOptions, function (err, decoded) {
			if (err)
				return reject(err);

			return resolve(decoded);
		});
	});
};

/**
 * @param {Response} res - The response for the request
 * @param {string} token - The token to set
 * @param {string} expires - The time until the cookie expires
 * @returns {void}
 */
module.exports.setJwtCookie = (res, token, expires) => {
	// Sets the token cookie, used for authentication
	res.cookie("digitalflashcards.token", token, {
		httpOnly: true,
		secure: secureSession,
		maxAge: ms(expires)
	});
};

/**
 * @param {Response} res - The reponse for the request
 * @returns {void}
 */
module.exports.removeJwtCookie = (res) => {
	// Removes the token cookie, when the user signs out
	res.clearCookie("digitalflashcards.token", {
		httpOnly: true,
		secure: secureSession
	});
};
