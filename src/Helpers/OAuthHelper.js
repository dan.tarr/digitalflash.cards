/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const axios = require("axios");
const qs = require("querystring");

// Classes
const GoogleAccessToken = require("../Classes/Responses/GoogleAccessToken");
const OutlookAccessToken = require("../Classes/Responses/OutlookAccessToken");

// Variables
const googleClientId = process.env.GOOGLE_OAUTH_CLIENT_ID;
const googleClientSecret = process.env.GOOGLE_OAUTH_CLIENT_SECRET;
const googleRedirectUri = process.env.GOOGLE_OAUTH_REDIRECT_URI;
const outlookClientId = process.env.OUTLOOK_OAUTH_CLIENT_ID;
const outlookClientSecret = process.env.OUTLOOK_OAUTH_CLIENT_SECRET;
const outlookRedirectUri = process.env.OUTLOOK_OAUTH_REDIRECT_URI;

/**
 * @param {string} code - The OAuth code from the Google callback
 * @return {Promise<GoogleAccessToken>} - An instance of GoogleAccessToken
 */
module.exports.getGoogleAccessTokenAsync = async (code) => {
	try {
		// Convert the OAuth code to an access token through Google's OAuth API
		/* eslint-disable camelcase */
		let googleResponse = (await axios.post("https://oauth2.googleapis.com/token", qs.stringify({
			code,
			client_id: googleClientId,
			client_secret: googleClientSecret,
			redirect_uri: googleRedirectUri,
			grant_type: "authorization_code"
		}))).data;
		/* eslint-enable camelcase */

		return Promise.resolve(new GoogleAccessToken(googleResponse));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} code - The OAuth code from the Outlook callback
 * @return {Promise<OutlookAccessToken>} - An instance of OutlookAccessToken
 */
module.exports.getOutlookAccessTokenAsync = async (code) => {
	try {
		// Convert the OAuth code to an access token through Outlook's OAuth API
		let url = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
		/* eslint-disable camelcase */
		let outlookResponse = (await axios.post(url, qs.stringify({
			code,
			client_id: outlookClientId,
			client_secret: outlookClientSecret,
			redirect_uri: outlookRedirectUri,
			grant_type: "authorization_code"
		}))).data;
		/* eslint-enable camelcase */

		return Promise.resolve(new OutlookAccessToken(outlookResponse));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} refreshToken - The Google refresh token
 * @return {Promise<GoogleAccessToken>} - An instance of GoogleAccessToken
 */
module.exports.refreshGoogleToken = async (refreshToken) => {
	try {
		// Refresh the google access token using the refresh token
		let url = "https://oauth2.googleapis.com/token";
		/* eslint-disable camelcase */
		let googleResponse = (await axios.post(url, qs.stringify({
			client_id: googleClientId,
			client_secret: googleClientSecret,
			redirect_uri: googleRedirectUri,
			grant_type: "refresh_token",
			refresh_token: refreshToken
		}))).data;
		/* eslint-enable camelcase */

		return Promise.resolve(new GoogleAccessToken(googleResponse));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} refreshToken - The Outlook refresh token
 * @return {Promise<OutlookAccessToken>} - An instance of OutlookAccessToken
 */
module.exports.refreshOutlookToken = async (refreshToken) => {
	try {
		// Refresh the outlook access token using the refresh token
		let url = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
		/* eslint-disable camelcase */
		let outlookResponse = (await axios.post(url, qs.stringify({
			client_id: outlookClientId,
			client_secret: outlookClientSecret,
			redirect_uri: outlookRedirectUri,
			grant_type: "refresh_token",
			refresh_token: refreshToken
		}))).data;
		/* eslint-enable camelcase */

		return Promise.resolve(new OutlookAccessToken(outlookResponse));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
