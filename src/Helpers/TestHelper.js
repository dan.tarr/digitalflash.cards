/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Repositories
const userSubjectsRepository = require("../Repositories/UserSubjectsRepository");

module.exports.getWeakestStrongestSubjectsAsync = async (tests) => {
	try {
		// Creates 2 objects for the correct and incorrect subjects
		let correctSubjectStats = {};
		let incorrectSubjectStats = {};

		tests.forEach(test => {
			// Get the number of correct and incorrect answers for each subject ID
			test.questions.forEach(question => {
				if (!correctSubjectStats[question.subjectId])
					correctSubjectStats[question.subjectId] = 0;

				if (!incorrectSubjectStats[question.subjectId])
					incorrectSubjectStats[question.subjectId] = 0;

				if (question.answeredCorrectly === 1)
					correctSubjectStats[question.subjectId] += 1;

				if (question.answeredCorrectly === 0)
					incorrectSubjectStats[question.subjectId] += 1;
			});
		});

		// Return an array of counts for each subject ID
		let correctSubjectCount = Object.keys(correctSubjectStats).map(key => {
			return correctSubjectStats[key];
		});
		let incorrectSubjectCount = Object.keys(incorrectSubjectStats).map(key => {
			return incorrectSubjectStats[key];
		});

		// Get the max count for correct and incorrect answers
		let maxCorrectSubjectCount = correctSubjectCount.reduce((prev, current) =>
			(prev > current) ? prev : current);
		let maxIncorrectSubjectCount = incorrectSubjectCount.reduce((prev, current) =>
			(prev > current) ? prev : current);

		// Creates an array for the IDs of the strongest and weakest subjects
		let strongestSubjectIds = [];
		let weakestSubjectIds = [];

		// Remove all keys that do not have the value of the max, calculated above
		Object.keys(correctSubjectStats).forEach(key => {
			if (correctSubjectStats[key] === maxCorrectSubjectCount)
				strongestSubjectIds.push(key);
		});
		Object.keys(incorrectSubjectStats).forEach(key => {
			if (incorrectSubjectStats[key] !== maxIncorrectSubjectCount)
				weakestSubjectIds.push(key);
		});

		// Gets the subjects and joins their names with commas
		let strongestSubjects = (await userSubjectsRepository
			.getUserSubjectsByIdsAsync(strongestSubjectIds))
			.map(x => {
				return x.name;
			})
			.join(", ");
		let weakestSubjects = (await userSubjectsRepository
			.getUserSubjectsByIdsAsync(weakestSubjectIds))
			.map(x => {
				return x.name;
			})
			.join(", ");

		return Promise.resolve([
			strongestSubjects,
			weakestSubjects
		]);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
