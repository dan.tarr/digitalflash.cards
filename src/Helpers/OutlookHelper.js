/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const axios = require("axios");

// Classes
const OutlookUser = require("../Classes/Responses/OutlookUser");
const OutlookCalendar = require("../Classes/Responses/OutlookCalendar");

// Variables
const baseUrl = "https://graph.microsoft.com/v1.0";

/**
 * @param {string} accessToken - The Outlook access token
 * @return {Promise<OutlookUser>} - An instance of OutlookUser
 */
module.exports.getUserInfoAsync = async (accessToken) => {
	try {
		// Gets the user information from Outlook
		let userData = (await axios.get(`${baseUrl}/me/`, {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		})).data;

		return Promise.resolve(new OutlookUser(userData));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} accessToken - The users Outlook access token
 * @return {Promise<OutlookCalendar>} - An instance of OutlookCalendar
 */
module.exports.createCalendarAsync = async (accessToken) => {
	try {
		// API request to Microsoft to create a new calendar
		let calendarResponse = (await axios.post(`${baseUrl}/me/calendars`, {
			name: "digitalflash.cards Revision Timetable",
			color: "-1"
		}, {
			headers: {
				Authorization: `Bearer ${accessToken}`,
				"Content-Type": "application/json"
			}
		})).data;

		return Promise.resolve(new OutlookCalendar(calendarResponse));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} accessToken - The Outlook access token
 * @param {Object[]} requests - The array of requests to make
 * @return {Promise<Object>} - The response from Outlook
 */
module.exports.batchRequestAsync = async (accessToken, requests) => {
	try {
		// API request to Microsoft to execute a batch of requests
		let response = (await axios.post(`${baseUrl}/$batch`, {
			requests
		}, {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		})).data;

		return Promise.resolve(response);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
