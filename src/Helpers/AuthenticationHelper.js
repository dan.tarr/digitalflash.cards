/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const bcrypt = require("bcrypt");
const {authenticator} = require("otplib");
const qrcode = require("qrcode");

// Helpers
const genericHelper = require("../Helpers/GenericHelper");

// Variables
const saltRounds = 10;

/**
 * @param {string} password - User provided password
 * @return {boolean} - If the password passes the validation
 */
module.exports.validatePassword = (password) => {
	let allValid = true;

	if (password.length < 8)
		allValid = false;

	if (!/\d/.test(password))
		allValid = false;

	if (!/[a-z]/.test(password))
		allValid = false;

	if (!/[A-Z]/.test(password))
		allValid = false;

	return allValid;
};

/**
 * @param {string} password - The password to hash
 * @returns {Promise<string>} - Hashed password string
 */
module.exports.hashPasswordAsync = (password) => {
	return bcrypt.hash(password, saltRounds);
};

/**
 * @param {string} password - The plain text password entered by the user
 * @param {string} hash - The hashed password from db
 * @return {Promise<boolean>} - If the password is correct
 */
module.exports.compareBcryptPasswordAsync = (password, hash) => {
	return bcrypt.compare(password, hash);
};

/**
 * @param {string} username - The username to create the authenication secret for
 * @returns {Promise<{
 *     secret: string,
 *     url: string
 * }>} - An object containing the secret and QR code url
 */
module.exports.createAuthenticatorSecretAsync = async (username) => {
	try {
		// Generates a secret for 2FA
		let secret = authenticator.generateSecret();
		// Creates a URI for the 2FA, used the generate a QR code
		let otpauth = authenticator.keyuri(username, "digitalflash.cards", secret);
		// Creates a base64 data:image URL for the user to scan
		let qrCodeUrl = await qrcode.toDataURL(otpauth);

		return Promise.resolve({
			secret,
			url: qrCodeUrl
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} code - The code entered by the user
 * @param {string} secret - The secret used to generate the code
 * @returns {boolean} - A boolean to indicate if the entered code was correct
 */
module.exports.checkAuthenticatorCode = (code, secret) => {
	return authenticator.check(code, secret);
};

/**
 * @param {number} amount - The number of backup codes to generate
 * @returns {string[]} - An array of the generated backup codes
 */
module.exports.generateBackupCodes = (amount) => {
	let arr = [];
	// Loops the number of random backup codes needed and generates a random string
	for (let i = 0; i < amount; i++)
		arr.push(genericHelper.getRandomString(8));

	return arr;
};
