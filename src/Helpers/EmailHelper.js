/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const nodemailer = require("nodemailer");
// Creates a new email transport, used to send emails
const emailTransporter = nodemailer.createTransport({
	host: process.env.EMAIL_HOST,
	port: process.env.EMAIL_PORT,
	secure: true,
	auth: {
		user: process.env.EMAIL_USER,
		pass: process.env.EMAIL_PASSWORD
	}
});

/**
 * @param {string} email - The email to send to
 * @param {string} code - The verification code to include in the email
 * @param {string} username - The username of the user
 * @return {Promise<SentMessageInfo>} - An instance of SentMessageInfo
 */
module.exports.sendVerificationEmailAsync = async (email, code, username) => {
	try {
		// Sends an email to the provided email address
		let emailInfo = await emailTransporter.sendMail({
			from: "digitalflash.cards <noreply@digitalflash.cards>",
			to: email,
			subject: "digitalflash.cards email verification",
			html: `<h1>Hello, ${username}</h1><br><h3>Your code is <code>${code}</code></h3>`
		});

		return Promise.resolve(emailInfo);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} email - The email to send to
 * @param {string} userData - The stringified user data
 * @return {Promise<SentMessageInfo>} - An instance of SentMessageInfo
 */
module.exports.sendDataDownloadEmailAsync = async (email, userData) => {
	try {
		// Sends an email to the provided email address
		let emailInfo = await emailTransporter.sendMail({
			from: "digitalflash.cards <noreply@digitalflash.cards>",
			to: email,
			subject: "digitalflash.cards Data Download Request",
			html: "Your requested data download has been attached. You may request " +
				"another one in 2 weeks time.<br>Thank you for using digitalflash.cards",
			attachments: [
				{
					filename: "data.json",
					content: userData,
					contentType: "application/json"
				}
			]
		});

		return Promise.resolve(emailInfo);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} email - The email to send to
 * @param {string} username - The users username
 * @return {Promise<SentMessageInfo>} - An instance of SentMessageInfo
 */
module.exports.sendAccountDeletionEmailAsync = async (email, username) => {
	try {
		// Sends an email to the provided email address
		let emailInfo = await emailTransporter.sendMail({
			from: "digitalflash.cards <noreply@digitalflash.cards>",
			to: email,
			subject: "digitalflash.cards Account Deletion",
			html: `Hello, ${username}.<br>
				At your request, we have deleted your account and all its associated data.<br>
				Thank you for using digitalflash.cards`
		});

		return Promise.resolve(emailInfo);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
