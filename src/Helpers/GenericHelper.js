/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

/**
 * @param {number} length - The number of characters to generate
 * @returns {string} - The randomly generated string
 */
module.exports.getRandomString = (length) => {
	// The possible characters to choose when generating the random string
	const chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	let result = "";
	// Loop the number of characters required
	for (let i = length; i > 0; --i)
		result += chars[Math.floor(Math.random() * chars.length)];

	return result;
};

/**
 * @param {number} bytes - The bytes to format
 * @param {number} decimals - The number of decimal places to round to
 * @return {string} - The formatted string
 */
module.exports.formatBytes = (bytes, decimals) => {
	// If the number of bytes is 0, return "0 Bytes"
	if (bytes === 0) return "0 Bytes";
	let k = 1024;
	// Determine the number of decimal places to use
	let dm = decimals <= 0 ? 0 : decimals || 2;
	// An array of the available size units
	let sizes = ["Bytes", "KB", "MB", "GB"];
	// Determine which of the sizes best suits the number of bytes
	let i = Math.floor(Math.log(bytes) / Math.log(k));
	// Return the formatted size
	return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
};

/**
 * @param {*[]} array - The array to chunk
 * @param {number} value - The number of elements per chunk
 * @return {[][]} - A 2D array
 */
module.exports.chunkArray = (array, value) => {
	// Chunk the array into an array of arrays with a max count of value
	return Array.from(Array(Math.ceil(array.length / value)),
		(_, i) => array.slice(i * value, i * value + value));
};

/**
 * @param {{
 *     weight: number,
 *     item: string
 * }[]} options - The array of options of item and weights
 * @returns {string} - The selected item
 */
module.exports.getRandomWeightedItem = (options) => {
	let weights = [];
	let i;

	// Loop through each of the options and add a weight value to the weights array
	for (i = 0; i < options.length; i++)
		weights[i] = options[i].weight + (weights[i - 1] || 0);

	// Select a random number based on how many weights there are
	let random = Math.random() * weights[weights.length - 1];

	// Loop through all the weights until we reach a value greater than the random value generated
	for (i = 0; i < weights.length; i++) {
		if (weights[i] > random)
			break;
	}

	// Return the selected item
	return options[i].item;
};

module.exports.pickTextColour = (backgroundColour) => {
	// Strip the leading #, if the colour contains it
	let colour = (backgroundColour.charAt(0) === "#") ?
		backgroundColour.substring(1, 7) :
		backgroundColour;

	// Parses the hex colour into R, G and B components
	let r = parseInt(colour.substring(0, 2), 16);
	let g = parseInt(colour.substring(2, 4), 16);
	let b = parseInt(colour.substring(4, 6), 16);

	// Determines if the text colour should be black or white based on the background colour
	return (((r * 0.299) + (g * 0.587) + (b * 0.114)) > 172) ?
		"000000" : "ffffff";
};

/**
 * @param {Date} date - The date to start at
 * @param {number} dayOfWeek - The day of the week to find the next date for
 * @return {Date} - The next date
 */
module.exports.getNextDayOfWeek = (date, dayOfWeek) => {
	// Create a new date for the result
	let resultDate = new Date(date.getTime());
	// Set the date to the number of days until the next monday
	resultDate.setDate(date.getDate() + (7 + dayOfWeek - date.getDay()) % 7);

	// Return the date
	return resultDate;
};
