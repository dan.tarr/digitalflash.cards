/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const axios = require("axios");
const querystring = require("querystring");

// Variables
const reCaptchaSecretKey = process.env.RECAPTCHA_SECRET;

/**
 * @param {string} token - The response token from the client
 * @return {Promise<{boolean}>} - If the captcha verification was successful or not
 */
module.exports.validateCaptchaAsync = async (token) => {
	try {
		// Makes an API request to Google to verify the reCaptcha code
		let response = (await axios.post("https://www.google.com/recaptcha/api/siteverify",
			querystring.stringify({
				secret: reCaptchaSecretKey,
				response: token
			}), {
				headers: {
					"Content-Type": "application/x-www-form-urlencoded"
				}
			})).data;

		// If the response states the captcha token was successful
		if (response.success)
			return Promise.resolve(true);

		return Promise.resolve(false);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
