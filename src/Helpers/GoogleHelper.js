/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const axios = require("axios");

// Classes
const GoogleUser = require("../Classes/Responses/GoogleUser");
const GoogleCalendar = require("../Classes/Responses/GoogleCalendar");

// Variables
const baseUrl = "https://www.googleapis.com";

/**
 * @param {string} accessToken - The Google access token
 * @return {Promise<GoogleUser>} - An instance of GoogleUser
 */
module.exports.getUserInfoAsync = async (accessToken) => {
	try {
		// Make an API request to get the user information
		let url = `${baseUrl}/oauth2/v1/userinfo?access_token=${accessToken}`;
		let userData = (await axios.get(url)).data;

		return Promise.resolve(new GoogleUser(userData));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} token - The Google access token
 * @return {Promise<void>} - Nothing
 */
module.exports.revokeTokenAsync = async (token) => {
	try {
		// Make a POST request to revoke the token from Google
		await axios.post(`https://oauth2.googleapis.com/revoke?token=${token}`);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} accessToken - The Google access token
 * @return {Promise<GoogleCalendar>} - An instance of GoogleCalendar
 */
module.exports.createCalendarAsync = async (accessToken) => {
	try {
		// Make an API request to create a new calendar for the user
		let calendarResponse = (await axios.post(`${baseUrl}/calendar/v3/calendars`, {
			summary: "digitalflash.cards Revision Timetable"
		}, {
			headers: {
				Authorization: `Bearer ${accessToken}`,
				"Content-Type": "application/json"
			}
		})).data;

		return Promise.resolve(new GoogleCalendar(calendarResponse));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} accessToken - The Google access token
 * @param {string} calendarId - The ID of the Google calendar
 * @param {Object} body - The body of the request
 * @return {Promise<void>} - Nothing
 */
module.exports.createCalendarEventAsync = async (accessToken, calendarId, body) => {
	try {
		// Make an API request to create a new event on a specific calendar
		let eventResponse = (await axios.post(`${baseUrl}/calendar/v3/calendars/${calendarId}/events`,
			body, {
				headers: {
					Authorization: `Bearer ${accessToken}`
				}
			})).data;

		return Promise.resolve(eventResponse);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
