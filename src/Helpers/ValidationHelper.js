/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

/**
 * @param {string | null} value - The value to check
 * @returns {boolean} - Indicates if the check passed or not
 */
module.exports.presenceCheck = (value) => {
	// Checks if the value is null or an empty string
	return !(value === null || value === "");
};

/**
 * @param {string} value - The value to check
 * @param {number} max - The maxiumum length of this string
 * @returns {boolean} - Indicates if the check passed or not
 */
module.exports.lessThanLengthCheck = (value, max) => {
	/*
		Checks if the length of the provided string is less than or equal the max length provided
	*/
	return value.length <= max;
};

/**
 * @param {*} value - The value to check
 * @param {*[]} values - An array of valid values
 * @returns {boolean} - Indicates if the check passed or not
 */
module.exports.lookupCheck = (value, values) => {
	// Checks if the value provided is in the values array provided
	return values.includes(value);
};
