/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const mysql = require("mysql");
/**
 * @type {Connection}
 */
let connection;

module.exports.connectAsync = async () => {
	connection = mysql.createConnection({
		host: process.env.SQL_HOST,
		port: process.env.SQL_PORT,
		user: process.env.SQL_USERNAME,
		password: process.env.SQL_PASSWORD,
		database: process.env.SQL_DATABASE
	});

	connection.connect(function (err) {
		if (err) {
			console.error(err);
			process.exit(1);
		}

		console.log("Connected to MySQL");
		return Promise.resolve();
	});
};

/*
	connection.beginTransaction uses a traditional callback to complete
	To keep my code cleaner and to avoid "callback hell", create a function to
	return a Promise so that I can use await
 */
module.exports.beginTransactionAsync = async () => {
	connection.beginTransaction(function (err) {
		if (err)
			return Promise.reject(err);

		return Promise.resolve();
	});
};

/*
	connection.commit uses a traditional callback to complete
	To keep my code cleaner and to avoid "callback hell", create a function to
	return a Promise so that I can use await
 */
module.exports.commitAsync = async () => {
	connection.commit(function (err) {
		if (err)
			return Promise.reject(err);

		return Promise.resolve();
	});
};

/*
	connection.beginTransaction uses a traditional callback to complete
	To keep my code cleaner and to avoid "callback hell", create a function to
	return a Promise so that I can use await
 */
module.exports.rollbackAsync = async () => {
	connection.rollback(function (err) {
		if (err)
			return Promise.reject(err);

		return Promise.resolve();
	});
};

/*
	connection.query uses a traditional callback to complete
	To keep my code cleaner and to avoid "callback hell", create a function to
	return a Promise so that I can use await
 */
/**
 * @param {string} query - The SQL query
 * @param {*[] | Object} [values] - The values for the SQL query parameters
 * @return {Promise<RowDataPacket[]>} - An array of RowDataPackets
 */
module.exports.queryAsync = (query, values) => {
	return new Promise((resolve, reject) => {
		connection.query(query, values, function (err, results) {
			if (err)
				return reject(err);

			return resolve(results);
		});
	});
};
