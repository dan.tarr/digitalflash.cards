/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

$(function () {
	$("#changeUsernameConfirm").click(async function () {
		let usernameElement = $("#changeUsername");

		try {
			let username = usernameElement.val();

			if (!window.utils.validation.presenceCheck(username))
				return window.utils.displayError("changeUsernameError", "Username cannot be blank");

			if (username.length > 32) {
				return window.utils.displayError("changeUsernameError", "Username must " +
					"be less than 32 characters");
			}

			// Ensure the username only contains English characters and numbers
			if (!/^[A-Za-z][A-Za-z0-9]*$/.test(username)) {
				return window.utils.displayError("changeUsernameError", "Username must " +
					"only contain English alphanumeric characters");
			}

			window.utils.displayLoading("changeUsernameConfirm");
			window.utils.disableTextInputElements([usernameElement]);
			window.utils.clearErrors(["changeUsernameError", "changeUsernameSuccess"]);

			await window.apiRequest.putJSON("/api/user/username", {
				username
			});

			window.utils.hideLoading("changeUsernameConfirm");
			window.utils.displayError("changeUsernameSuccess", "Successfully changed username");
			window.utils.enableTextInputElements([usernameElement]);

			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading("changeUsernameConfirm");
			window.utils.displayError("changeUsernameError", err.displayMessage);
			window.utils.enableTextInputElements([usernameElement]);
			return null;
		}
	});
});
