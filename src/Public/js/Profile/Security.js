/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

/**
 * @param {string} password - The new password
 * @param {string} confirmPassword - The confirmed password
 * @return {{
 * 		checks: [],
 * 		allValid: boolean
 * }} - Object indicating if the password is valid/what is not valid
 */
function validatePassword(password, confirmPassword) {
	let checks = [];
	let allValid = true;

	if (password.length < 8) {
		checks.push({
			passed: false,
			text: "Password must be at least 8 characters long"
		});
		allValid = false;
	} else {
		checks.push({
			passed: true,
			text: "Password must be at least 8 characters long"
		});
	}

	if (!/\d/.test(password)) {
		checks.push({
			passed: false,
			text: "Password must contain at least 1 number"
		});
		allValid = false;
	} else {
		checks.push({
			passed: true,
			text: "Password must contain at least 1 number"
		});
	}

	if (!/[a-z]/.test(password)) {
		checks.push({
			passed: false,
			text: "Password must contain at least 1 lower case letter"
		});
		allValid = false;
	} else {
		checks.push({
			passed: true,
			text: "Password must contain at least 1 lower case letter"
		});
	}

	if (!/[A-Z]/.test(password)) {
		checks.push({
			passed: false,
			text: "Password must contain at least 1 upper case letter"
		});
		allValid = false;
	} else {
		checks.push({
			passed: true,
			text: "Password must contain at least 1 upper case letter"
		});
	}

	if ((password === confirmPassword) && allValid) {
		checks.push({
			passed: true,
			text: "Passwords must match"
		});
	} else {
		checks.push({
			passed: false,
			text: "Passwords must match"
		});
		allValid = false;
	}

	return {
		checks,
		allValid
	};
}

$(function () {
	$("#changePasswordConfirm").click(async function () {
		// Get user input element
		let oldPasswordElement = $("#oldPassword");
		let newPasswordElement = $("#newPassword");
		let confirmPasswordElement = $("#confirmPassword");

		try {
			let oldPassword = oldPasswordElement.val();
			let newPassword = newPasswordElement.val();
			let confirmPassword = confirmPasswordElement.val();

			// Validation checks
			if (!window.utils.validation.presenceCheck(oldPassword))
				return window.utils.displayError("changePasswordError", "Old password cannot be blank");

			if (!window.utils.validation.presenceCheck(newPassword))
				return window.utils.displayError("changePasswordError", "New password cannot be blank");

			if (!window.utils.validation.presenceCheck(confirmPassword))
				return window.utils.displayError("changePasswordError", "Confirm password cannot be blank");

			window.utils.clearErrors(["changePasswordError", "changePasswordSuccess"]);

			// Check the password is valid
			let passwordValidation = validatePassword(newPassword, confirmPassword);

			if (!passwordValidation.allValid) {
				let html = "";

				// Display password validation errors
				passwordValidation.checks.forEach(check => {
					if (check.passed)
						html += `<p class="text-green mb-1"><i class="fal fa-check mr-2"></i>${check.text}</p>`;
					else
						html += `<p class="text-red mb-1"><i class="fal fa-times mr-1"></i> ${check.text}</p>`;
				});

				return $("#changePasswordError").html(html);
			}

			window.utils.displayLoading("changePasswordConfirm");
			window.utils.disableTextInputElements([oldPasswordElement,
				newPasswordElement, confirmPasswordElement]);

			// POST request to change the users password
			await window.apiRequest.postJSON("/api/user/changepassword", {
				oldPassword,
				newPassword,
				confirmPassword
			});

			// Reset the input states
			window.utils.hideLoading("changePasswordConfirm");
			window.utils.displayError("changePasswordSuccess", "Successfully changed password");
			window.utils.enableTextInputElements([oldPasswordElement,
				newPasswordElement, confirmPasswordElement]);
			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading("changePasswordConfirm");
			window.utils.displayError("changePasswordError", err.displayMessage);
			window.utils.enableTextInputElements([oldPasswordElement,
				newPasswordElement, confirmPasswordElement]);
			return null;
		}
	});

	$("#enableTwoFactor").click(async function () {
		try {
			window.utils.displayLoading("enableTwoFactor");
			window.utils.clearErrors(["twoFactorError"]);

			// POST request to enable two factor and return a secret key
			/**
			 * @type {{
			 *     error: boolean,
			 *     code: string,
			 *     url: string,
			 *     secret: string
			 * }}
			 */
			let twoFactorResponse = await window.apiRequest.postJSON("/api/twofactor/enable");

			// Open the 2FA modal and add the relevant information
			window.utils.hideLoading("enableTwoFactor");
			$("#twoFactorQr").attr("src", twoFactorResponse.url);
			$("#twoFactorKey").text(twoFactorResponse.secret);
			$("#enableTwoFactorModal").modal("show");

			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading("enableTwoFactor");
			window.utils.displayError("twoFactorError", err.displayMessage);
			return null;
		}
	});

	$("#confirmTwoFactor").click(async function () {
		let twoFactorCodeElement = $("#confirmTwoFactorCode");

		try {
			let code = twoFactorCodeElement.val();

			// Validation checks
			if (!window.utils.validation.presenceCheck(code))
				return window.utils.displayError("confirmTwoFactorError", "Code cannot be blank");

			if (code.length !== 6)
				return window.utils.displayError("confirmTwoFactorError", "Code must be 6 digits");

			window.utils.clearErrors(["confirmTwoFactorError"]);
			window.utils.displayLoading("confirmTwoFactor");

			// POST request to confirm the code is corret and enable 2FA
			await window.apiRequest.postJSON("/api/twofactor/confirm", {
				code
			});

			// Refresh the page and display a success toast
			window.location = "?notify=Successfully enabled two-factor authentication";
			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading("confirmTwoFactor");
			window.utils.displayError("confirmTwoFactorError", err.displayMessage);
			return null;
		}
	});

	// Open the disable 2FA modal when the button is clicked
	$("#disableTwoFactor").click(function () {
		$("#disableTwoFactorModal").modal("show");
	});

	$("#disableTwoFactorConfirm").click(async function () {
		let twoFactorCodeElement = $("#disableTwoFactorCode");

		try {
			let code = twoFactorCodeElement.val();

			// Validation checks
			if (!window.utils.validation.presenceCheck(code))
				return window.utils.displayError("disableTwoFactorError", "Code cannot be blank");

			if (code.length !== 6)
				return window.utils.displayError("disableTwoFactorError", "Code must be 6 digits");

			window.utils.clearErrors(["disableTwoFactorError"]);
			window.utils.displayLoading("disableTwoFactorConfirm");

			// POST request to check the code and disable 2FA
			await window.apiRequest.postJSON("/api/twofactor/disable", {
				code
			});

			window.location = "?notify=Successfully disabled two-factor authentication";
			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading("disableTwoFactorConfirm");
			window.utils.displayError("disableTwoFactorError", err.displayMessage);
			return null;
		}
	});
});
