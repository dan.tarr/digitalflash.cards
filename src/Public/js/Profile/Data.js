/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

$(function () {
	$("#deleteAccountModal").on("show.bs.modal", function () {
		let countdownElement = $("#deleteAccountCountdown");
		let countdownTextElement = $("#countdownText");
		let confirmButton = $("#deleteAccountConfirm");
		let secondsRemaining = 5;

		countdownTextElement.show();
		confirmButton.attr("disabled", "true");
		countdownElement.text(secondsRemaining);
		secondsRemaining -= 1;

		let intervalId = setInterval(() => {
			countdownElement.text(secondsRemaining);

			if (secondsRemaining === 0) {
				clearInterval(intervalId);
				$("#deleteAccountConfirm").removeAttr("disabled");
				$("#countdownText").hide();
			}

			secondsRemaining -= 1;
		}, 1000);
	});

	$("#downloadData").click(async function () {
		try {
			window.utils.displayLoading("downloadData");
			window.utils.clearErrors(["downloadDataError"]);

			await window.apiRequest.postJSON("/api/account/download", {});

			window.utils.hideLoading("downloadData");
			window.utils.displayError("downloadDataSuccess", "Successfully requested data" +
				" download. A link will be emailed to you shortly.");

			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			window.utils.hideLoading("downloadData");
			window.utils.displayError("downloadDataError", err.displayMessage);
			return null;
		}
	});

	$("#deleteAccountConfirm").click(async function () {
		let passwordElement = $("#password");

		try {
			let password = passwordElement.val();

			if (!window.utils.validation.presenceCheck(password))
				return window.utils.displayError("deleteAccountError", "Password cannot be blank");

			window.utils.displayLoading("deleteAccountConfirm");
			window.utils.clearErrors(["deleteAccountError"]);

			await window.apiRequest.deleteJSON("/api/account", {
				password
			});

			window.location = "/";

			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			window.utils.hideLoading("deleteAccountConfirm");
			window.utils.displayError("deleteAccountError", err.displayMessage);
			window.utils.enableTextInputElements([passwordElement]);
			return null;
		}
	});
});
