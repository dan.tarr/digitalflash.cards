/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

(function ($) {
	"use strict";

	// Enable Bootstrap tooltips via data-attributes globally
	$("[data-toggle=\"tooltip\"]").tooltip();

	// Enable Bootstrap popovers via data-attributes globally
	$("[data-toggle=\"popover\"]").popover();

	$(".popover-dismiss").popover({
		trigger: "focus"
	});

	// Activate Bootstrap scrollspy for the sticky nav component
	$("body").scrollspy({
		target: "#stickyNav",
		offset: 82
	});

	// Scrolls to an offset anchor when a sticky nav link is clicked
	$(".nav-sticky a.nav-link[href*=\"#\"]:not([href=\"#\"])").click(function () {
		if (
			location.pathname.replace(/^\//, "") ===
			this.pathname.replace(/^\//, "") &&
			location.hostname === this.hostname
		) {
			let target = $(this.hash);
			target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
			if (target.length) {
				$("html, body").animate({
					scrollTop: target.offset().top - 81
				}, 200);
			}
		}
	});

	// Collapse Navbar
	// Add styling fallback for when a transparent background .navbar-marketing is scrolled
	let navbarCollapse = function () {
		let navbar = $(".navbar-marketing.bg-transparent.fixed-top");

		if (navbar.length === 0)
			return;

		if (navbar.offset().top > 0)
			$(".navbar-marketing").addClass("navbar-scrolled");
		else
			$(".navbar-marketing").removeClass("navbar-scrolled");
	};
	// Collapse now if page is not at top
	navbarCollapse();
	// Collapse the navbar when page is scrolled
	$(window).scroll(navbarCollapse);
}(jQuery));
