/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

let selectedSubjectsCount = 0;
let minimumSelectedSubjects = 3;
let maxSubjects = 20;

// Splits an array into "chunks" of a certain number
// Used to split an array into rows and columns
/**
 * @param {[]} array - The array to chunk
 * @param {number} value - The number of values in each chunk
 * @return {[][]} - An array of arrays
 */
function chunkArray(array, value) {
	return Array.from(Array(Math.ceil(array.length / value)),
		(_, i) => array.slice(i * value, i * value + value));
}

function updateSelectedSubjects() {
	let subjectsSelectedElement = $("#subjectsSelected");
	subjectsSelectedElement.text(`${selectedSubjectsCount}/${maxSubjects}`);

	if (selectedSubjectsCount >= minimumSelectedSubjects) {
		subjectsSelectedElement.removeClass("text-red")
			.addClass("text-green");
		$("#continueAccountSetup").removeAttr("disabled");
	} else {
		subjectsSelectedElement.removeClass("text-green")
			.addClass("text-red");
		$("#continueAccountSetup").attr("disabled");
	}
}

$(async function () {
	try {
		/**
		 * @type {{
		 *     error: boolean,
		 *     code: string,
		 *     defaultSubjects: DefaultSubject[]
		 * }}
		 */
		let subjectsResponse = await window.apiRequest.get("/api/defaultsubjects?filter=all");
		let defaultSubjects = subjectsResponse.defaultSubjects;

		let html = "";
		let defaultSubjectRows = chunkArray(defaultSubjects, 3);

		defaultSubjectRows.forEach(defaultSubjectRow => {
			html += "<div class='row'>";

			defaultSubjectRow.forEach(defaultSubject => {
				html += `<div class="col-lg-4 text-center image">
					<div class="card shadow-sm border-0 pt-1 pb-1 mb-3 mt-5 bg-gray-800 mx-auto
						subject-container" data-id="${defaultSubject.defaultSubjectId}">
						<div class="card-body text-center text-white">
							<h1>${defaultSubject.name}</h1>
							<button class="btn btn-green rounded-pill mt-3 subject-add">Add</button>
						</div>
					</div>
				</div>`;
			});

			html += "</div>";
		});

		$("#subjectContainer").html(html);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		window.utils.displayError("subjectsError", "Failed to load subjects, try again later");
	}

	$(document).on("click", ".subject-add", function () {
		// Get the current button that was clicked
		let button = $(this);

		button.removeClass("subject-add")
			.removeClass("btn-green")
			.addClass("subject-remove")
			.addClass("btn-red")
			.text("Remove");

		selectedSubjectsCount += 1;
		updateSelectedSubjects();
	});

	$(document).on("click", ".subject-remove", function () {
		// Get the current button that was clicked
		let button = $(this);

		button.removeClass("subject-remove")
			.removeClass("btn-red")
			.addClass("subject-add")
			.addClass("btn-green")
			.text("Add");

		selectedSubjectsCount -= 1;
		updateSelectedSubjects();
	});

	$("#continueAccountSetup").click(async function () {
		try {
			let selectedSubjects = $(".subject-remove")
				.closest(".subject-container");

			if (selectedSubjects.length < minimumSelectedSubjects) {
				return window.utils.displayError("continueError", `You must select at least 
				${minimumSelectedSubjects} subjects to continue`);
			}

			// Create an empty array to store the IDs of the selected subjects
			let selectedIds = [];

			// Loop through each of the selected subjects and add the ID to the selectedIds array
			selectedSubjects.each((index, el) => {
				let element = $(el);

				selectedIds.push(element.attr("data-id"));
			});

			// Display a loading indicator on the button
			window.utils.displayLoading("continueAccountSetup");

			// Make a POST request to the API to setup the users account
			await window.apiRequest.postJSON("/api/account/setup", {
				defaultSubjectIds: selectedIds
			});

			// Once setup is complete, redirect the user back to the homepage
			window.location = "/home";
			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			/*
				If the error doesn't have a display message property, the
				error did not originate from the API itself, so replace with a
				generic error message
			 */
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading("continueAccountSetup");
			window.utils.displayError("continueError", err.displayMessage);
			return null;
		}
	});
});
