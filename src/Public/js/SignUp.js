/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// https://emailregex.com/
// eslint-disable-next-line max-len
const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

function validatePassword(password, confirmPassword) {
	let checks = [];
	let allValid = true;

	if (password.length < 8) {
		checks.push({
			passed: false,
			text: "Password must be at least 8 characters long"
		});
		allValid = false;
	} else {
		checks.push({
			passed: true,
			text: "Password must be at least 8 characters long"
		});
	}

	if (!/\d/.test(password)) {
		checks.push({
			passed: false,
			text: "Password must contain at least 1 number"
		});
		allValid = false;
	} else {
		checks.push({
			passed: true,
			text: "Password must contain at least 1 number"
		});
	}

	if (!/[a-z]/.test(password)) {
		checks.push({
			passed: false,
			text: "Password must contain at least 1 lower case letter"
		});
		allValid = false;
	} else {
		checks.push({
			passed: true,
			text: "Password must contain at least 1 lower case letter"
		});
	}

	if (!/[A-Z]/.test(password)) {
		checks.push({
			passed: false,
			text: "Password must contain at least 1 upper case letter"
		});
		allValid = false;
	} else {
		checks.push({
			passed: true,
			text: "Password must contain at least 1 upper case letter"
		});
	}

	if ((password === confirmPassword) && allValid) {
		checks.push({
			passed: true,
			text: "Passwords must match"
		});
	} else {
		checks.push({
			passed: false,
			text: "Passwords must match"
		});
		allValid = false;
	}

	return {
		checks,
		allValid
	};
}

// eslint-disable-next-line no-unused-vars
async function register(reCaptchaToken) {
	window.grecaptcha.reset();
	let usernameElement = $("#username");
	let emailElement = $("#email");
	let passwordElement = $("#password");
	let confirmPasswordElement = $("#confirmPassword");

	try {
		// Fetch all the HTML elements of the inputs and the values entered
		let username = usernameElement.val().trim();
		let email = emailElement.val().trim();
		let password = passwordElement.val();
		let confirmPassword = confirmPasswordElement.val();

		window.utils.clearErrors(["registerError", "registerPasswordError"]);

		// Validate inputs before its sent to the server
		if (window.utils.isNullOrEmpty(username)) {
			return window.utils.displayError("registerError", "Username " +
				"cannot be empty");
		}

		if (username.length > 32) {
			return window.utils.displayError("registerError", "Username must " +
				"be less than 32 characters");
		}

		// Ensure the username only contains English characters and numbers
		if (!/^[A-Za-z][A-Za-z0-9]*$/.test(username)) {
			return window.utils.displayError("registerError", "Username must " +
				"only contain English alphanumeric characters");
		}

		if (window.utils.isNullOrEmpty(email)) {
			return window.utils.displayError("registerError", "Email cannot " +
				"be empty");
		}

		if (email.length > 360) {
			return window.utils.displayError("registerError", "Email must be " +
				"less than 360 characters");
		}

		if (!emailRegex.test(email)) {
			return window.utils.displayError("registerError", "Invalid email " +
				"provided");
		}

		if (window.utils.isNullOrEmpty(password)) {
			return window.utils.displayError("registerError", "Password cannot " +
				"be empty");
		}

		if (window.utils.isNullOrEmpty(confirmPassword)) {
			return window.utils.displayError("registerError", "Confirm password " +
				"cannot be empty");
		}

		let passwordValidation = validatePassword(password, confirmPassword);

		if (!passwordValidation.allValid) {
			let html = "";

			passwordValidation.checks.forEach(check => {
				if (check.passed)
					html += `<p class="text-green mb-1"><i class="fal fa-check mr-2"></i>${check.text}</p>`;
				else
					html += `<p class="text-red mb-1"><i class="fal fa-times mr-1"></i> ${check.text}</p>`;
			});

			return $("#registerPasswordError").html(html);
		}

		// Disable the elements to prevent the user editing the values while the request
		// is being made
		window.utils.disableTextInputElements([usernameElement, emailElement,
			passwordElement, confirmPasswordElement]);
		window.utils.displayLoading("registerButton");

		// Make the POST request to the server to register the account
		await window.apiRequest.postJSON("/api/authentication/register", {
			username,
			email,
			password,
			confirmPassword,
			reCaptchaToken
		});

		window.location = "/emailverification";
		return null;
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		if (!err.displayMessage)
			err.displayMessage = "Something went wrong";

		window.utils.enableTextInputElements([usernameElement, emailElement,
			passwordElement, confirmPasswordElement]);
		window.utils.hideLoading("registerButton");
		window.utils.displayError("registerError", err.displayMessage);
		return null;
	}
}

$(function () {
	// When the register button is clicked
	$("#registerButton").click(async function () {
		window.grecaptcha.execute();
	});
});
