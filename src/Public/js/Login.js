/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// eslint-disable-next-line no-unused-vars
async function login(reCaptchaToken) {
	window.grecaptcha.reset();
	let usernameEmailElement = $("#loginUsernameEmail");
	let passwordElement = $("#loginPassword");

	try {
		// Fetch all the HTML elements of the inputs and the values entered
		let usernameEmail = usernameEmailElement.val().trim();
		let password = passwordElement.val();
		let rememberMe = $("#loginRememberMe").prop("checked");

		window.utils.clearErrors(["loginError"]);

		// Validate inputs before its sent to the server
		if (window.utils.isNullOrEmpty(usernameEmail)) {
			return window.utils.displayError("loginError", "Username/Email " +
				"cannot be empty");
		}

		if (window.utils.isNullOrEmpty(password)) {
			return window.utils.displayError("loginError", "Password cannot " +
				"be empty");
		}

		// Disable the elements to prevent the user editing the values while the request
		// is being made
		window.utils.disableTextInputElements([usernameEmailElement, passwordElement]);
		window.utils.displayLoading("loginButton");

		// Make the POST request to the server to register the account
		/**
		 * @type {{
		 *     code: string,
		 *     username: string | null
		 * }}
		 */
		let loginResponse = await window.apiRequest.postJSON("/api/authentication/login", {
			usernameEmail,
			password,
			rememberMe,
			reCaptchaToken
		});

		if (loginResponse.code === "REQUIRES_TWO_FACTOR") {
			$("#twoFactorUsername").text(loginResponse.username);
			$("#loginContainer").hide();
			$("#twoFactorContainer").show();
			return null;
		}

		window.location = "/home";
		return null;
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		if (!err.displayMessage)
			err.displayMessage = "Something went wrong";

		switch (err.code) {
			case "REQUIRES_EMAIL_VERIFICATION": {
				window.location = "/emailverification";
				break;
			}

			case "REQUIRES_ACCOUNT_SETUP": {
				window.location = "/accountsetup";
				break;
			}

			default: {
				window.utils.enableTextInputElements([usernameEmailElement, passwordElement]);
				window.utils.hideLoading("loginButton");
				window.utils.displayError("loginError", err.displayMessage);
			}
		}

		return null;
	}
}

$(function () {
	// When the enter key is pressed while focussed on either username or password input elements
	$("#loginUsernameEmail, #loginPassword").keypress(function (e) {
		if (e.keyCode === 13)
			$("#loginButton").click();
	});

	// When the register button is clicked
	$("#loginButton").click(async function () {
		window.grecaptcha.execute();
	});

	$("#twoFactorSubmit").click(async function () {
		let twoFactorCodeElement = $("#twoFactorCode");

		try {
			let code = twoFactorCodeElement.val();

			if (!window.utils.validation.presenceCheck(code))
				return window.utils.displayError("twoFactorError", "Code cannot be blank");

			// Regular codes are 6, backup codes are 8
			if (code.length !== 6 && code.length !== 8)
				return window.utils.displayError("twoFactorError", "Invalid code");

			window.utils.displayLoading("twoFactorSubmit");
			window.utils.disableTextInputElements([twoFactorCodeElement]);

			await window.apiRequest.postJSON("/api/authentication/login2fa", {
				code
			});

			window.location = "/home";
			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			window.utils.enableTextInputElements([twoFactorCodeElement]);
			window.utils.hideLoading("twoFactorSubmit");
			window.utils.displayError("twoFactorError", err.displayMessage);
			return null;
		}
	});
});
