/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const confidenceLevels = ["High", "Medium", "Low"];

// Splits an array into "chunks" of a certain number
// Used to split an array into rows and columns
/**
 * @param {[]} array - The array to chunk
 * @param {number} value - The number of values in each chunk
 * @return {[][]} - An array of arrays
 */
function chunkArray(array, value) {
	return Array.from(Array(Math.ceil(array.length / value)),
		(_, i) => array.slice(i * value, i * value + value));
}

/**
 * @param {Object} element - The JQuery object of the element
 * @param {string} text - The text of the button
 * @return {number} - The setInterval ID
 */
function wordedLoading(element, text) {
	let originalText = element.text();
	// Set the orignal text in the element data, used to reset to original text
	element.data("original-text", originalText);
	element.text(text);
	element.attr("disabled", "true");

	return window.setInterval(function () {
		let buttonText = element.text();
		let afterText = buttonText.replace(text, "");

		if (afterText.length === 3)
			afterText = "";
		else
			afterText += ".";

		element.text(text + afterText);
	}, 300);
}

function resetWordedLoading(button, interval) {
	let originalText = button.data("original-text");
	button.text(originalText);
	button.removeAttr("disabled");
	clearInterval(interval);
}

let getMediaHTMLTypes = {
	image: function (media) {
		return `<img class="m-3" src="/media/${media.path}" alt="">`;
	},

	video: function (media) {
		return `<video class="m-3" controls>
			<source src="/media/${media.path}" type="video/mp4">
		</video>`;
	},

	audio: function (media) {
		return `<audio class="m-3" controls style="width: 90%">
			<source src="/media/${media.path}" type="audio/mp3">
		</audio>`;
	}
};

function getMediaHTML(media) {
	let html = "";

	media.forEach(currentMedia => {
		html += getMediaHTMLTypes[currentMedia.mediaType](currentMedia);
	});

	return html;
}

$(async function () {
	let loadingText = $("#loadingText");
	let loadingId = wordedLoading(loadingText,
		"Please wait while we load flashcards tailored to you");

	try {
		let {flashcards} = await window.apiRequest.get("/api/revision");

		let flashcardRows = chunkArray(flashcards, 3);

		let html = "";
		flashcardRows.forEach(row => {
			html += "<div class=\"row p-3\">";

			row.forEach(flashcard => {
				html += `<div class="col-sm-4 mt-1">
					<div class="card bg-gray-700 font-weight-400" style="height: 100%">
						<p class="font-weight-400 text-left mx-3 my-1">
							Title: <span class="flashcard-title">${flashcard.title}</span>
						</p>
						<p class="font-weight-400 text-left mx-3 my-1">Body: ${flashcard.body}</p>
	
						${getMediaHTML(flashcard.media)}
	
						<p class="font-weight-400 text-left mx-3 my-3 mx-auto">
							Current Confidence Level: ${confidenceLevels[flashcard.confidenceLevel]}
						</p>
						
						<a class="btn btn-yellow rounded-pill w-80 m-2 mx-auto"
						   href="/flashcards/${flashcard.flashcardId}/edit">
							Happy with this flashcard?
							<i class="fal fa-edit mr-1"></i> Edit
						</a>
					</div>
				</div>`;
			});

			html += "</div>";
		});

		$("#flashcardContainer").html(html);
		resetWordedLoading(loadingText, loadingId);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		if (!err.displayMessage)
			err.displayMessage = "Something went wrong fetching flashcards, please try again later";

		resetWordedLoading(loadingText, loadingId);

		loadingText.remove();
		window.utils.displayError("loadFlashcardsError", err.displayMessage);
	}
});
