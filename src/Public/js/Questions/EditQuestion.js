/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

$(function () {
	$("#addAnswer").click(function () {
		$("#answersContainer").append(`<div class="input-group mb-3 answer">
			<div class="input-group-prepend">
				<span class="input-group-text input-group-text-dark question-answer">
					<i class="fal fa-${window.answerCount === 0 ? "check" : "times"}-circle c-pointer"></i>
				</span>
			</div>
			<input type="text" class="form-control form-control-dark" placeholder="Answer">
			<div class="input-group-append">
				<span class="input-group-text input-group-text-dark">
					<i class="fal fa-trash-alt text-red c-pointer answer-delete"></i>
				</span>
			</div>
		</div>`);

		window.answerCount += 1;

		// Max of 10 answers are allowed
		if (window.answerCount === 10)
			$("#addAnswer").hide();
	});

	// If the user clicks on a question-answer with a check mark, change to a times icon
	$(document).on("click", ".question-answer .fa-check-circle", function () {
		$(this).removeClass("fa-check-circle")
			.addClass("fa-times-circle");
	});

	// If the user clicks on a question-answer with a times, change to a check icon
	$(document).on("click", ".question-answer .fa-times-circle", function () {
		$(this).removeClass("fa-times-circle")
			.addClass("fa-check-circle");
	});

	// If the user clicks on a delete button, remove the answer and take one away from the total count
	$(document).on("click", ".answer-delete", function () {
		$(this).closest(".answer")
			.remove();
		window.answerCount -= 1;
	});

	$("#saveQuestion").click(async function () {
		// Create variables for each of the input elements
		let subjectElement = $("#flashcardSubject");
		let answerElements = $(".answer");

		try {
			let subject = subjectElement.find("option:selected")
				.attr("value");

			let correctAnswers = 0;
			let answers = [];
			// Loop through each of the answers and perform validation
			// eslint-disable-next-line consistent-return
			answerElements.each((index, el) => {
				let element = $(el);
				let correct = false;

				// Elements which are marked as correct will have a fa-check-circle classed element
				if (element.find(".fa-check-circle").length > 0) {
					correctAnswers += 1;
					correct = true;
				}

				let text = element.find("input")
					.val();

				if (!window.utils.validation.presenceCheck(text))
					return window.utils.displayError("saveQuestionError", "Answers cannot be blank");

				if (!window.utils.validation.lessThanLengthCheck(text, 64)) {
					return window.utils.displayError("saveQuestionError", "Answer must be less" +
						" than 64 characters");
				}

				answers.push({
					text,
					correct
				});
			});

			if (correctAnswers !== 1) {
				return window.utils.displayError("saveQuestionError", "You must mark " +
					"exactly one answer as correct");
			}

			if (answers.length > 10) {
				return window.utils.displayError("saveQuestionError", "You may only " +
					"have up to 10 answers per question");
			}

			// Display a loading button and disable the inputs
			window.utils.displayLoading("saveQuestion");
			window.utils.clearErrors(["saveQuestionError"]);

			// PATCH to the API and save the question
			let requestUrl = `/api/questions/${window.questionId}`;
			await window.apiRequest.patchJSON(requestUrl, {
				subject,
				answers
			});

			window.location = "/questions?notify=Successfully edited question";
			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			/*
				If the error doesn't have a display message property, the
				error did not originate from the API itself, so replace with a
				generic error message
			 */
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading("saveQuestion");
			window.utils.displayError("saveQuestionError", err.displayMessage);
			return null;
		}
	});
});
