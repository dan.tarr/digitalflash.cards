/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

let answerCount = 0;

$(function () {
	// When the user clicks add answer, create HTML for the answer and append it to the container
	$("#addAnswer").click(function () {
		$("#answersContainer").append(`<div class="input-group mb-3 answer">
			<div class="input-group-prepend">
				<span class="input-group-text input-group-text-dark question-answer">
					<i class="fal fa-${answerCount === 0 ? "check" : "times"}-circle c-pointer"></i>
				</span>
			</div>
			<input type="text" class="form-control form-control-dark" placeholder="Answer">
			<div class="input-group-append">
				<span class="input-group-text input-group-text-dark">
					<i class="fal fa-trash-alt text-red c-pointer answer-delete"></i>
				</span>
			</div>
		</div>`);

		answerCount += 1;

		// Max of 10 answers are allowed
		if (answerCount === 10)
			$("#addAnswer").hide();
	});

	// If the user clicks on a question-answer with a check mark, change to a times icon
	$(document).on("click", ".question-answer .fa-check-circle", function () {
		$(this).removeClass("fa-check-circle")
			.addClass("fa-times-circle");
	});

	// If the user clicks on a question-answer with a times, change to a check icon
	$(document).on("click", ".question-answer .fa-times-circle", function () {
		$(this).removeClass("fa-times-circle")
			.addClass("fa-check-circle");
	});

	// If the user clicks on a delete button, remove the answer and take one away from the total count
	$(document).on("click", ".answer-delete", function () {
		$(this).closest(".answer")
			.remove();

		answerCount -= 1;

		if (answerCount < 10)
			$("#addAnswer").show();
	});

	$("#addQuestion").click(async function () {
		// Create variables for each of the input elements
		let titleElement = $("#questionTitle");
		let subjectElement = $("#questionSubject");
		let answerElements = $(".answer");

		try {
			// Get the values of the inputs
			let title = titleElement.val();
			let subject = subjectElement.find("option:selected")
				.attr("value");

			// Validate all the user data and display error if validation fails
			if (!window.utils.validation.presenceCheck(title))
				return window.utils.displayError("addQuestionError", "Title cannot be blank");

			if (!window.utils.validation.lessThanLengthCheck(title, 128)) {
				return window.utils.displayError("addQuestionError", "Title must be less" +
					" than 128 characters");
			}

			if (answerElements.length < 2)
				return window.utils.displayError("addQuestionError", "You must create at least 2 answers");

			let correctAnswers = 0;
			let answers = [];
			let validAnswers = true;
			// Loop through each of the answers and perform validation
			// eslint-disable-next-line consistent-return
			answerElements.each((index, el) => {
				let element = $(el);
				let correct = false;

				// Elements which are marked as correct will have a fa-check-circle classed element
				if (element.find(".fa-check-circle").length > 0) {
					correctAnswers += 1;
					correct = true;
				}

				let text = element.find("input")
					.val();

				if (!window.utils.validation.presenceCheck(text)) {
					validAnswers = false;
					return window.utils.displayError("addQuestionError", "Answers cannot be blank");
				}

				if (!window.utils.validation.lessThanLengthCheck(text, 128)) {
					validAnswers = false;
					return window.utils.displayError("addQuestionError", "Answer must be less" +
						" than 128 characters");
				}

				// Add the answers to an array to be POSTed to the server
				answers.push({
					text,
					correct
				});
			});

			if (!validAnswers)
				return null;

			if (correctAnswers !== 1) {
				return window.utils.displayError("addQuestionError", "You must mark " +
					"exactly one answer as correct");
			}

			if (answers.length > 10) {
				return window.utils.displayError("addQuestionError", "You may only " +
					"have up to 10 answers per question");
			}

			// Clear any existing errors, disable text inputs and display a loading indicator
			window.utils.clearErrors(["addQuestionError"]);
			window.utils.disableTextInputElements([titleElement]);
			window.utils.displayLoading("addQuestion");

			// POST request to the API to create the question
			await window.apiRequest.postJSON("/api/questions", {
				title,
				subject,
				answers
			});

			// Return the user to the questions page and display a toast notification
			window.location = "/questions?notify=Successfully created question";
			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			/*
				If the error doesn't have a display message property, the
				error did not originate from the API itself, so replace with a
				generic error message
			 */
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading("addQuestion");
			window.utils.displayError("addQuestionError", err.displayMessage);
			window.utils.enableTextInputElements([titleElement]);
			return null;
		}
	});
});
