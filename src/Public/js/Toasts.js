/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

$(function () {
	window.toasts = new window.Notyf({
		duration: 4000
	});
});
