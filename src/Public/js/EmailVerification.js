/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

$(function () {
	$("#codeButton").click(async function () {
		let codeElement = $("#code");

		try {
			let code = codeElement.val().trim();

			if (window.utils.isNullOrEmpty(code))
				return window.utils.displayError("codeError", "Code cannot be empty");

			if (code.length !== 6)
				return window.utils.displayError("codeError", "Code must be 6 characters in length");

			window.utils.disableTextInputElements([codeElement]);
			window.utils.displayLoading("codeButton");

			await window.apiRequest.postJSON("/api/authentication/emailverify", {
				code
			});

			window.location = "/accountsetup";
			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			window.utils.enableTextInputElements([codeElement]);
			window.utils.hideLoading("codeButton");
			window.utils.displayError("codeError", err.displayMessage);
			return null;
		}
	});
});
