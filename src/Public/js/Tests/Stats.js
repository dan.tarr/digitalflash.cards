/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

window.Chart.defaults.global.defaultFontFamily = "Raleway";
window.Chart.defaults.global.defaultFontColor = "#fff";
window.Chart.defaults.global.defaultFontStyle = "normal";

$(async function () {
	try {
		let url = "/api/tests/history";

		if (window.testId)
			url += `?beforeThan=${window.testId}`;

		// Fetch the test history
		let {tests} = await window.apiRequest.get(url);

		/*
			A chart with 1 point looks stupid, check the user has at least 2
			subjects to display the chart
		 */
		if (tests.length < 2) {
			return window.utils.displayError("testImprovementGraphError", "You must have " +
				"at least 2 tests before this one to display this graph");
		}

		let ctx = $("#testImprovementGraph")[0].getContext("2d");

		/*
		 	Create an array of length equal to the number of tests,
		 	changing the last one to green to indicate the latest
		 */
		let pointColours = Array(tests.length - 1).fill("#fff");
		pointColours.push("#00ff00");

		// Create the chart
		// eslint-disable-next-line no-unused-vars
		let chart = new window.Chart(ctx, {
			type: "line",
			data: {
				labels: tests.map(x => {
					return x.takenAtDate;
				}),
				datasets: [{
					label: "Test Score (%)",
					data: tests.map(x => {
						return x.percentageScore;
					}),
					borderColor: "#fff",
					borderWidth: 2,
					pointBackgroundColor: pointColours,
					pointBorderColor: pointColours,
					fill: false,
					pointRadius: 4
				}]
			},
			options: {
				layout: {
					padding: {
						right: 5
					}
				},
				elements: {
					line: {
						tension: 0
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true,
							suggestedMax: 100
						}
					}],
					xAxes: [{
						ticks: {
							autoSkip: false,
							maxRotation: 20,
							minRotation: 20,
							display: false
						}
					}]
				}
			}
		});

		return null;
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		if (!err.displayMessage)
			err.displayMessage = "Something went wrong fetching graph";

		window.utils.displayError("testImprovementGraphError", err.displayMessage);
		return null;
	}
});
