/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

let testId;

/**
 * @param {Object} element - The JQuery object of the element
 * @param {string} text - The text of the button
 * @return {number} - The setInterval ID
 */
function wordedLoading(element, text) {
	let originalText = element.text();
	// Set the orignal text in the element data, used to reset to original text
	element.data("original-text", originalText);
	element.text(text);
	element.attr("disabled", "true");

	return window.setInterval(function () {
		let buttonText = element.text();
		let afterText = buttonText.replace(text, "");

		if (afterText.length === 3)
			afterText = "";
		else
			afterText += ".";

		element.text(text + afterText);
	}, 300);
}

function resetWordedLoading(button, interval) {
	let originalText = button.data("original-text");
	button.text(originalText);
	button.removeAttr("disabled");
	clearInterval(interval);
}

function getAnswersHTML(answers) {
	let html = "";

	answers.forEach(answer => {
		html += `<div class="custom-control custom-radio question-answer">
			<input type="radio" id="${answer.questionAnswerId}" name="${answer.questionId}"
				   class="custom-control-input">
				<label class="custom-control-label" for="${answer.questionAnswerId}">
					${answer.text}
				</label>
		</div>`;
	});

	return html;
}

$(async function () {
	let loadingText = $("#loadingText");
	let loadingId = wordedLoading(loadingText,
		"Please wait while we generate a test for you");

	try {
		// Create a new test and destructure the quesitons and test object
		let {questions, test} = await window.apiRequest.postJSON("/api/tests");
		testId = test.testId;

		// Generate the HTML to display the test on the page
		let html = `<p>${questions.length} questions</p>`;
		for (let i = 0; i < questions.length; i++) {
			let question = questions[i];

			html += `<div class="text-left question-container my-2 p-2" data-id="${question.questionId}">
				<p>Q${i + 1}) ${question.title}</p>

				${getAnswersHTML(question.answers)}
			</div>`;
		}

		// Set the container content to the generated HTML
		$("#testContainer").prepend(html)
			.show();
		resetWordedLoading(loadingText, loadingId);
		loadingText.remove();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		if (!err.displayMessage)
			err.displayMessage = "Something went wrong creating test, please try again later";

		resetWordedLoading(loadingText, loadingId);

		loadingText.remove();
		window.utils.displayError("loadTestError", err.displayMessage);
	}

	// When the user clicks on an answer, remove any border
	$(document).on("click", ".question-answer input", function () {
		let input = $(this);
		let questionContainer = input.closest(".question-container");

		questionContainer.css("border", "");
	});

	// When the user clicks the submit button
	$("#submitTest").click(async function () {
		try {
			// Fetch each of the questions
			let questions = $(".question-container");

			let questionsBody = [];
			let allAnswered = true;
			// Loop through each of the questions
			questions.each((index, el) => {
				let questionElement = $(el);
				// Find the input which has been checked
				let selectedAnswer = questionElement.find(".question-answer input:checked");

				// If no input has been checked, the question was not answered
				if (selectedAnswer.length === 0) {
					allAnswered = false;
					questionElement.css("border", "1px solid red")
						.css("border-radius", "1rem");
				} else {
					questionsBody.push({
						questionId: questionElement.attr("data-id"),
						selectedAnswerId: selectedAnswer.attr("id")
					});
				}
			});

			// All questions must be answered to submit
			if (!allAnswered) {
				return window.utils.displayError("testError", "You must answer " +
					"all questions before submitting");
			}

			window.utils.clearErrors(["testError"]);
			window.utils.displayLoading("submitTest");

			// POST request to mark the test, destructure the marked questions and test stats
			let {
				questions: markedQuestions,
				stats
			} = await window.apiRequest.postJSON("/api/tests/mark", {
				testId,
				questions: questionsBody
			});

			/*
				Loop through the marked questions and change the text colour to green
				on each of the correct answers, and red if the user selected an answer
				which was not correct as well as adding cross and ticks next to correct
				or incorrect answers
			 */
			markedQuestions.forEach(markedQuestion => {
				let questionContainer = $(`.question-container[data-id="${markedQuestion.questionId}"]`);

				let selectedAnswer = questionContainer
					.find(`.question-answer input[id="${markedQuestion.selectedAnswerId}"]`)
					.closest(".question-answer");
				let selectedAnswerText = selectedAnswer
					.find("label")
					.text();

				if (!markedQuestion.correct) {
					questionContainer.find(`.question-answer input[id="${markedQuestion.correctAnswerId}"]`)
						.closest(".question-answer")
						.addClass("text-green");
					selectedAnswer.addClass("text-red")
						.find("label")
						.html(selectedAnswerText + "<i class=\"fal fa-times ml-1\"></i>");
					selectedAnswer.find("input")
						.addClass("incorrect");
				} else {
					selectedAnswer.addClass("text-green")
						.find("label")
						.html(selectedAnswerText + "<i class=\"fal fa-check ml-1\"></i>");
					selectedAnswer.find("input")
						.addClass("correct");
				}
			});

			// Calculate the percentage of answers correct
			let percentageScore = Math.round(stats.correct / stats.total * 100);
			let textColour;

			/*
				Determine the colour of the score text depending on percentage correct.
				Low scores in red, medium in yellow/orange and high in green.
			 */
			if (percentageScore < 50)
				textColour = "text-red";
			else if (percentageScore >= 50 && percentageScore < 75)
				textColour = "text-yellow";
			else if (percentageScore >= 75)
				textColour = "text-green";

			$("#testScore")
				.text(`You scored ${stats.correct}/${stats.total} (${percentageScore}%)`)
				.addClass(textColour);
			$("#submitTest").after(`<a class='btn btn-success' href='/tests/${testId}/stats'>` +
				"Continue to detailed statistics</a>")
				.remove();

			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			window.utils.hideLoading("submitTest");
			window.utils.displayError("testError", err.displayMessage);

			return null;
		}
	});
});
