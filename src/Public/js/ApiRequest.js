/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

window.apiRequest = {
	/**
	 * @param {string} url - The url to make the request to
	 * @returns {Promise<Object>} - The response from the API
	 */
	get: function (url) {
		return new Promise((resolve, reject) => {
			$.ajax({
				url,
				method: "GET",
				success: function (result) {
					return resolve(result);
				},
				error: function ({responseJSON, status}) {
					if (responseJSON) {
						responseJSON.status = status;
						return reject(responseJSON);
					}

					return reject({
						status,
						displayMessage: "Something went wrong, please try again later",
						code: "UNKNOWN_STATUS"
					});
				}
			});
		});
	},

	/**
	 * @param {string} url - The url to make the request to
	 * @param {Object} [body] - The body of the request
	 * @return {Promise<Object>} - The response from the API
	 */
	postJSON: function (url, body) {
		return new Promise((resolve, reject) => {
			$.ajax({
				url,
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				data: JSON.stringify(body),
				success: function (result) {
					return resolve(result);
				},
				error: function ({responseJSON, status}) {
					if (responseJSON) {
						responseJSON.status = status;
						return reject(responseJSON);
					}

					return reject({
						status,
						displayMessage: "Something went wrong, please try again later",
						code: "UNKNOWN_STATUS"
					});
				}
			});
		});
	},

	/**
	 * @param {string} url - The url to make the request to
	 * @param {Object} form - The body of the request
	 * @return {Promise<Object>} - The response from the API
	 */
	postForm: function (url, form) {
		return new Promise((resolve, reject) => {
			$.ajax({
				url,
				method: "POST",
				data: form,
				processData: false,
				mimeType: "multipart/form-data",
				contentType: false,
				dataType: "json",
				success: function (result) {
					return resolve(result);
				},
				error: function ({responseJSON, status}) {
					if (responseJSON) {
						responseJSON.status = status;
						return reject(responseJSON);
					}

					return reject({
						status,
						displayMessage: "Something went wrong, please try again later",
						code: "UNKNOWN_STATUS"
					});
				}
			});
		});
	},

	/**
	 * @param {string} url - The url to make the request to
	 * @param {Object} body - The body of the request
	 * @return {Promise<Object>} - The response from the API
	 */
	deleteJSON: function (url, body) {
		return new Promise((resolve, reject) => {
			$.ajax({
				url,
				method: "DELETE",
				headers: {
					"Content-Type": "application/json"
				},
				data: JSON.stringify(body),
				success: function (result) {
					return resolve(result);
				},
				error: function ({responseJSON, status}) {
					if (responseJSON) {
						responseJSON.status = status;
						return reject(responseJSON);
					}

					return reject({
						status,
						displayMessage: "Something went wrong, please try again later",
						code: "UNKNOWN_STATUS"
					});
				}
			});
		});
	},

	/**
	 * @param {string} url - The url to make the request to
	 * @param {Object} body - The body of the request
	 * @return {Promise<Object>} - The response from the API
	 */
	patchJSON: function (url, body) {
		return new Promise((resolve, reject) => {
			$.ajax({
				url,
				method: "PATCH",
				headers: {
					"Content-Type": "application/json"
				},
				data: JSON.stringify(body),
				success: function (result) {
					return resolve(result);
				},
				error: function ({responseJSON, status}) {
					if (responseJSON) {
						responseJSON.status = status;
						return reject(responseJSON);
					}

					return reject({
						status,
						displayMessage: "Something went wrong, please try again later",
						code: "UNKNOWN_STATUS"
					});
				}
			});
		});
	},

	/**
	 * @param {string} url - The url to make the request to
	 * @param {Object} form - The body of the request
	 * @return {Promise<Object>} - The response from the API
	 */
	patchForm: function (url, form) {
		return new Promise((resolve, reject) => {
			$.ajax({
				url,
				method: "PATCH",
				data: form,
				processData: false,
				mimeType: "multipart/form-data",
				contentType: false,
				dataType: "json",
				success: function (result) {
					return resolve(result);
				},
				error: function ({responseJSON, status}) {
					if (responseJSON) {
						responseJSON.status = status;
						return reject(responseJSON);
					}

					return reject({
						status,
						displayMessage: "Something went wrong, please try again later",
						code: "UNKNOWN_STATUS"
					});
				}
			});
		});
	},

	/**
	 * @param {string} url - The url to make the request to
	 * @param {Object} body - The body of the request
	 * @return {Promise<Object>} - The response from the API
	 */
	putJSON: function (url, body) {
		return new Promise((resolve, reject) => {
			$.ajax({
				url,
				method: "PUT",
				headers: {
					"Content-Type": "application/json"
				},
				data: JSON.stringify(body),
				success: function (result) {
					return resolve(result);
				},
				error: function ({responseJSON, status}) {
					if (responseJSON) {
						responseJSON.status = status;
						return reject(responseJSON);
					}

					return reject({
						status,
						displayMessage: "Something went wrong, please try again later",
						code: "UNKNOWN_STATUS"
					});
				}
			});
		});
	}
};
