/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

$(function () {
	let urlParams = new URLSearchParams(window.location.search);

	// If the query params have a notify value
	if (urlParams.has("notify")) {
		window.history.replaceState({}, document.title, window.location.pathname);

		let notifyMessage = urlParams.get("notify");
		let notifyType = urlParams.get("notifyType");

		if (!notifyType)
			notifyType = "success";

		// Display a toast with the value of the notify query param
		window.toasts[notifyType](notifyMessage);
	}
});
