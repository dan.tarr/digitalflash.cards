/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

window.utils = {
	validation: {
		/**
		 * @param {string | null} value - The value to check
		 * @returns {boolean} - Indicates if the check passed or not
		 */
		presenceCheck: function (value) {
			return !(value === null || value === "");
		},

		/**
		 * @param {string} value - The value to check
		 * @param {number} max - The maxiumum length of this string
		 * @returns {boolean} - Indicates if the check passed or not
		 */
		lessThanLengthCheck: function (value, max) {
			return value.length <= max;
		},

		/**
		 * @param {*} value - The value to check
		 * @param {*[]} values - An array of valid values
		 * @returns {boolean} - Indicates if the check passed or not
		 */
		lookupCheck: function (value, values) {
			return values.includes(value);
		}
	},

	/**
	 * @param {string | null | undefined} value - The string to check
	 * @return {boolean} - If the value entered is either null or an empty string
	 */
	isNullOrEmpty: function (value) {
		return value === null || value === "";
	},

	/**
	 * @param {string | Object} button - A string ID of the button or the button element itself
	 * @returns {void}
	 */
	displayLoading: function (button) {
		if (typeof button === "string") {
			let buttonElement = $(`#${button}`);
			buttonElement.append(`<img src="${window.loadingIcon}"
			alt="" class="loading-icon">`);
			buttonElement.attr("disabled", "true");
			buttonElement.find(".text").hide();
		} else {
			button.append(`<img src="${window.loadingIcon}"
			alt="" class="loading-icon">`);
			button.attr("disabled", "true");
			button.find(".text").hide();
		}
	},

	/**
	 * @param {string | Object} button - A string ID of the button or the button element itself
	 * @returns {void}
	 */
	hideLoading: function (button) {
		if (typeof button === "string") {
			let buttonElement = $(`#${button}`);
			buttonElement.removeAttr("disabled");
			buttonElement.find(".text").show();
			buttonElement.find(".loading-icon").remove();
		} else {
			button.removeAttr("disabled");
			button.find(".text").show();
			button.find(".loading-icon").remove();
		}
	},

	/**
	 * @param {string} id - The ID of the element
	 * @param {string} message - The message to be displayed in the element
	 * @returns {void}
	 */
	displayError: function (id, message) {
		$(`#${id}`).text(message);
	},

	/**
	 * @param {string} id - The ID of the element
	 * @param {string} message - The HTML message to be displayed in the element
	 * @returns {void}
	 */
	displayHTMLError: function (id, message) {
		$(`#${id}`).html(message);
	},

	/**
	 * @param {string[]} errorIds - An array of element IDs to clear
	 * @returns {void}
	 */
	clearErrors: function (errorIds) {
		errorIds.forEach(id => {
			$(`#${id}`).text("");
		});
	},

	/**
	 * @param {Object[]} elements - An array of elements to disable
	 * @returns {void}
	 */
	disableTextInputElements: function (elements) {
		elements.forEach(function (element) {
			element.attr("disabled", "true");
		});
	},

	/**
	 * @param {Object[]} elements - An array of elements to disable
	 * @returns {void}
	 */
	enableTextInputElements: function (elements) {
		elements.forEach(function (element) {
			element.removeAttr("disabled");
		});
	}
};
