/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

$(function () {
	$(".revision-checkbox").click(function () {
		let checkbox = $(this);
		let day = checkbox.attr("data-day");
		let checked = checkbox.prop("checked");

		// Show/hide the corresponding revision-times-row for the clicked checkbox
		if (checked)
			$(`.revision-times-row[data-day=${day}]`).show();
		else
			$(`.revision-times-row[data-day=${day}]`).hide();
	});

	$("#generateTimetable").click(async function () {
		try {
			let selectedDays = $(".revision-checkbox:checked");

			// The user must select at least 1 day
			if (selectedDays.length === 0) {
				return window.utils.displayError("generateTimetableError", "You " +
					"must select at least 1 day");
			}

			let validTimes = true;
			// An object used to store the data for each day
			let days = {};

			selectedDays.each((dayIndex, dayEl) => {
				let dayElement = $(dayEl);
				let day = dayElement.attr("data-day");
				let fromHoursElement = $(`#timeFromHours${day}`);
				let fromHours = Number(fromHoursElement.val());
				let fromMinutesElement = $(`#timeFromMinutes${day}`);
				let fromMinutes = Number(fromMinutesElement.val());
				let toHoursElement = $(`#timeToHours${day}`);
				let toHours = Number(toHoursElement.val());
				let toMinutesElement = $(`#timeToMinutes${day}`);
				let toMinutes = Number(toMinutesElement.val());

				let validRow = true;

				// Check time ranges
				if (fromHours < 0 || toHours < 0)
					validRow = false;

				if (fromHours > 23 || toHours > 23)
					validRow = false;

				if (fromMinutes < 0 || toMinutes < 0)
					validRow = false;

				if (fromMinutes > 59 || toMinutes > 59)
					validRow = false;

				// Convert the hours and minutes to minutes for easier comparison
				let totalFromMinutes = (fromHours * 60) + fromMinutes;
				let totalToMinutes= (toHours * 60) + toMinutes;

				// Check the "To" time is after the "From" time
				if (totalToMinutes <= totalFromMinutes)
					validRow = false;

				if (!validRow) {
					validTimes = false;
					fromHoursElement.css("border", "1px red solid");
					fromMinutesElement.css("border", "1px red solid");
					toHoursElement.css("border", "1px red solid");
					toMinutesElement.css("border", "1px red solid");
				} else {
					fromHoursElement.css("border", "");
					fromMinutesElement.css("border", "");
					toHoursElement.css("border", "");
					toMinutesElement.css("border", "");
				}

				days[day] = {
					fromHours: fromHours,
					fromMinutes: fromMinutes,
					toHours: toHours,
					toMinutes: toMinutes
				};
			});

			if (!validTimes) {
				return window.utils.displayError("generateTimetableError", "\"From\" times " +
					"must be before \"To\" times and they must be valid 24-hour times");
			}

			window.utils.clearErrors(["generateTimetableError"]);
			window.utils.displayLoading("generateTimetable");

			await window.apiRequest.postJSON("/api/revisiontimetable", {
				days
			});

			window.location = "/revisiontimetable?notify=Successfully created revision timetable";

			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading("generateTimetable");
			window.utils.displayError("generateTimetableError", err.displayMessage);
			return null;
		}
	});
});
