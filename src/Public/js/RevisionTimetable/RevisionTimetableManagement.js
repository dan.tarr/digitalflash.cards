/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

$(function () {
	let exportGoogleButton = $("#exportGoogle");

	$("#deleteTimetable").click(async function () {
		try {
			window.utils.displayLoading("deleteTimetable");
			window.utils.clearErrors(["deleteTimetableError"]);

			await window.apiRequest.deleteJSON("/api/revisiontimetable", {});

			window.location = "/home?notify=Successfully deleted revision timetable";

			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			window.utils.hideLoading("deleteTimetable");
			window.utils.displayError("deleteTimetableError", err.displayMessage);
			return null;
		}
	});

	exportGoogleButton.click(async function () {
		try {
			// Display loading indicator
			window.utils.displayLoading("exportGoogle");
			window.utils.clearErrors(["revisionTimetableError"]);

			// POST request to API to export the calendar
			await window.apiRequest.postJSON("/api/revisiontimetable/export/google");

			window.utils.hideLoading("exportGoogle");
			window.utils.displayError("revisionTimetableSuccess", "Successfully " +
				"exported to Google Calendar");

			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// If a relink to Google is required, redirect the user to login
			if (err.code === "GOOGLE_RELINK_REQUIRED") {
				window.location = "/api/oauth/google/login?returnAction=exportGoogleCalendar";
				return null;
			}

			window.utils.hideLoading("exportGoogle");
			window.utils.displayError("revisionTimetableError", err.displayMessage);
			return null;
		}
	});

	$("#exportOutlook").click(async function () {
		try {
			// Display loading indicator
			window.utils.displayLoading("exportOutlook");
			window.utils.clearErrors(["revisionTimetableError"]);

			// POST request to API to export the calendar
			await window.apiRequest.postJSON("/api/revisiontimetable/export/outlook");

			window.utils.hideLoading("exportOutlook");
			window.utils.displayError("revisionTimetableSuccess", "Successfully " +
				"exported to Outlook Calendar");

			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			window.utils.hideLoading("exportOutlook");
			window.utils.displayError("revisionTimetableError", err.displayMessage);
			return null;
		}
	});

	let urlParams = new URLSearchParams(window.location.search);

	// If the query params have a action value
	if (urlParams.has("action")) {
		window.history.replaceState({}, document.title, window.location.pathname);

		let action = urlParams.get("action");

		/*
			If the action is "exportGoogleCalendar" then the user has returned
			from relinking their Google account when attempting to export
			their calendar due to an expired access token
		 */
		if (action === "exportGoogleCalendar") {
			exportGoogleButton.click();
			// Animate scrolling to the bottom of the page to display the export buttons
			$("html, body").animate({
				scrollTop: $(document).height()
			}, 1000);
		}
	}
});
