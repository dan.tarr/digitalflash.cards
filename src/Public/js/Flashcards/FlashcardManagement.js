/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

$(function () {
	$(".delete-flashcard").click(function () {
		let button = $(this);
		let card = button.closest(".card");
		let flashcardTitle = card.find(".flashcard-title")
			.text();
		let id = button.attr("data-id");

		$("#deleteFlashcard").attr("data-id", id);
		$("#deleteFlashcardModal").modal("show");
		$("#deleteFlashcardTitle").text(flashcardTitle);
	});

	$("#deleteFlashcard").click(async function () {
		let button = $(this);
		let id = button.attr("data-id");

		try {
			// Display loading indicator on the button
			window.utils.displayLoading(button);
			// Clear any existing errors
			window.utils.clearErrors(["deleteFlashcardError"]);

			// DELETE request to the API to delete the flashcard
			let requestUrl = `/api/flashcards/${id}`;
			await window.apiRequest.deleteJSON(requestUrl, {});

			window.location = "/flashcards?notify=Successfully deleted flashcard";
			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			/*
				If the error doesn't have a display message property, the
				error did not originate from the API itself, so replace with a
				generic error message
			 */
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading(button);
			window.utils.displayError("deleteFlashcardError", err.displayMessage);
			return null;
		}
	});
});
