/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

let validConfidenceLevels = ["high", "medium", "low"];
let mediaToDelete = [];

// Convert bytes to the closest unit to a number of decimals
function formatBytes(bytes, decimals) {
	if (bytes === 0) return "0 Bytes";
	let k = 1024;
	let dm = decimals <= 0 ? 0 : decimals || 2;
	let sizes = ["Bytes", "KB", "MB", "GB"];
	let i = Math.floor(Math.log(bytes) / Math.log(k));
	return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

$(function () {
	$("#addFile").click(function () {
		// When the user clicks the add file button, create a new file input and open the file picker
		let element = $(`<input class="flashcard-media-input" type="file" style="display: none"
			accept=".png, .jpeg, .jpg, .gif, .mp4, .mp3, .wav">`);
		$("#addFile").after(element);
		element.click();
	});

	// When a new file is selected
	$(document).on("change", ".flashcard-media-input", function () {
		let file = this.files[0];

		// Create HTML for the media row
		let html = $(`<div class="flashcard-media row mx-0 mb-2">
			<div class="col-4 text-left filename">
				<span class="middle-text">
					${file.name}
				</span>
			</div>
			<div class="col-4">
				<span class="middle-text">
					${formatBytes(file.size)}
				</span>
			</div>
			<div class="col-4 text-right text-red">
				<span class="middle-text">
					<i class="fal fa-trash-alt c-pointer delete-media"></i>
				</span>
			</div>
		</div>`);

		// Move the file input to inside the row so that it is also removed with the row
		$(this).appendTo(html);
		// Add the row to the end of the container
		$("#mediaContainer").append(html);
	});

	$(document).on("click", ".delete-media", function () {
		// Navigate UP the DOM tree to find the element with .flashcard-media and remove it
		let container = $(this).closest(".flashcard-media");
		mediaToDelete.push(container.attr("data-id"));
		container.remove();
	});

	$("#saveFlashcard").click(async function () {
		// Create variables for each of the input elements
		let bodyElement = $("#flashcardBody");
		let confidenceLevelElement = $("#flashcardConfidenceLevel");
		let subjectElement = $("#flashcardSubject");
		let fileElements = $(".flashcard-media-input");

		try {
			let body = bodyElement.val();
			let confidenceLevel = confidenceLevelElement.find("option:selected")
				.attr("value");
			let subject = subjectElement.find("option:selected")
				.attr("value");

			if (!window.utils.validation.presenceCheck(body))
				return window.utils.displayError("saveFlashcardError", "Body cannot be blank");

			if (!window.utils.validation.lessThanLengthCheck(body, 16777215)) {
				return window.utils.displayError("saveFlashcardError", "Body cannot" +
					" be longer than 16,777,215 characters");
			}

			if (!window.utils.validation.lookupCheck(confidenceLevel, validConfidenceLevels)) {
				return window.utils.displayError("saveFlashcardError", "Confidence level " +
					"must be one of " + validConfidenceLevels.join(", "));
			}

			// Display a loading button and disable the inputs
			window.utils.displayLoading("saveFlashcard");
			window.utils.disableTextInputElements([bodyElement]);

			let formData = new FormData();
			formData.append("body", body);
			formData.append("confidenceLevel", confidenceLevel);
			formData.append("subject", subject);
			formData.append("mediaToDelete", mediaToDelete.join(","));

			fileElements.each((index, el) => {
				let fileElement = $(el);
				let file = fileElement.prop("files")[0];
				formData.append("media", file);
			});

			// PATCH to the API and save the flashcard
			let requestUrl = `/api/flashcards/${window.flashcardId}`;
			await window.apiRequest.patchForm(requestUrl, formData);

			window.location = "/flashcards?notify=Successfully edited flashcard";
			return null;
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			/*
				If the error doesn't have a display message property, the
				error did not originate from the API itself, so replace with a
				generic error message
			 */
			if (!err.displayMessage)
				err.displayMessage = "Something went wrong";

			// Hide the loading icon from the button and display the error message to the user
			window.utils.hideLoading("saveFlashcard");
			window.utils.displayError("saveFlashcardError", err.displayMessage);
			window.utils.enableTextInputElements([bodyElement]);
			return null;
		}
	});
});
