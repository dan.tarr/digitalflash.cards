/*
 * Copyright (c) 2020 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const mySql = require("./Database/MySql");
const fs = require("fs").promises;

// Middleware
const validJsonBodyMiddleware = require("./Middleware/ValidJsonBodyMiddleware");
const jwtMiddleware = require("./Middleware/JwtMiddleware");
const accountSetupRequiredMiddleware = require("./Middleware/AccountSetupRequiredMiddleware");

// Variables
const port = process.env.PORT;

// Async function to run as soon as the program is ran
(async () => {
	// Establish the MySQL connection
	await mySql.connectAsync();

	// Add the middleware to parse cookies sent with requests
	app.use(cookieParser());
	// Add the middleware that will return an error if a malformed JSON body is provided
	app.use(validJsonBodyMiddleware);
	// Add the middleware that will verify and decode valid JWT tokens
	app.use(jwtMiddleware);
	// Add the middleware that will redirect the user to the account setup page if it's required
	app.use(accountSetupRequiredMiddleware);
	// Add the middleware that will parse a request's extended URL encoded body
	// noinspection JSCheckFunctionSignatures
	app.use(bodyParser.urlencoded({extended: true}));
	// Serve static files from the UserFiles directory to the /media path
	app.use("/media", express.static("./UserFiles"));

	// Disables the default behaviour of adding a x-powered-by: express header in responses
	// Prevents attackers knowing what framework the app is running to launch specifically-targeted
	// attacks
	// https://expressjs.com/en/advanced/best-practice-security.html
	app.disable("x-powered-by");

	// Set the view/template engine
	app.set("view engine", "vash");
	// Set the root directory of the template files
	app.set("views", "./src/Pages");
	// Setup assets endpoints to serve static files
	app.use("/assets", express.static("./src/Public"));
	// Run the routing file to register the endpoints
	app.use(require("./Routes/Routing"));

	// Start the webserver on a specified port
	app.listen(port, () => {
		console.log(`HTTP server listening on ${port}`);
	});

	try {
		// Try to read the UserFiles directory
		await fs.stat("./UserFiles");
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		// If an error is thrown, the directory doesn't exist, so create it
		await fs.mkdir("./UserFiles");
	}
})();
