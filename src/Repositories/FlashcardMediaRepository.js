/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const {v4: uuidv4} = require("uuid");

// Database Methods
const {queryAsync} = require("../Database/MySql");

// Classes
const FlashcardMedia = require("../Classes/FlashcardMedia");

/**
 * @param {string} mediaType - The type of file, image, video or audio
 * @param {string} path - The path of the file
 * @param {number} size - The size of the file in bytes
 * @param {string} flashcardId - The ID of the flashcard this is assigned to
 * @param {string} originalFileName - The trimmed original filename
 * @return {Promise<FlashcardMedia>} - An instance of FlashcardMedia
 */
module.exports.addFlashcardMediaAsync =
	async (mediaType, path, size, flashcardId, originalFileName) => {
		try {
			// Create a new instance of FlashcardMedia to save to the DB
			let flashcardMedia = new FlashcardMedia();
			flashcardMedia.flashcardMediaId = uuidv4();
			flashcardMedia.mediaType = mediaType;
			flashcardMedia.path = path;
			flashcardMedia.size = size;
			flashcardMedia.flashcardId = flashcardId;
			flashcardMedia.uploadedAt = new Date().getTime();
			flashcardMedia.originalFileName = originalFileName;

			// Execute the query on the database to insert the media
			await queryAsync("INSERT INTO flashcardMedia SET ?", flashcardMedia);

			return Promise.resolve(flashcardMedia);
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			return Promise.reject(err);
		}
	};

/**
 * @param {string} id - The ID of the flashcard media
 * @return {Promise<void>} - Nothing
 */
module.exports.deleteFlashcardMediaAsync = async (id) => {
	try {
		// Execute the query on the database to delete the media
		await queryAsync("DELETE FROM flashcardMedia WHERE flashcardMediaId = ?", [id]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
