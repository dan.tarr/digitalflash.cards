/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const {v4: uuidv4} = require("uuid");

// Database Methods
const {queryAsync} = require("../Database/MySql");

// Classes
const OutlookConnection = require("../Classes/OutlookConnection");

/**
 * @param {string} userId - The ID of the user
 * @return {Promise<OutlookConnection>} - An instance of OutlookConnection
 */
module.exports.getOutlookConnectionByUserIdAsync = async (userId) => {
	try {
		// Execute the query to fetch the OutlookConnection for the user ID
		let [outlookConnection] =
			await queryAsync("SELECT * FROM outlookConnections WHERE userId = ?", [userId]);

		if (!outlookConnection)
			return Promise.resolve(null);

		return Promise.resolve(new OutlookConnection(outlookConnection));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} userId - The ID of the user
 * @param {string} outlookId - The ID of the Outlook account connected
 * @param {string} outlookDisplayName - The display name of the Outlook account connected
 * @param {string} accessToken - The access token for the connection
 * @param {string} refreshToken - The refresh token for the connection
 * @param {number} expiresIn - The number of seconds until this token expires
 * @return {Promise<OutlookConnection>} - An instance of OutlookConnection
 */
module.exports.addOutlookConnectionAsync =
	async (userId, outlookId, outlookDisplayName, accessToken, refreshToken, expiresIn) => {
		try {
			// Creates a new instance of OutlookConnection, to be saved to the DB
			let outlookConnection = new OutlookConnection();
			outlookConnection.outlookConnectionId = uuidv4();
			outlookConnection.userId = userId;
			outlookConnection.outlookId = outlookId;
			outlookConnection.outlookDisplayName = outlookDisplayName;
			outlookConnection.accessToken = accessToken;
			outlookConnection.refreshToken = refreshToken;
			// expiresIn is in seconds, * 1000 to convert to ms
			outlookConnection.expiresAt = new Date().getTime() + (expiresIn * 1000);

			await queryAsync("INSERT INTO outlookConnections SET ?", outlookConnection);

			return Promise.resolve(outlookConnection);
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			return Promise.reject(err);
		}
	};

/**
 * @param {string} id - The ID of the Outlook connection
 * @return {Promise<void>} - Nothing
 */
module.exports.deleteOutlookConnectionAsync = async (id) => {
	try {
		// Executes a query to delete the OutlookConnection from the database by ID
		await queryAsync("DELETE FROM outlookConnections WHERE outlookConnectionId = ?", [id]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {OutlookConnection} outlookConnection - The instance of OutlookConnection to update
 * @return {Promise<void>} - Nothing
 */
module.exports.updateOutlookConnectionAsync = async (outlookConnection) => {
	try {
		// Execute the query to update the outlookConnection
		await queryAsync("UPDATE outlookConnections SET ? WHERE outlookConnectionId = ?",
			[outlookConnection, outlookConnection.outlookConnectionId]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
