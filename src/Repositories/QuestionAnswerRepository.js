/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const {v4: uuidv4} = require("uuid");

// Database Methods
const {queryAsync} = require("../Database/MySql");

// Classes
const QuestionAnswer = require("../Classes/QuestionAnswer");

/**
 * @param {string} text - The text of the answer
 * @param {boolean} correct - If this answer is correct
 * @param {string} questionId - The ID of the question that this answer belongs to
 * @return {Promise<QuestionAnswer>} - An instance of QuestionAnswer
 */
module.exports.addAnswerAsync = async (text, correct, questionId) => {
	try {
		// Create a new instance of QuestionAnswer, to be saved to the DB
		let questionAnswer = new QuestionAnswer();
		questionAnswer.questionAnswerId = uuidv4();
		questionAnswer.questionId = questionId;
		questionAnswer.text = text;
		questionAnswer.correctAnswer = correct;

		// Execute the query to save the question answer to the db
		await queryAsync("INSERT INTO questionAnswers SET ?", questionAnswer);

		return Promise.resolve(questionAnswer);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} id - The ID of the answer
 * @return {Promise<void>} - Nothing
 */
module.exports.deleteAnswerByIdAsync = async (id) => {
	try {
		// Execute the query on the database to delete the answer
		await queryAsync("DELETE FROM questionAnswers WHERE questionAnswerId = ?", [id]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} questionId - The ID of the question
 * @return {Promise<void>} - Nothing
 */
module.exports.deleteAnswersByQuestionIdAsync = async (questionId) => {
	try {
		// Execute the query on the database to delete the answers
		await queryAsync("DELETE FROM questionAnswers WHERE questionId = ?", [questionId]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
