/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const {v4: uuidv4} = require("uuid");

// Database Methods
const {
	queryAsync,
	beginTransactionAsync,
	commitAsync,
	rollbackAsync
} = require("../Database/MySql");

// Classes
const Flashcard = require("../Classes/Flashcard");
const FlashcardMedia = require("../Classes/FlashcardMedia");

/**
 * @param {string} userId - The ID of the user
 * @return {Promise<Flashcard[]>} - An array of instances of Flashcard
 */
module.exports.listUserFlashcardsAsync = async (userId) => {
	try {
		// Fetch the users flashcards by order of created date
		/**
		 * @type {RowDataPacket[]}
		 */
		let flashcardsRows = await queryAsync("SELECT * FROM flashcards WHERE userId = ? " +
			"ORDER BY createdAt DESC", [userId]);

		/*
			If there are no flashcards, return an empty array here to prevent
			unnecessary additonal SQL statement which will ultimately return nothing
		 */
		if (flashcardsRows.length === 0)
			return Promise.resolve([]);

		// Convert each RowDataPacket into a flashcard object
		let flashcards = flashcardsRows.map(x => {
			return new Flashcard(x);
		});

		// Return an array of flashcard IDs
		let flashcardIds = flashcards.map(x => {
			return x.flashcardId;
		});

		// Fetch all the flashcard media rows for each of the users flashcards
		/**
		 * @type {RowDataPacket[]}
		 */
		let flashcardMediaRows = await queryAsync("SELECT * FROM flashcardMedia " +
			"WHERE flashcardId IN (?)", [flashcardIds]);

		// Convert each RowDataPacket into a flashcard media object
		let flashcardMedia = flashcardMediaRows.map(x => {
			return new FlashcardMedia(x);
		});

		// Add the each of the flashcard's media
		flashcards.forEach(flashcard => {
			// Filter the array of flashcard media to only return those with this flashcard's ID
			flashcard.media = flashcardMedia.filter(x => x.flashcardId === flashcard.flashcardId);
		});

		return Promise.resolve(flashcards);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} id - The ID of the flashcard
 * @return {Promise<Flashcard>} - An instance of Flashcard
 */
module.exports.getFlashcardByIdAsync = async (id) => {
	try {
		// Execute the SQL query to fetch the flashcard
		let [flashcard] = await queryAsync("SELECT * FROM flashcards WHERE flashcardId = ?", [id]);

		if (!flashcard)
			return Promise.resolve(null);

		// Execute the SQL query to fetch the media for the flashcard
		let flashcardMedia = await queryAsync("SELECT * FROM flashcardMedia WHERE flashcardId = ?",
			[flashcard.flashcardId]);

		flashcard = new Flashcard(flashcard);
		flashcard.media = flashcardMedia;

		return Promise.resolve(flashcard);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} title - The title of the flashcards
 * @param {string} body - The body of the flashcard
 * @param {string} confidenceLevel - The confidence level of the flashcard
 * @param {string} subjectId - The ID of the subject
 * @param {string} userId - The ID of the user
 * @return {Promise<Flashcard>} - An instance of Flashcard
 */
module.exports.addFlashcardAsync =
	async (title, body, confidenceLevel, subjectId, userId) => {
		try {
			// Creates a new instance of Flashcard, to be saved to the DB
			let flashcard = new Flashcard();
			flashcard.flashcardId = uuidv4();
			flashcard.title = title;
			flashcard.body = body;
			flashcard.confidenceLevel = confidenceLevel;
			flashcard.userId = userId;
			flashcard.subjectId = subjectId;
			flashcard.createdAt = new Date().getTime();
			flashcard.updatedAt = new Date().getTime();
			flashcard.views = 0;

			/*
				.media is a property added outside of the database, so remove it to
				prevent MySQL throwing errors because it is not recognised as a field
			 */
			delete flashcard.media;

			// Execute the query to add the flashcard to the db
			await queryAsync("INSERT INTO flashcards SET ?", flashcard);

			return Promise.resolve(flashcard);
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			return Promise.reject(err);
		}
	};

/**
 * @param {Flashcard} flashcard - The Flashcard to update
 * @return {Promise<void>} - Nothing
 */
module.exports.updateFlashcardAsync = async (flashcard) => {
	try {
		/*
			.media is a property added outside of the database, so remove it to
			prevent MySQL throwing errors because it is not recognised as a field
		 */
		delete flashcard.media;

		await queryAsync("UPDATE flashcards SET ? WHERE flashcardId = ?",
			[flashcard, flashcard.flashcardId]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

module.exports.deleteFlashcardByIdAsync = async (id) => {
	try {
		// Begin a transaction
		await beginTransactionAsync();

		// Deletes the flashcard and its media
		await queryAsync("DELETE FROM flashcardMedia WHERE flashcardId = ?", [id]);
		await queryAsync("DELETE FROM flashcards WHERE flashcardId = ?", [id]);

		// Once all queries have succeeded, the changes can be commited to the db
		await commitAsync();

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		// Rollback any changes that were already made before one failed
		await rollbackAsync();
		return Promise.reject(err);
	}
};
