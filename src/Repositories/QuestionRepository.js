/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const {v4: uuidv4} = require("uuid");

// Database Methods
const {
	queryAsync,
	beginTransactionAsync,
	commitAsync,
	rollbackAsync
} = require("../Database/MySql");

// Repositories
const questionAnswerRepository = require("./QuestionAnswerRepository");

// Classes
const Question = require("../Classes/Question");
const QuestionAnswer = require("../Classes/QuestionAnswer");

/**
 * @param {string} userId - The ID of the user
 * @return {Promise<Question[]>} - An array of instances of Question
 */
module.exports.listUserQuestionsAsync = async (userId) => {
	try {
		// Fetch the users questions by order of created date
		/**
		 * @type {RowDataPacket[]}
		 */
		let questionRows = await queryAsync("SELECT * FROM questions WHERE userId = ? " +
			"ORDER BY createdAt DESC", [userId]);

		/*
			If there are no questions, return an empty array here to prevent
			unnecessary additonal SQL statement which will ultimately return nothing
		 */
		if (questionRows.length === 0)
			return Promise.resolve([]);

		// Convert each RowDataPacket into a question object
		let questions = questionRows.map(x => {
			return new Question(x);
		});

		// Return an array of question IDs
		let questionIds = questions.map(x => {
			return x.questionId;
		});

		// Fetch all the question answer rows for each of the users questions
		/**
		 * @type {RowDataPacket[]}
		 */
		let questionAnswerRows = await queryAsync("SELECT * FROM questionAnswers " +
			"WHERE questionId IN (?)", [questionIds]);

		// Convert each RowDataPacket into a question answer object
		let questionAnswers = questionAnswerRows.map(x => {
			return new QuestionAnswer(x);
		});

		// Add the each of the questions's answers
		questions.forEach(question => {
			// Filter the array of question answers to only return those with this questions's ID
			question.answers = questionAnswers.filter(x => x.questionId === question.questionId);
		});

		return Promise.resolve(questions);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} id - The ID of the question
 * @return {Promise<Question>} - An instance of Question
 */
module.exports.getQuestionByIdAsync = async (id) => {
	try {
		// Execute the SQL query to fetch the question
		let [question] = await queryAsync("SELECT * FROM questions WHERE questionId = ?", [id]);

		if (!question)
			return Promise.resolve(null);

		// Execute the SQL query to fetch the media for the flashcard
		let questionAnswers = await queryAsync("SELECT * FROM questionAnswers WHERE questionId = ?",
			[question.questionId]);

		question = new Question(question);
		question.answers = questionAnswers;

		return Promise.resolve(question);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} title - The title of the question
 * @param {string} subject - The subject ID of the question
 * @param {{text: string, correct: boolean}[]} answers - The answers of the question
 * @param {string} userId - The ID of the user
 * @return {Promise<Question>} - An instance of Question
 */
module.exports.addQuestionAsync = async (title, subject, answers, userId) => {
	try {
		// Create a new instance of Question, to be saved to the DB
		let question = new Question();
		question.questionId = uuidv4();
		question.title = title;
		question.subjectId = subject;
		question.userId = userId;
		question.createdAt = new Date().getTime();
		question.views = 0;

		/*
			.answers is a property added outside of the database, so remove it to
			prevent MySQL throwing errors because it is not recognised as a field
		 */
		delete question.answers;

		// Begins a transaction
		await beginTransactionAsync();
		await queryAsync("INSERT INTO questions SET ?", question);

		let questionAnswers = [];
		// Loop through each of the answers and add them to the DB
		for (let answer of answers) {
			questionAnswers.push(await questionAnswerRepository.addAnswerAsync(answer.text,
				answer.correct, question.questionId));
		}

		// All changes have been made, commit them to the db
		await commitAsync();

		question.answers = questionAnswers;

		return Promise.resolve(question);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		// Something went wrong, rollback any changes that were made
		await rollbackAsync();
		return Promise.reject(err);
	}
};

/**
 * @param {Question} question - The Question to update
 * @return {Promise<void>} - Nothing
 */
module.exports.updateQuestionAsync = async (question) => {
	try {
		/*
			.answers is a property added outside of the database, so remove it to
			prevent MySQL throwing errors because it is not recognised as a field
		 */
		delete question.answers;

		await queryAsync("UPDATE questions SET ? WHERE questionId = ?",
			[question, question.questionId]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} id - The ID of the question
 * @return {Promise<void>} - Nothing
 */
module.exports.deleteQuestionByIdAsync = async (id) => {
	try {
		// Begin a transaction
		await beginTransactionAsync();

		await queryAsync("DELETE FROM questionAnswers WHERE questionId = ?", [id]);
		await queryAsync("DELETE FROM questions WHERE questionId = ?", [id]);

		// Once all queries have succeeded, the changes can be commited to the db
		await commitAsync();

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		// Rollback any changes that were already made before one failed
		await rollbackAsync();
		return Promise.reject(err);
	}
};
