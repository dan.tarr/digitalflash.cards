/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const {v4: uuidv4} = require("uuid");

// Database Methods
const {
	queryAsync,
	beginTransactionAsync,
	commitAsync,
	rollbackAsync
} = require("../Database/MySql");

// Repositories
const testQuestionRepository = require("./TestQuestionRepository");

// Classes
const Test = require("../Classes/Test");
const TestQuestion = require("../Classes/TestQuestion");

/**
 * @param {string} userId - The ID of the user
 * @param {number} [olderThan] - A timestamp which resulting tests must be before
 * @return {Promise<Test[]>} - An array of instances of Question
 */
module.exports.listUserTestsAsync = async (userId, olderThan) => {
	try {
		// Set a variable to the start of the SQL query
		let query = "SELECT * FROM tests WHERE userId = ? AND complete = 1 ";

		// If the tests returned should be older than a certain point, add that to the query
		if (olderThan)
			query += `AND takenAt <= ${olderThan} `;

		// Add the order by statement at the end of the query
		query += "ORDER BY takenAt DESC";

		// Fetch the users tests by order of taken at
		/**
		 * @type {RowDataPacket[]}
		 */
		let testRows = await queryAsync(query, [userId]);

		/*
			If there are no questions, return an empty array here to prevent
			unnecessary additonal SQL statement which will ultimately return nothing
		 */
		if (testRows.length === 0)
			return Promise.resolve([]);

		// Convert each RowDataPacket into a test object
		let tests = testRows.map(x => {
			return new Test(x);
		});

		// Return an array of test IDs
		let testIds = tests.map(x => {
			return x.testId;
		});

		// Fetch all the test question rows for each of the users tests
		/**
		 * @type {RowDataPacket[]}
		 */
		let testQuestionRows = await queryAsync("SELECT * FROM testQuestions " +
			"WHERE testId IN (?)", [testIds]);

		// Convert each RowDataPacket into a test question object
		let testQuestions = testQuestionRows.map(x => {
			return new TestQuestion(x);
		});

		// Add the each of the tests questions
		tests.forEach(test => {
			// Filter the array of question answers to only return those with this questions's ID
			test.questions = testQuestions.filter(x => x.testId === test.testId);
		});

		return Promise.resolve(tests);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

module.exports.listAllUserTestsAsync = async (userId) => {
	try {
		// Fetch the users tests by order of taken at
		/**
		 * @type {RowDataPacket[]}
		 */
		let testRows = await queryAsync("SELECT * FROM tests WHERE userId = ? " +
			"ORDER BY takenAt DESC", [userId]);

		/*
			If there are no questions, return an empty array here to prevent
			unnecessary additonal SQL statement which will ultimately return nothing
		 */
		if (testRows.length === 0)
			return Promise.resolve([]);

		// Convert each RowDataPacket into a test object
		let tests = testRows.map(x => {
			return new Test(x);
		});

		// Return an array of test IDs
		let testIds = tests.map(x => {
			return x.testId;
		});

		// Fetch all the test question rows for each of the users tests
		/**
		 * @type {RowDataPacket[]}
		 */
		let testQuestionRows = await queryAsync("SELECT * FROM testQuestions " +
			"WHERE testId IN (?)", [testIds]);

		// Convert each RowDataPacket into a test question object
		let testQuestions = testQuestionRows.map(x => {
			return new TestQuestion(x);
		});

		// Add the each of the tests questions
		tests.forEach(test => {
			// Filter the array of question answers to only return those with this questions's ID
			test.questions = testQuestions.filter(x => x.testId === test.testId);
		});

		return Promise.resolve(tests);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} id - The ID of the test
 * @return {Promise<Test>} - An instance of Test
 */
module.exports.getTestByIdAsync = async (id) => {
	try {
		// Fetch the test by the test ID
		let [test] = await queryAsync("SELECT * FROM tests WHERE testId = ?", [id]);

		if (!test)
			return Promise.resolve(null);

		// Create a query to join the original question and the questionAnswers for this test question
		/**
		 * @type {RowDataPacket[]}
		 */
		let testQuestions = await queryAsync("SELECT * FROM testQuestions " +
			"LEFT JOIN questions q on q.questionId = testQuestions.questionId " +
			"LEFT JOIN questionAnswers qa on qa.questionId = q.questionId " +
			"WHERE testId = ? AND qa.correctAnswer = 1",
		[test.testId]);

		test.questions = testQuestions.map(x => {
			return new TestQuestion(x);
		});

		return Promise.resolve(new Test(test));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} userId - The ID of the user
 * @param {Question[]} questions - An array of Questions
 * @return {Promise<Test>} - An instance of Test
 */
module.exports.addTestAsync = async (userId, questions) => {
	try {
		let test = new Test();
		test.testId = uuidv4();
		test.userId = userId;
		test.complete = false;
		test.takenAt = new Date().getTime();

		/*
			.questions is a property added outside of the database, so remove it to
			prevent MySQL throwing errors because it is not recognised as a field
		 */
		delete test.questions;

		// Begin a transaction
		await beginTransactionAsync();
		// Execute query to add test to db
		await queryAsync("INSERT INTO tests SET ?", test);

		// Add all the test questions to the db
		let testQuestions = await Promise.all(questions.map(x => {
			return testQuestionRepository.addTestQuestionAsync(test.testId, x.questionId);
		}));

		// Commit the changes to the db
		await commitAsync();

		test.questions = testQuestions;

		return Promise.resolve(test);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		await rollbackAsync();
		return Promise.reject(err);
	}
};

/**
 * @param {Test} test - The test to update
 * @return {Promise<void>} - Nothing
 */
module.exports.updateTestAsync = async (test) => {
	try {
		await queryAsync("UPDATE tests SET ? WHERE testId = ?",
			[test, test.testId]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} id - The ID of the test
 * @return {Promise<void>} - Nothing
 */
module.exports.deleteTestByIdAsync = async (id) => {
	try {
		await queryAsync("DELETE FROM testQuestions WHERE testId = ?", [id]);
		await queryAsync("DELETE FROM tests WHERE testId = ?", [id]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
