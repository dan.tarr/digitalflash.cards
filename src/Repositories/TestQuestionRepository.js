/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const {v4: uuidv4} = require("uuid");

// Database Methods
const {queryAsync} = require("../Database/MySql");

// Classes
const TestQuestion = require("../Classes/TestQuestion");

/**
 * @param {string} testId - The ID of the test
 * @param {string} questionId - The ID of the question
 * @return {Promise<TestQuestion>} - An instance of TestQuestion
 */
module.exports.addTestQuestionAsync = async (testId, questionId) => {
	try {
		// Create a new TestQuestion, to be saved to the DB
		let testQuestion = new TestQuestion();
		testQuestion.testQuestionId = uuidv4();
		testQuestion.testId = testId;
		testQuestion.questionId = questionId;
		testQuestion.answeredCorrectly = false;

		// Execute query to save to db
		await queryAsync("INSERT INTO testQuestions SET ?", testQuestion);

		return Promise.resolve(testQuestion);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {TestQuestion} testQuestion - The test question to update
 * @return {Promise<void>} - Nothing
 */
module.exports.updateTestAsync = async (testQuestion) => {
	try {
		// Execute query to update db
		await queryAsync("UPDATE testQuestions SET ? WHERE testQuestionId = ?",
			[testQuestion, testQuestion.testQuestionId]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
