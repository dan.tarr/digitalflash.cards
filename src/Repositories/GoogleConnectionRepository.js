/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const {v4: uuidv4} = require("uuid");

// Database Methods
const {queryAsync} = require("../Database/MySql");

// Classes
const GoogleConnection = require("../Classes/GoogleConnection");

/**
 * @param {string} userId - The ID of the user
 * @return {Promise<GoogleConnection>} - An instance of GoogleConnection
 */
module.exports.getGoogleConnectionByUserIdAsync = async (userId) => {
	try {
		// Fetches the google connection for the users ID
		let [googleConnection] =
			await queryAsync("SELECT * FROM googleConnections WHERE userId = ?", [userId]);

		if (!googleConnection)
			return Promise.resolve(null);

		return Promise.resolve(new GoogleConnection(googleConnection));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} userId - The ID of the user
 * @param {string} googleId - The ID of the Google account connected
 * @param {string} googleEmail - The email of the Google account connected
 * @param {string} accessToken - The access token for the connection
 * @param {string} refreshToken - The refresh token for the connection
 * @param {number} expiresIn - The number of seconds until this token expires
 * @return {Promise<GoogleConnection>} - An instance of GoogleConnection
 */
module.exports.addGoogleConnectionAsync =
	async (userId, googleId, googleEmail, accessToken, refreshToken, expiresIn) => {
		try {
			// Creates a new instance of GoogleConnection, to be saved to the DB
			let googleConnection = new GoogleConnection();
			googleConnection.googleConnectionId = uuidv4();
			googleConnection.userId = userId;
			googleConnection.googleId = googleId;
			googleConnection.googleEmail = googleEmail;
			googleConnection.accessToken = accessToken;
			googleConnection.refreshToken = refreshToken;
			// expiresIn is in seconds, * 1000 to convert to ms
			googleConnection.expiresAt = new Date().getTime() + (expiresIn * 1000);

			await queryAsync("INSERT INTO googleConnections SET ?", googleConnection);

			return Promise.resolve(googleConnection);
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			return Promise.reject(err);
		}
	};

/**
 * @param {string} id - The ID of the Google connection
 * @return {Promise<void>} - Nothing
 */
module.exports.deleteGoogleConnectionAsync = async (id) => {
	try {
		// Executes the query to delete the GoogleConnection from the database by ID
		await queryAsync("DELETE FROM googleConnections WHERE googleConnectionId = ?", [id]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {GoogleConnection} googleConnection - The instance of GoogleConnection to update
 * @return {Promise<void>} - Nothing
 */
module.exports.updateGoogleConnectionAsync = async (googleConnection) => {
	try {
		// Execute the query to update the googleConnection
		await queryAsync("UPDATE googleConnections SET ? WHERE googleConnectionId = ?",
			[googleConnection, googleConnection.googleConnectionId]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
