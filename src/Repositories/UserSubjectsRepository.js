/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const {v4: uuidv4} = require("uuid");

// Database Methods
const {
	queryAsync,
	beginTransactionAsync,
	commitAsync,
	rollbackAsync
} = require("../Database/MySql");

// Classes
const UserSubject = require("../Classes/UserSubject");

/**
 * @param {string} userId - The ID of the user
 * @return {Promise<UserSubject[]>} - An array of UserSubjects
 */
module.exports.listUserSubjectsAsync = async (userId) => {
	try {
		/**
		 * @type {RowDataPacket[]}
		 */
		let subjects = await queryAsync("SELECT * FROM userSubjects WHERE userId = ?", [userId]);

		// Map through each default subject and create an instance of the UserSubject class to return
		return Promise.resolve(subjects.map(x => {
			return new UserSubject(x);
		}));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

module.exports.getUserSubjectByIdAsync = async (id) => {
	try {
		// Execute the SQL query to fetch the user
		let [userSubject] = await queryAsync("SELECT * FROM userSubjects WHERE userSubjectId = ?",
			[id]);

		if (!userSubject)
			return Promise.resolve(null);

		// Return the user object
		return Promise.resolve(new UserSubject(userSubject));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string[]} ids - An array of IDs
 * @return {Promise<UserSubject[]>} - An array of UserSubjects
 */
module.exports.getUserSubjectsByIdsAsync = async (ids) => {
	try {
		// If an empty array was provided, return an empty array
		if (ids.length === 0)
			return Promise.resolve([]);

		/**
		 * @type {RowDataPacket[]}
		 */
		let subjects = await queryAsync("SELECT * FROM userSubjects WHERE userSubjectId IN (?)", [ids]);

		return Promise.resolve(subjects.map(x => {
			return new UserSubject(x);
		}));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} name - The name of the subject
 * @param {string} userId - The ID of the user owning these subjects
 * @return {Promise<UserSubject>} - An instance of UserSubject
 */
module.exports.addUserSubjectAsync = async (name, userId) => {
	try {
		// Creates a new instance of UserSubject, to be saved to the DB
		let userSubject = new UserSubject();
		userSubject.userSubjectId = uuidv4();
		userSubject.name = name;
		// Generate a random 6 digit HEX colour
		userSubject.colour = Math.floor(Math.random() * 16777215).toString(16);
		userSubject.userId = userId;

		// Execute the query to add the user subject to the db
		await queryAsync("INSERT INTO userSubjects SET ?", userSubject);

		return Promise.resolve(userSubject);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		// Something went wrong with the queries, rollback the changes
		await rollbackAsync();
		return Promise.reject(err);
	}
};

/**
 * @param {DefaultSubject[]} defaultSubjects - An array of DefaultSubjects
 * @param {string} userId - The ID of the user owning these subjects
 * @return {Promise<UserSubject[]>} - An array of UserSubjects
 */
module.exports.addManyUserSubjectsAsync = async (defaultSubjects, userId) => {
	try {
		// Creates an empty array for the created user subjects
		let userSubjects = [];
		// Creates an empty array for the queries to make to add the user subjects
		let queries = [];

		// Begins a SQL transaction
		await beginTransactionAsync();

		// Loop through each of the default subjects
		for (const subject of defaultSubjects) {
			// Creates a new instance of UserSubject for the current default subject
			let userSubject = new UserSubject();
			userSubject.userSubjectId = uuidv4();
			userSubject.name = subject.name;
			// Generate a random 6 digit HEX colour as the default
			userSubject.colour = Math.floor(Math.random() * 16777215).toString(16);
			userSubject.userId = userId;

			// Adds the query to add this user subject to the queries array
			queries.push(queryAsync("INSERT INTO userSubjects SET ?", userSubject));
			// Adds the resulting user subject to the array
			userSubjects.push(userSubject);
		}

		// Execute adding each row in parallel to improve performance
		await Promise.all(queries);

		// Once all queries have succeeded, the changes can be commited to the db
		await commitAsync();

		return Promise.resolve(userSubjects);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		// Something went wrong with the queries, rollback the changes
		await rollbackAsync();
		return Promise.reject(err);
	}
};

/**
 * @param {string} id - The ID of the subject
 * @return {Promise<void>} - Nothing
 */
module.exports.deleteUserSubjectByIdAsync = async (id) => {
	try {
		// Executes the query to delete a user subject by ID
		await queryAsync("DELETE FROM userSubjects WHERE userSubjectId = ?", [id]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
