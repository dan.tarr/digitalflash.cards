/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const {v4: uuidv4} = require("uuid");

// Database Methods
const {queryAsync} = require("../Database/MySql");

// Classes
const RevisionTimetable = require("../Classes/RevisionTimetable");

/**
 * @param {string} userId - The ID of the user
 * @return {Promise<RevisionTimetable>} - An instance of RevisionTimetable
 */
module.exports.getRevisionTimetableByUserIdAsync = async (userId) => {
	try {
		// Execute the query to fetch the RevisionTimetable for the user ID
		let [revisionTimetable] = await queryAsync("SELECT * FROM revisionTimetables WHERE userId = ?",
			[userId]);

		if (!revisionTimetable)
			return Promise.resolve(null);

		return Promise.resolve(new RevisionTimetable(revisionTimetable));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} userId - The ID of the user
 * @param {string} timetableJson - The stringified JSON timetable
 * @return {Promise<RevisionTimetable>} - An instance of RevisionTimetable
 */
module.exports.addRevisionTimetableAsync = async (userId, timetableJson) => {
	try {
		// Creates a new instance of RevisionTimetable, to be saved to the DB
		let revisionTimetable = new RevisionTimetable();
		revisionTimetable.revisionTimetableId = uuidv4();
		revisionTimetable.userId = userId;
		revisionTimetable.timetableJson = timetableJson;

		await queryAsync("INSERT INTO revisionTimetables SET ?", revisionTimetable);

		return Promise.resolve(revisionTimetable);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} id - The ID of the user
 * @return {Promise<void>} - Nothing
 */
module.exports.deleteRevisionTimetableAsync = async (id) => {
	try {
		// Executes the query to delete the RevisionTimetable by ID
		await queryAsync("DELETE FROM revisionTimetables WHERE revisionTimetableId = ?", [id]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
