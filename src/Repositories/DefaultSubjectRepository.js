/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Database Methods
const {queryAsync} = require("../Database/MySql");

// Classes
const DefaultSubject = require("../Classes/DefaultSubject");

/**
 * @return {Promise<DefaultSubject[]>} - An array of instances of DefaultSubject
 */
module.exports.listDefaultSubjects = async () => {
	try {
		// Query the database for the default subjects
		/**
		 * @type {RowDataPacket[]}
		 */
		let defaultSubjects = await queryAsync("SELECT * FROM defaultSubjects");

		// Map through each default subject and create an instance of the DefaultSubject class to return
		return Promise.resolve(defaultSubjects.map(x => {
			return new DefaultSubject(x);
		}));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
