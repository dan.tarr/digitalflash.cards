/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const {v4: uuidv4} = require("uuid");

// Database Methods
const {queryAsync} = require("../Database/MySql");

// Classes
const User = require("../Classes/User");

/**
 * @param {string} uuid - The unique ID of the user
 * @return {Promise<User>} - An instance of User
 */
module.exports.getUserByUuidAsync = async (uuid) => {
	try {
		// Execute the SQL query to fetch the user
		let [user] = await queryAsync("SELECT * FROM users WHERE userId = ?", [uuid]);

		if (!user)
			return Promise.resolve(null);

		// Return the user object
		return Promise.resolve(new User(user));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} username - The name of the user
 * @return {Promise<User>} - An instance of User
 */
module.exports.getUserByUsernameAsync = async (username) => {
	try {
		// Execute the SQL query to fetch the user
		let [user] = await queryAsync("SELECT * FROM users WHERE username = ?", [username]);

		if (!user)
			return Promise.resolve(null);

		// Return the user object
		return Promise.resolve(new User(user));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} email - The email of the user
 * @return {Promise<User>} - An instance of User
 */
module.exports.getUserByEmailAsync = async (email) => {
	try {
		// Execute the SQL query to fetch the user
		let [user] = await queryAsync("SELECT * FROM users WHERE email = ?", [email]);

		if (!user)
			return Promise.resolve(null);

		// Return the user object
		return Promise.resolve(new User(user));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} username - The username of the user to create
 * @param {string} email - The email of the user
 * @param {string} password - The hashed password for the user
 * @param {string} emailVerificationCode - The verification code sent to the users email
 * @return {Promise<User>} - An instance of user
 */
module.exports.addUserAsync = async (username, email, password, emailVerificationCode) => {
	try {
		// Create a new user class and assign the appropriate properties
		let user = new User();
		user.userId = uuidv4();
		user.username = username;
		user.email = email;
		user.password = password;
		user.emailVerificationCode = emailVerificationCode;
		user.verifiedEmail = false;
		user.finishedAccountSetup = false;
		user.twoFactorEnabled = false;

		// Execute the SQL query to add the user to the table
		await queryAsync("INSERT INTO users SET ?", user);

		return Promise.resolve(new User(user));
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {User} user - An instance of User to update
 * @return {Promise<void>} - Nothing
 */
module.exports.updateUserAsync = async (user) => {
	try {
		// Executes the query to update the user's properties
		await queryAsync("UPDATE users SET ? WHERE userId = ?", [user, user.userId]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};

/**
 * @param {string} id - The ID of the user
 * @return {Promise<void>} - Nothing
 */
module.exports.deleteUserByIdAsync = async (id) => {
	try {
		// Delete the user by ID
		await queryAsync("DELETE FROM users WHERE userId = ?", [id]);

		return Promise.resolve();
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return Promise.reject(err);
	}
};
