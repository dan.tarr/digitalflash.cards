<!DOCTYPE html>

@{
var environment = model.environment;
var baseAssetUrl;

if (environment === "development") {
baseAssetUrl = "/assets";
} else if (environment === "production") {
baseAssetUrl = "https://digitalflash.cards/assets/prod";
}
}

<!--suppress JSUnresolvedLibraryURL -->
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
	<meta name="description" content=""/>
	<meta name="author" content=""/>
	<title>@model.title</title>

	<!-- CSS -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@@0,200;0,400;0,700;1,200;1,400;1,700&display=swap"
		  rel="stylesheet">
	<link href="https://pro.fontawesome.com/releases/v5.13.1/css/all.css" crossorigin="anonymous" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/notyf@@3/notyf.min.css" rel="stylesheet">
	<link href="@baseAssetUrl/css/Frameworks/bootstrap.css" rel="stylesheet"/>
	<link href="@baseAssetUrl/css/General.css" rel="stylesheet"/>
	@html.block("styling")

	<!-- JS -->
	<script src="@baseAssetUrl/js/Utils.js"></script>
	<script src="@baseAssetUrl/js/ApiRequest.js"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"
			crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/notyf@@3/notyf.min.js"></script>
	<script src="@baseAssetUrl/js/GeneralSetup.js" type="text/javascript"></script>
	<script src="@baseAssetUrl/js/Frameworks/aos.js" type="text/javascript"></script>
	<script src="@baseAssetUrl/js/Toasts.js" type="text/javascript"></script>
	<script src="@baseAssetUrl/js/QueryParamToast.js" type="text/javascript"></script>
	@html.block("scripts")

	<script>
		window.loadingIcon = "data:image/svg+xml,%3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='24px' height='30px' viewBox='0 0 24 30' style='enable-background:new 0 0 50 50;' xml:space='preserve'%3E%3Crect x='0' y='10' width='4' height='10' fill='%23fff' opacity='0.2'%3E%3Canimate attributeName='opacity' attributeType='XML' values='0.2; 1; .2' begin='0s' dur='0.6s' repeatCount='indefinite' /%3E%3Canimate attributeName='height' attributeType='XML' values='10; 20; 10' begin='0s' dur='0.6s' repeatCount='indefinite' /%3E%3Canimate attributeName='y' attributeType='XML' values='10; 5; 10' begin='0s' dur='0.6s' repeatCount='indefinite' /%3E%3C/rect%3E%3Crect x='8' y='10' width='4' height='10' fill='%23fff' opacity='0.2'%3E%3Canimate attributeName='opacity' attributeType='XML' values='0.2; 1; .2' begin='0.15s' dur='0.6s' repeatCount='indefinite' /%3E%3Canimate attributeName='height' attributeType='XML' values='10; 20; 10' begin='0.15s' dur='0.6s' repeatCount='indefinite' /%3E%3Canimate attributeName='y' attributeType='XML' values='10; 5; 10' begin='0.15s' dur='0.6s' repeatCount='indefinite' /%3E%3C/rect%3E%3Crect x='16' y='10' width='4' height='10' fill='%23fff' opacity='0.2'%3E%3Canimate attributeName='opacity' attributeType='XML' values='0.2; 1; .2' begin='0.3s' dur='0.6s' repeatCount='indefinite' /%3E%3Canimate attributeName='height' attributeType='XML' values='10; 20; 10' begin='0.3s' dur='0.6s' repeatCount='indefinite' /%3E%3Canimate attributeName='y' attributeType='XML' values='10; 5; 10' begin='0.3s' dur='0.6s' repeatCount='indefinite' /%3E%3C/rect%3E%3C/svg%3E";
	</script>
</head>

<body>

<div id="layoutDefault">
	<div id="layoutDefault_content">
		<main>
			<nav class="navbar navbar-marketing navbar-expand-lg bg-transparent navbar-dark fixed-top">
					<a class="navbar-brand text-white" href="/">digitalflash.cards</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
							data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
							aria-expanded="false" aria-label="Toggle navigation">
						<i class="fal fa-bars"></i>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto mr-lg-5">
							<li class="nav-item">
								<a class="nav-link" href="/home">Home </a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/flashcards">Flashcards</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/questions">Questions</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/revision">Revision</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/tests">Tests</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/revisiontimetable">Revision Timetable</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/profile">Profile</a>
							</li>
						</ul>
						<a class="btn-green btn rounded-pill px-4 ml-lg-4"
						   href="/api/authentication/logout"><i
								class="fas fa-arrow-left mr-1"></i> Logout</a>
					</div>
			</nav>

			@html.block("content")
		</main>
	</div>

	<div id="layoutDefault_footer">
		<footer class="footer pt-10 pb-5 mt-auto bg-dark footer-dark">
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
						<div class="footer-brand">digitalflash.cards</div>
						<div class="mb-3">Easy online flashcards</div>
					</div>
					<div class="col-lg-9">
						<div class="row">
							<div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
								<div class="text-uppercase-expanded text-xs mb-4">Resources</div>
								<ul class="list-unstyled mb-0">
									<li class="mb-2"><a href="/flashcards">View Flashcards</a></li>
									<li class="mb-2"><a href="/flashcards/new">New Flashcard</a></li>
									<li class="mb-2"><a href="/questions">View Questions</a></li>
									<li class="mb-2"><a href="/questions/new">New Question</a></li>
									<li class="mb-2"><a href="/revisiontimetable">Revision Timetable</a></li>
									<li><a href="/revision">Revise</a></li>
								</ul>
							</div>
							<div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
								<div class="text-uppercase-expanded text-xs mb-4">Test Yourself</div>
								<ul class="list-unstyled mb-0">
									<li class="mb-2"><a href="/tests">View Tests</a></li>
									<li><a href="/tests/new">Start A Test</a></li>
								</ul>
							</div>
							<div class="col-lg-4 col-md-6 mb-5 mb-md-0">
								<div class="text-uppercase-expanded text-xs mb-4">Manage Profile</div>
								<ul class="list-unstyled mb-0">
									<li class="mb-2"><a href="/profile">Profile Settings</a></li>
									<li class="mb-2"><a href="/profile/security">Security Settings</a></li>
									<li><a href="/profile/data">Data Settings</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<hr class="my-5"/>
				<div class="row align-items-center">
					<div class="col-md-6 small">Copyright &copy; digitalflash.cards 2020</div>
					<div class="col-md-6 text-md-right small">
						<a href="/privacy">Privacy Policy</a>
						&middot;
						<a href="/terms">Terms &amp; Conditions</a>
					</div>
				</div>
			</div>
		</footer>
	</div>
</div>

<script>
	AOS.init();
</script>

</body>
</html>
