/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();
const moment = require("moment");

// Repository
const questionRepository = require("../Repositories/QuestionRepository");
const userSubjectsRepository = require("../Repositories/UserSubjectsRepository");

// Helpers
const genericHelper = require("../Helpers/GenericHelper");

// Variables
const environment = process.env.ENVIRONMENT;

router.get("/", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		// List the users questions from the db
		/**
		 * @type {Question[]}
		 */
		let questions = await questionRepository.listUserQuestionsAsync(req.session.uuid);

		questions.forEach(x => {
			// Loop through each of the questions and change the createdAt timestamp to a moment object
			x.createdAt = moment(x.createdAt);
		});

		// Chunk the array into chunks of 3, for 3 cards per row
		let questionRows = genericHelper.chunkArray(questions, 3);

		return res.render("Questions", {
			title: "digitalflash.cards | Flashcard Management",
			environment,
			questionRows
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/new", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		// List the users subjects to be displayed on the page
		let userSubjects = await userSubjectsRepository.listUserSubjectsAsync(req.session.uuid);

		return res.render("Questions/New", {
			title: "digitalflash.cards | New Question",
			environment,
			userSubjects
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/:id/edit", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		// Fetch the current question and users subjects
		let [question, userSubjects] = await Promise.all([
			questionRepository.getQuestionByIdAsync(req.params.id),
			userSubjectsRepository.listUserSubjectsAsync(req.session.uuid)
		]);

		// The question ID wasn't found, show the 404 page
		if (!question)
			return next();

		return res.render("Questions/Edit", {
			title: "digitalflash.cards | Edit Question",
			environment,
			question,
			userSubjects,
			formatBytes: genericHelper.formatBytes
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
