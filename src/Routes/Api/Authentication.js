/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const express = require("express");
const router = express.Router();

// Repositories
const userRepository = require("../../Repositories/UserRepository");

// Helpers
const captchaHelper = require("../../Helpers/CaptchaHelper");
const authenticationHelper = require("../../Helpers/AuthenticationHelper");
const emailHelper = require("../../Helpers/EmailHelper");
const jwtHelper = require("../../Helpers/JwtHelper");
const genericHelper = require("../../Helpers/GenericHelper");
const validationHelper = require("../../Helpers/ValidationHelper");

// Variables
// https://emailregex.com/
// eslint-disable-next-line max-len
const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

router.post("/login", async (req, res, next) => {
	try {
		// Destructure the fields provided from the body object
		let {usernameEmail, password, rememberMe, reCaptchaToken} = req.body;

		// Perform a range of validation checks on the provided data
		if (!validationHelper.presenceCheck(usernameEmail)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Missing usernameEmail",
				code: "MISSING_PARAMETER"
			});
		}

		if (!validationHelper.presenceCheck(password)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Missing password",
				code: "MISSING_PARAMETER"
			});
		}

		if (typeof rememberMe !== "boolean") {
			return res.status(400).send({
				error: true,
				displayMessage: "rememberMe must be a boolean",
				code: "INVALID_PARAMETER"
			});
		}

		if (!validationHelper.presenceCheck(reCaptchaToken)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Missing reCaptchaToken",
				code: "MISSING_PARAMETER"
			});
		}

		// Check if the reCaptchaToken is valid
		/**
		 * @type {{boolean}}
		 */
		let passedCaptcha = await captchaHelper.validateCaptchaAsync(reCaptchaToken);

		if (!passedCaptcha) {
			return res.status(400).send({
				error: true,
				displayMessage: "Failed captcha, please try again",
				code: "FAILED_CAPTCHA"
			});
		}

		// Test the provided email against the email regex
		let emailFormat = emailRegex.test(usernameEmail);
		/**
		 * @type {User}
		 */
		let user;

		// If the email matches an email format, fetch the user by email otherwise fetch by username
		if (emailFormat)
			user = await userRepository.getUserByEmailAsync(usernameEmail);
		else
			user = await userRepository.getUserByUsernameAsync(usernameEmail);

		if (!user) {
			return res.status(400).send({
				error: true,
				displayMessage: "Invalid username, email or password",
				code: "INVALID_CREDENTIALS"
			});
		}

		// Check if the correct password was provided
		let correctPassword = await authenticationHelper
			.compareBcryptPasswordAsync(password, user.password);

		if (!correctPassword) {
			return res.status(400).send({
				error: true,
				displayMessage: "Invalid username, email or password",
				code: "INVALID_CREDENTIALS"
			});
		}

		// If the users email is not verified, redirect them to the email verification page
		if (!user.verifiedEmail) {
			let token = await jwtHelper.signAsync({
				uuid: user.userId,
				authState: "PENDING_EMAIL_VERIFICATION"
			}, "24h");

			jwtHelper.setJwtCookie(res, token, "24h");

			return res.status(403).send({
				error: false,
				code: "REQUIRES_EMAIL_VERIFICATION"
			});
		}

		// If the user hasn't completed the account setup stage, redirect them to the setup page
		if (!user.finishedAccountSetup) {
			let token = await jwtHelper.signAsync({
				uuid: user.userId,
				authState: "REQUIRES_ACCOUNT_SETUP"
			}, "24h");

			jwtHelper.setJwtCookie(res, token, "24h");

			return res.status(403).send({
				error: false,
				code: "REQUIRES_ACCOUNT_SETUP"
			});
		}

		// If the user has 2FA enabled, display the submit 2FA content on the page
		if (user.twoFactorEnabled) {
			let token = await jwtHelper.signAsync({
				uuid: user.userId,
				authState: "REQUIRES_TWO_FACTOR"
			}, "48h");

			jwtHelper.setJwtCookie(res, token, "48h");

			return res.status(200).send({
				error: false,
				code: "REQUIRES_TWO_FACTOR",
				username: user.username
			});
		}

		let expiresIn = rememberMe ? "30d" : "24h";

		let token = await jwtHelper.signAsync({
			uuid: user.userId,
			authState: "LOGGED_IN"
		}, expiresIn);

		jwtHelper.setJwtCookie(res, token, expiresIn);

		return res.status(200).send({
			error: false,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.post("/login2fa", async (req, res, next) => {
	try {
		// The user must have logged in and hit the 2FA required requirement first
		if (req.session.authState !== "REQUIRES_TWO_FACTOR") {
			return res.status(400).send({
				error: true,
				displayMessage: "You do not currently need to login with 2FA",
				code: "INVALID_AUTH_STATE"
			});
		}

		// Destructure the code from the body
		let {code} = req.body;

		// Validation checks
		if (!validationHelper.presenceCheck(code)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Code cannot be blank",
				code: "MISSING_PARAMETER"
			});
		}

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		// Check the user has 2FA enabled
		if (!user.twoFactorEnabled) {
			return res.status(400).send({
				error: true,
				displayMessage: "You do not have 2FA enabled",
				code: "2FA_NOT_ENABLED"
			});
		}

		// If the code is 6 long, its a regular 2FA code
		if (code.length === 6) {
			// Check if code is correct
			if (!authenticationHelper.checkAuthenticatorCode(code, user.twoFactorSecret)) {
				return res.status(400).send({
					error: true,
					displayMessage: "Invalid code",
					code: "INVALID_CODE"
				});
			}

			let token = await jwtHelper.signAsync({
				uuid: user.userId,
				authState: "LOGGED_IN"
			}, "24h");

			jwtHelper.setJwtCookie(res, token, "24h");

			return res.status(200).send({
				error: false,
				code: "SUCCESS"
			});
		}

		// The only other possible code is a backup code, which is 8 long
		if (code.length !== 8) {
			return res.status(400).send({
				error: true,
				displayMessage: "Invalid code",
				code: "INVALID_CODE"
			});
		}

		// Check if the code provided is in the user's backup codes
		let backupCodes = user.twoFactorBackupCodes ? user.twoFactorBackupCodes.split(",") : [];
		let backupCodeIndex = backupCodes.indexOf(code);

		if (backupCodeIndex === -1) {
			return res.status(400).send({
				error: true,
				displayMessage: "Invalid code",
				code: "INVALID_CODE"
			});
		}

		// Remove the used backup code from the valid backup codes
		backupCodes.splice(backupCodeIndex, 1);

		// Save the new list of backup codes to db
		user.twoFactorBackupCodes = backupCodes.join(",");
		await userRepository.updateUserAsync(user);

		let token = await jwtHelper.signAsync({
			uuid: user.userId,
			authState: "LOGGED_IN"
		}, "24h");

		jwtHelper.setJwtCookie(res, token, "24h");

		return res.status(200).send({
			error: false,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.post("/register", async (req, res, next) => {
	try {
		if (!req.body) {
			return res.status(400).send({
				error: true,
				displayMessage: "No body provided",
				code: "EMPTY_BODY"
			});
		}

		// Destructure the provided fields from the body object
		let {username, email, password, confirmPassword, reCaptchaToken} = req.body;

		// Validate provided inputs
		if (!validationHelper.presenceCheck(username)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Missing username",
				code: "MISSING_PARAMETER"
			});
		}

		if (!validationHelper.presenceCheck(email)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Missing email",
				code: "MISSING_PARAMETER"
			});
		}

		if (!validationHelper.presenceCheck(password)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Missing password",
				code: "MISSING_PARAMETER"
			});
		}

		if (!validationHelper.presenceCheck(confirmPassword)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Missing confirmPassword",
				code: "MISSING_PARAMETER"
			});
		}

		if (!validationHelper.presenceCheck(reCaptchaToken)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Missing reCaptchaToken",
				code: "MISSING_PARAMETER"
			});
		}

		if (!validationHelper.lessThanLengthCheck(username, 32)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Username must be less than 32 characters",
				code: "INVALID_PARAMETER"
			});
		}

		if (!/^[A-Za-z][A-Za-z0-9]*$/.test(username)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Username must only contain English alphanumeric characters",
				code: "INVALID_PARAMETER"
			});
		}

		if (!validationHelper.lessThanLengthCheck(email, 360)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Email must be less than 360 characters",
				code: "INVALID_PARAMETER"
			});
		}

		if (!emailRegex.test(email)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Invalid email provided",
				code: "INVALID_PARAMETER"
			});
		}

		/**
		 * @type {{boolean}}
		 */
		let passedCaptcha = await captchaHelper.validateCaptchaAsync(reCaptchaToken);

		if (!passedCaptcha) {
			return res.status(400).send({
				error: true,
				displayMessage: "Failed captcha, please try again",
				code: "FAILED_CAPTCHA"
			});
		}

		if (!authenticationHelper.validatePassword(password)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Password doesn't meet security requirements",
				code: "INVALID_PARAMETER"
			});
		}

		if (password !== confirmPassword) {
			return res.status(400).send({
				error: true,
				displayMessage: "Passwords do not match",
				code: "INVALID_PARAMETER"
			});
		}

		let existingUser = await userRepository.getUserByUsernameAsync(username);

		if (existingUser) {
			return res.status(400).send({
				error: true,
				displayMessage: "A user with that username already exists",
				code: "USERNAME_TAKEN"
			});
		}

		// Hash the user's password to store securely
		let hashedPassword = await authenticationHelper.hashPasswordAsync(password);
		// Generate a verification code to send to the users email
		let emailVerificationCode = genericHelper.getRandomString(6);

		/**
		 * @type {User}
		 */
		let user = await userRepository.addUserAsync(username, email, hashedPassword,
			emailVerificationCode);

		// Send an email to the user with the verification code
		await emailHelper.sendVerificationEmailAsync(user.email, emailVerificationCode,
			user.username);

		let token = await jwtHelper.signAsync({
			uuid: user.userId,
			authState: "PENDING_EMAIL_VERIFICATION"
		}, "24h");

		jwtHelper.setJwtCookie(res, token, "24h");

		return res.status(200).send({
			error: false,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.post("/emailverify", async (req, res, next) => {
	try {
		// Check the user is pending email verification
		if (req.session.authState !== "PENDING_EMAIL_VERIFICATION") {
			return res.status(400).send({
				error: true,
				displayMessage: "You are not pending an email verification",
				code: "INVALID_AUTH_STATE"
			});
		}

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		// Destructure the code from the body object
		let {code} = req.body;

		if (!validationHelper.presenceCheck(code)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Missing code",
				code: "MISSING_PARAMETER"
			});
		}

		if (code !== user.emailVerificationCode) {
			return res.status(400).send({
				error: true,
				displayMessage: "Invalid code",
				code: "INVALID_CODE"
			});
		}

		// Set the user fields to indicate the email has been verified
		user.verifiedEmail = true;
		user.emailVerificationCode = null;
		await userRepository.updateUserAsync(user);

		let token = await jwtHelper.signAsync({
			uuid: user.userId,
			authState: "REQUIRES_ACCOUNT_SETUP"
		}, "24h");

		jwtHelper.setJwtCookie(res, token, "24h");

		return res.status(200).send({
			error: false,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/logout", (req, res) => {
	jwtHelper.removeJwtCookie(res);

	return res.redirect("/");
});

module.exports = router;
