/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();

// Repositories
const userRepository = require("../../Repositories/UserRepository");

// Helpers
const validationHelper = require("../../Helpers/ValidationHelper");
const authenticationHelper = require("../../Helpers/AuthenticationHelper");

router.post("/enable", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);
		// Generate a 2FA secret and QR code URL
		/**
		 * @type {{secret: string, url: string}}
		 */
		let twoFactorDetails = await authenticationHelper.createAuthenticatorSecretAsync(user.username);

		// Sets the 2FA details and save to db
		user.twoFactorSecret = twoFactorDetails.secret;
		await userRepository.updateUserAsync(user);

		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			url: twoFactorDetails.url,
			secret: twoFactorDetails.secret
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.post("/confirm", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		let {code} = req.body;

		// Validation checks
		if (!validationHelper.presenceCheck(code)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Code cannot be blank",
				code: "MISSING_PARAMETER"
			});
		}

		if (code.length !== 6) {
			return res.status(400).send({
				error: true,
				displayMessage: "Code must be 6 digits",
				code: "MISSING_PARAMETER"
			});
		}

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		// Check a 2FA secret exists, generated from the endpoint above
		if (!user.twoFactorSecret) {
			return res.status(400).send({
				error: true,
				displayMessage: "2FA has not been initialised, use /api/twofactor/enable first",
				code: "2FA_NOT_INITALISED"
			});
		}

		// Checks if the code is correct
		if (!authenticationHelper.checkAuthenticatorCode(code, user.twoFactorSecret)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Invalid code",
				code: "INVALID_CODE"
			});
		}

		// Marks 2FA as enabled and saves to db
		user.twoFactorEnabled = true;
		await userRepository.updateUserAsync(user);

		return res.status(200).send({
			error: true,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.post("/disable", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		let {code} = req.body;

		// Validation checks
		if (!validationHelper.presenceCheck(code)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Code cannot be blank",
				code: "MISSING_PARAMETER"
			});
		}

		if (code.length !== 6) {
			return res.status(400).send({
				error: true,
				displayMessage: "Code must be 6 digits",
				code: "MISSING_PARAMETER"
			});
		}

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		// Check if 2FA is already disabled
		if (!user.twoFactorEnabled) {
			return res.status(400).send({
				error: true,
				displayMessage: "2FA is already disabled",
				code: "2FA_ALREADY_DISABLED"
			});
		}

		// Check if code is correct
		if (!authenticationHelper.checkAuthenticatorCode(code, user.twoFactorSecret)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Invalid code",
				code: "INVALID_CODE"
			});
		}

		// Disable 2FA properties and save to db
		user.twoFactorEnabled = false;
		user.twoFactorSecret = null;
		user.twoFactorBackupCodes = null;
		await userRepository.updateUserAsync(user);

		return res.status(200).send({
			error: true,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/codes", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		// Check the user has 2FA enabled already
		if (!user.twoFactorEnabled) {
			return res.status(400).send({
				error: true,
				displayMessage: "You do not have 2FA enabled",
				code: "2FA_NOT_ENABLED"
			});
		}

		// Generates 6 backup codes
		let backupCodes = authenticationHelper.generateBackupCodes(6);

		// Join each of the backup codes with a comma and save to the db
		user.twoFactorBackupCodes = backupCodes.join(",");
		await userRepository.updateUserAsync(user);

		// Generate content for the txt file
		let fileContent = `Backup codes for ${user.username} on digitalflash.cards\n`;
		fileContent += `Generated at ${new Date().toString()}\n\n`;
		fileContent += `* ${backupCodes.join("\n* ")}`;

		// Set a header to tell the browser to download the contents as a file
		res.setHeader("Content-disposition", "attachment; filename=digitalflash.cards-backupcodes.txt");
		return res.status(200).send(fileContent);
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
