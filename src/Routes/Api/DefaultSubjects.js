/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const express = require("express");
const router = express.Router();

// Repositories
const defaultSubjectRepository = require("../../Repositories/DefaultSubjectRepository");

// Variables
const validFilters = [
	"all",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"10",
	"11",
	"12",
	"13"
];

router.get("/", async (req, res, next) => {
	try {
		let filter = req.query.filter;

		/*
			If the filter provided wasn't in the valid array, return
			a bad request listing the valid filters
		 */
		if (!validFilters.includes(filter)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Invalid filter provided",
				code: "INVALID_FILTER",
				validFilters
			});
		}

		/**
		 * @type {DefaultSubject[]}
		 */
		let defaultSubjects = await defaultSubjectRepository.listDefaultSubjects();

		/*
		 	If the user's requested filter was not everything, filter the returned array by the
		 	provided filter
		 */
		if (filter !== "all") {
			defaultSubjects = defaultSubjects
				.filter(defaultSubject => defaultSubject.yearGroup.includes(filter));
		}

		// Return a 200 OK with the default subjects
		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			defaultSubjects
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
