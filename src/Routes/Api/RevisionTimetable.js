/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();
const moment = require("moment");

// Repositories
const userSubjectsRepository = require("../../Repositories/UserSubjectsRepository");
const revisionTimetableRepository = require("../../Repositories/RevisionTimetableRepository");
const googleConnectionRepository = require("../../Repositories/GoogleConnectionRepository");
const outlookConnectionRepository = require("../../Repositories/OutlookConnectionRepository");

// Helpers
const validationHelper = require("../../Helpers/ValidationHelper");
const googleHelper = require("../../Helpers/GoogleHelper");
const outlookHelper = require("../../Helpers/OutlookHelper");
const oauthHelper = require("../../Helpers/OAuthHelper");
const genericHelper = require("../../Helpers/GenericHelper");

// Variables
const validDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

router.post("/", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		// Destructure the days from the body object
		let {days} = req.body;

		// Validation checks
		if (!validationHelper.presenceCheck(days)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Days cannot be blank",
				code: "MISSING_PARAMETER"
			});
		}

		if (typeof days !== "object") {
			return res.status(400).send({
				error: true,
				displayMessage: "Days must be an object",
				code: "INVALID_PARAMETER"
			});
		}

		// Returns an array of keys from the days object
		let daysKeys = Object.keys(days);
		// Checks that at least 1 valid day was included in the days object
		let containsValidDay = daysKeys.some(x => validDays.includes(x));

		if (!containsValidDay) {
			return res.status(400).send({
				error: true,
				displayMessage: "The Days object must contain at least one valid day",
				code: "INVALID_PARAMETER",
				validDays
			});
		}

		// Creates a 2D array to store the timetable
		let timetableJson = [[], [], [], [], [], [], []];
		/**
		 * @type {UserSubject[]}
		 */
		let subjects = await userSubjectsRepository.listUserSubjectsAsync(req.session.uuid);

		if (subjects.length < 2) {
			return res.status(400).send({
				error: true,
				displayMessage: "You must have at least 2 subjects to generate a timetable",
				code: "TOO_FEW_SUBJECTS"
			});
		}

		// Add a 1/2 of the subject count as empty subjects to allow the timetable to add blank slots
		for (let i = 0; i < subjects.length / 2; i++) {
			// noinspection JSCheckFunctionSignatures
			subjects.push({});
		}

		let slotCount = 0;
		daysKeys.forEach(day => {
			let dayIndex = validDays.indexOf(day);

			if (dayIndex < 0)
				return;

			// Loop from the starting hour to the ending hour, increasing 1 hour at a time
			for (let i = days[day].fromHours; i < days[day].toHours; i++) {
				// Randomly select a subject
				let selectedSubject = subjects[Math.floor(Math.random() * subjects.length)];

				// If the selected subject does not have a ID, it was a blank one, ignore and continue
				if (!selectedSubject.userSubjectId)
					continue;

				slotCount += 1;
				// Add the subject slot
				timetableJson[dayIndex].push({
					start: i,
					end: i + 1,
					subject: selectedSubject.userSubjectId
				});
			}
		});

		// If less than 2 slots were created, class it as a failed generation due to lack of time
		if (slotCount < 2) {
			return res.status(400).send({
				error: true,
				displayMessage: "Sorry, we couldn't generate a timetable for you. Try " +
					"select a wider range of times or days",
				code: "TIMETABLE_GENERATION_FAILED"
			});
		}

		// Add the revision timetable to db
		let revisionTimetable = await revisionTimetableRepository
			.addRevisionTimetableAsync(req.session.uuid, JSON.stringify(timetableJson));

		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			revisionTimetable
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.delete("/", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		/**
		 * @type {RevisionTimetable}
		 */
		let revisionTimetable = await revisionTimetableRepository
			.getRevisionTimetableByUserIdAsync(req.session.uuid);

		// Check the user has a timetable to delete
		if (!revisionTimetable) {
			return res.status(400).send({
				error: true,
				displayMessage: "You do not have a revision timetable to delete",
				code: "TIMETABLE_NOT_FOUND"
			});
		}

		// Delete the user timetable
		await revisionTimetableRepository
			.deleteRevisionTimetableAsync(revisionTimetable.revisionTimetableId);

		// Return 200 OK
		return res.status(200).send({
			error: true,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.post("/export/outlook", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		let [outlookConnection, revisionTimetable] = await Promise.all([
			outlookConnectionRepository
				.getOutlookConnectionByUserIdAsync(req.session.uuid),
			revisionTimetableRepository.getRevisionTimetableByUserIdAsync(req.session.uuid)
		]);

		if (!outlookConnection) {
			return res.status(400).send({
				error: true,
				displayMessage: "You must connect an Outlook account to export to Outlook Calendar",
				code: "OUTLOOK_NOT_CONNECTED"
			});
		}

		if (!revisionTimetable) {
			return res.status(400).send({
				error: true,
				displayMessage: "You do not have a revision timetable to export",
				code: "NO_REVISION_TIMETABLE"
			});
		}

		// If the access token has expired, refresh it and save the new tokens
		if (new Date().getTime() > outlookConnection.expiresAt) {
			/**
			 * @type {OutlookAccessToken}
			 */
			let tokenData = await oauthHelper.refreshOutlookToken(outlookConnection.refreshToken);

			outlookConnection.accessToken = tokenData.accessToken;
			outlookConnection.refreshToken = tokenData.refreshToken;
			outlookConnection.expiresAt = new Date().getTime() + (tokenData.expiresIn * 1000);

			await outlookConnectionRepository.updateOutlookConnectionAsync(outlookConnection);
		}

		// Attempt to create a new calendar
		/**
		 * @type {OutlookCalendar}
		 */
		let outlookCalendar = await outlookHelper.createCalendarAsync(outlookConnection.accessToken);

		// Parse the timetable as JSON
		let timetable = JSON.parse(revisionTimetable.timetableJson);
		// Fetch the user subjects
		/**
		 * @type {UserSubject[]}
		 */
		let subjects = await userSubjectsRepository.listUserSubjectsAsync(req.session.uuid);
		// Create an empty array to store the request objects
		let requests = [];
		// Start an ID counter at 1
		let id = 1;
		// Start a coutner for going through the days
		let dayIndex = 0;
		// Create a Date for tomorrow
		let tomorrow = new Date();
		tomorrow.setDate(tomorrow.getDate() + 1);

		for (let day of timetable) {
			let nextDate = genericHelper.getNextDayOfWeek(tomorrow, dayIndex + 1);

			for (let slot of day) {
				let subject = subjects.find(x => x.userSubjectId === slot.subject);

				// Create a new Date for this slot and set the time of the slot
				let slotStartTime = new Date(nextDate.getTime());
				slotStartTime.setHours(slot.start);
				slotStartTime.setMinutes(0);
				slotStartTime.setSeconds(0);
				slotStartTime.setMilliseconds(0);
				// Create a new Date for the end time of the slot
				let slotEndTime = new Date(nextDate.getTime());
				slotEndTime.setHours(slot.end);
				slotEndTime.setMinutes(0);
				slotEndTime.setSeconds(0);
				slotEndTime.setMilliseconds(0);
				let rangeStartDate = moment(slotStartTime.getTime())
					.format("YYYY[-]MM[-]DD");

				// Push a new request object to the requests array
				requests.push({
					id,
					method: "POST",
					url: `/me/calendars/${outlookCalendar.id}/events`,
					body: {
						subject: subject.name,
						start: {
							dateTime: slotStartTime.toISOString(),
							timeZone: "UTC"
						},
						end: {
							dateTime: slotEndTime.toISOString(),
							timeZone: "UTC"
						},
						// Repeated for 4 weeks
						recurrence: {
							pattern: {
								type: "weekly",
								interval: 1,
								daysOfWeek: [validDays[slotStartTime.getDay() - 1]]
							},
							range: {
								type: "numbered",
								numberOfOccurrences: 4,
								startDate: rangeStartDate
							}
						}
					},
					headers: {
						"Content-Type": "application/json"
					}
				});

				id += 1;
			}

			dayIndex += 1;
		}

		// Endpoint allows max 4 requests concurrently
		let batchRequests = genericHelper.chunkArray(requests, 4);

		let responses = [];

		for (let batch of batchRequests) {
			/**
			 * @type {{
			 *     responses: []
			 * }}
			 */
			let response = await outlookHelper.batchRequestAsync(outlookConnection.accessToken, batch);

			responses = responses.concat(response.responses);
		}

		return res.status(200).send({
			error: false,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		if (err.response) {
			// HTTP 409 = Conflict
			if (err.response.status === 409) {
				return res.status(409).send({
					error: true,
					displayMessage: "You already have a calendar on Outlook for us, please delete " +
						"the calendar named \"digitalflash.cards Revision Timetable\" and try again",
					code: "EXISTING_CALENDAR"
				});
			}
		}

		return next(err);
	}
});

router.post("/export/google", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		let [googleConnection, revisionTimetable] = await Promise.all([
			googleConnectionRepository
				.getGoogleConnectionByUserIdAsync(req.session.uuid),
			revisionTimetableRepository.getRevisionTimetableByUserIdAsync(req.session.uuid)
		]);

		if (!googleConnection) {
			return res.status(400).send({
				error: true,
				displayMessage: "You must connect an Google account to export to Google Calendar",
				code: "GOOGLE_NOT_CONNECTED"
			});
		}

		if (!revisionTimetable) {
			return res.status(400).send({
				error: true,
				displayMessage: "You do not have a revision timetable to export",
				code: "NO_REVISION_TIMETABLE"
			});
		}

		/*
			For some unknown reason, Google decided to implement their
			OAuth differently to a regular implementation. A new refresh
			token cannot be obtained once a token has been refreshed
			using an existing refresh token.
			The users consent must be obtained once again, so instead
			of attempting to refresh the token, remove the Google connection
			and ask the user to relink their account
		 */
		if (new Date().getTime() > googleConnection.expiresAt) {
			await googleConnectionRepository
				.deleteGoogleConnectionAsync(googleConnection.googleConnectionId);

			return res.status(400).send({
				error: true,
				displayMessage: "Google relink required",
				code: "GOOGLE_RELINK_REQUIRED"
			});
		}

		// Attempt to create a new calendar
		/**
		 * @type {GoogleCalendar}
		 */
		let googleCalendar = await googleHelper.createCalendarAsync(googleConnection.accessToken);

		// Parse the timetable as JSON
		let timetable = JSON.parse(revisionTimetable.timetableJson);
		// Fetch the user subjects
		/**
		 * @type {UserSubject[]}
		 */
		let subjects = await userSubjectsRepository.listUserSubjectsAsync(req.session.uuid);
		// Create an empty array to store the request objects
		let requests = [];
		// Start a coutner for going through the days
		let dayIndex = 0;
		// Create a Date for tomorrow
		let tomorrow = new Date();
		tomorrow.setDate(tomorrow.getDate() + 1);

		for (let day of timetable) {
			let nextDate = genericHelper.getNextDayOfWeek(tomorrow, dayIndex + 1);

			for (let slot of day) {
				let subject = subjects.find(x => x.userSubjectId === slot.subject);

				// Create a new Date for this slot and set the time of the slot
				let slotStartTime = new Date(nextDate.getTime());
				slotStartTime.setHours(slot.start);
				slotStartTime.setMinutes(0);
				slotStartTime.setSeconds(0);
				slotStartTime.setMilliseconds(0);
				// Create a new Date for the end time of the slot
				let slotEndTime = new Date(nextDate.getTime());
				slotEndTime.setHours(slot.end);
				slotEndTime.setMinutes(0);
				slotEndTime.setSeconds(0);
				slotEndTime.setMilliseconds(0);

				// Push a new request object to the requests array
				requests.push({
					summary: subject.name,
					start: {
						dateTime: slotStartTime.toISOString(),
						timeZone: "Etc/Universal"
					},
					end: {
						dateTime: slotEndTime.toISOString(),
						timeZone: "Etc/Universal"
					},
					// Repeated for 4 weeks
					recurrence: [
						// RFC5545 standard
						"RRULE:FREQ=WEEKLY;COUNT=4"
					]
				});
			}

			dayIndex += 1;
		}

		for (let requestBody of requests) {
			await googleHelper.createCalendarEventAsync(googleConnection.accessToken,
				googleCalendar.id, requestBody);
		}

		return res.status(200).send({
			error: false,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
