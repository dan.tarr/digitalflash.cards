/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();

// Repositories
const flashcardRepository = require("../../Repositories/FlashcardRepository");

// Helpers
const genericHelper = require("../../Helpers/GenericHelper");

// Variables
const confidenceLevelWeights = [50, 25, 10];

/**
 * @param {{
 *     weight: number,
 *     item: string
 * }[]} weights - The weighted values
 * @param {number} amount - The number to return
 * @param {string[]} selected - An array of selected IDs
 * @returns {string[]} - An array of selected flashcard IDs
 */
function getRandomFlashcards(weights, amount, selected) {
	// Get a new flashcard ID
	let newId = genericHelper.getRandomWeightedItem(weights);

	// If the new ID does not already exist in the selected array, add it
	if (!selected.includes(newId))
		selected.push(newId);

	// If the number of selected items is the amount we want, return the selected items
	if (selected.length === amount)
		return selected;

	// Otherwise, call the function again to generate another
	return getRandomFlashcards(weights, amount, selected);
}

router.get("/", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		/**
		 * @type {Flashcard[]}
		 */
		let flashcards = await flashcardRepository.listUserFlashcardsAsync(req.session.uuid);

		// 6 flashcards are returned so the user must have at least 6
		if (flashcards.length < 6) {
			return res.status(400).send({
				error: true,
				displayMessage: "You must have at least 6 flashcards before using the revision section",
				code: "TOO_FEW_FLASHCARDS"
			});
		}

		let weights = [];
		// Find the highest view count from the array of flashcards
		let maxViewCount = flashcards.reduce((prev, current) =>
			(prev.views > current.views) ? prev : current).views;

		flashcards.forEach(flashcard => {
			let weightedValue = 0;

			/*
				Add the weighted value of the flashcards confidence level.
				The lower the confidence level, the higher the weight to increase the
				chance of it being picked
			 */
			weightedValue += confidenceLevelWeights[flashcard.confidenceLevel];

			weightedValue += maxViewCount - flashcard.views;

			/*
				Add the current flashcard's ID to the weighted object with the calculated weighted value
			 */
			weights.push({
				weight: weightedValue,
				item: flashcard.flashcardId
			});
		});

		// Recursive function to get a number of random flashcards, ensuring unique IDs are selected
		let selectedIds = getRandomFlashcards(weights, 6, []);

		// Get the full flashcard objects from the array of IDs
		let selectedFlashcards = selectedIds.map(id => {
			return flashcards.find(x => x.flashcardId === id);
		});

		// Loop through each of the selected flashcards and increase the view count by 1
		let updatePromises = [];
		selectedFlashcards.forEach(flashcard => {
			let clonedFlashcard = Object.assign({}, flashcard);
			clonedFlashcard.views += 1;
			updatePromises.push(flashcardRepository.updateFlashcardAsync(clonedFlashcard));
		});

		// Update all the flashcards at once
		await Promise.all(updatePromises);

		// Return a 200 OK with the array of selected flashcards
		return res.status(200).send({
			error: true,
			code: "SUCCESS",
			flashcards: selectedFlashcards
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
