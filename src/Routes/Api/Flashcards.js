/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();
const multer = require("multer");
const upload = multer({
	storage: multer.memoryStorage(),
	limits: {
		// 5MB
		fileSize: 5 * 1024 * 1024
	}
});
const uploader = upload.array("media", 5);
const {v4: uuidv4} = require("uuid");
const fs = require("fs").promises;

// Repositories
const flashcardRepository = require("../../Repositories/FlashcardRepository");
const flashcardMediaRepository = require("../../Repositories/FlashcardMediaRepository");
const userSubjectRepository = require("../../Repositories/UserSubjectsRepository");

// Helpers
const validationHelper = require("../../Helpers/ValidationHelper");

// Variables
const confidenceLevelValues = {
	low: 0,
	medium: 1,
	high: 2
};
const validExtensions = [
	"png",
	"jpg",
	"jpeg",
	"gif",
	"mp4",
	"mp3"
];
const mediaTypes = {
	png: "image",
	jpg: "image",
	jpeg: "image",
	gif: "image",
	mp4: "video",
	mp3: "audio"
};

router.post("/", (req, res, next) => {
	// noinspection JSValidateTypes
	uploader(req, res, async error => {
		if (error instanceof multer.MulterError) {
			// If the file was above the size limit defined above
			if (error.code === "LIMIT_FILE_SIZE") {
				return res.status(413).send({
					error: true,
					displayMessage: "File too large. Max file size is 5MB.",
					code: "FILE_TOO_LARGE"
				});
			}
		} else if (error)
			return next(error);

		try {
			// Check that the user is logged in
			if (req.session.authState !== "LOGGED_IN") {
				return res.status(401).send({
					error: true,
					displayMessage: "You must be logged in to use this endpoint",
					code: "INVALID_AUTH_STATE"
				});
			}

			// Destructure each property provided by the user
			let {
				title,
				body,
				confidenceLevel,
				subject,
				newSubject
			} = req.body;
			let createNewSubject = false;

			// Validation checks
			if (!validationHelper.presenceCheck(title)) {
				return res.status(400).send({
					error: true,
					displayMessage: "Title cannot be blank",
					code: "MISSING_PARAMETER"
				});
			}

			if (!validationHelper.presenceCheck(body)) {
				return res.status(400).send({
					error: true,
					displayMessage: "Body cannot be blank",
					code: "MISSING_PARAMETER"
				});
			}

			if (!validationHelper.presenceCheck(subject)) {
				return res.status(400).send({
					error: true,
					displayMessage: "Subject ID is required",
					code: "MISSING_PARAMETER"
				});
			}

			// If the subject is new, a newSubject name must be provided
			if (newSubject) {
				if (!validationHelper.presenceCheck(newSubject)) {
					return res.status(400).send({
						error: true,
						displayMessage: "New subject cannot be blank",
						code: "MISSING_PARAMETER"
					});
				}

				if (!validationHelper.lessThanLengthCheck(newSubject, 64)) {
					return res.status(400).send({
						error: true,
						displayMessage: "New subject cannot be longer than 64 characters",
						code: "INVALID_PARAMETER"
					});
				}

				// New subject checks have passed, indicate that a new subject has to be created
				createNewSubject = true;
			}

			if (!validationHelper.lookupCheck(confidenceLevel, ["high", "medium", "low"])) {
				return res.status(400).send({
					error: true,
					displayMessage: "Invalid value provided for confidenceLevel",
					code: "INVALID_PARAMETER"
				});
			}

			if (!validationHelper.lessThanLengthCheck(title, 64)) {
				return res.status(400).send({
					error: true,
					displayMessage: "Title cannot be longer than 64 characters",
					code: "INVALID_PARAMETER"
				});
			}

			if (!validationHelper.lessThanLengthCheck(body, 16777215)) {
				return res.status(400).send({
					error: true,
					displayMessage: "Body cannot be longer than 16,777,215 characters",
					code: "INVALID_PARAMETER"
				});
			}

			if (createNewSubject) {
				// Creates a new subject
				subject = (await userSubjectRepository.addUserSubjectAsync(newSubject, req.session.uuid))
					.userSubjectId;
			}

			// Create the new flashcard
			/**
			 * @type {Flashcard}
			 */
			let flashcard = await flashcardRepository.addFlashcardAsync(title, body,
				confidenceLevelValues[confidenceLevel], subject,
				req.session.uuid);

			// Loop through each of the provided files
			let flashcardMedias = [];
			for (let file of req.files) {
				// Generate a new UUID for the filename to prevent overwrites
				let newFilename = uuidv4();

				// Extract the extension of the file
				let matchedExtension = file.originalname.match(/\.([0-9a-z]+)(?=[?#])|(\.)(?:[\w]+)$/gmi);

				if (!matchedExtension) {
					return res.status(400).send({
						error: true,
						displayMessage: "The file must have an extension",
						code: "INVALID_FILE"
					});
				}

				// Remove leading .
				let extension = matchedExtension[0].substr(1).toLowerCase();

				if (!validExtensions.includes(extension)) {
					return res.status(400).send({
						error: true,
						displayMessage: "Invalid file format",
						code: "INVALID_FILE",
						supportedExtensions: validExtensions
					});
				}

				let originalFileName = file.originalname;
				originalFileName = originalFileName.substr(0, 16);

				// If the original filename is more than 16, the string has been trimmed
				if (file.originalname.length > 16)
					originalFileName += "...";

				// Write the file to the UserFiles directory and add to the database
				let filename = `${newFilename}.${extension}`;
				await fs.writeFile(`./UserFiles/${filename}`, file.buffer);
				let flashcardMedia = await flashcardMediaRepository
					.addFlashcardMediaAsync(mediaTypes[extension],
						filename, file.size, flashcard.flashcardId, originalFileName);
				flashcardMedias.push(flashcardMedia);
			}

			// Add the array of media to the flashcard object
			flashcard.media = flashcardMedias;

			// Return a 200 OK with the created flashcard
			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				flashcard
			});
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			return next(err);
		}
	});
});

router.delete("/:id", async (req, res, next) => {
	try {
		/**
		 * @type {Flashcard}
		 */
		let flashcard = await flashcardRepository.getFlashcardByIdAsync(req.params.id);

		if (!flashcard) {
			return res.status(400).send({
				error: true,
				displayMessage: "A flashcard with that ID could not be found",
				code: "FLASHCARD_NOT_FOUND"
			});
		}

		// Delete the flashcard by its ID
		await flashcardRepository.deleteFlashcardByIdAsync(flashcard.flashcardId);

		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			flashcard
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.patch("/:id", async (req, res, next) => {
	// noinspection JSValidateTypes
	uploader(req, res, async error => {
		if (error instanceof multer.MulterError) {
			if (error.code === "LIMIT_FILE_SIZE") {
				return res.status(413).send({
					error: true,
					displayMessage: "File too large. Max file size is 5MB.",
					code: "FILE_TOO_LARGE"
				});
			}
		} else if (error)
			return next(error);

		try {
			/**
			 * @type {Flashcard}
			 */
			let flashcard = await flashcardRepository.getFlashcardByIdAsync(req.params.id);

			if (!flashcard) {
				return res.status(400).send({
					error: true,
					displayMessage: "A flashcard with that ID could not be found",
					code: "FLASHCARD_NOT_FOUND"
				});
			}

			// Destructure each property provided by the user
			let {
				body,
				confidenceLevel,
				subject,
				mediaToDelete
			} = req.body;

			// Validation checks
			if (!validationHelper.presenceCheck(body)) {
				return res.status(400).send({
					error: true,
					displayMessage: "Body cannot be blank",
					code: "MISSING_PARAMETER"
				});
			}

			if (!validationHelper.presenceCheck(subject)) {
				return res.status(400).send({
					error: true,
					displayMessage: "Subject ID is required",
					code: "MISSING_PARAMETER"
				});
			}

			if (!validationHelper.lookupCheck(confidenceLevel, ["high", "medium", "low"])) {
				return res.status(400).send({
					error: true,
					displayMessage: "Invalid value provided for confidenceLevel",
					code: "INVALID_PARAMETER"
				});
			}

			if (!validationHelper.lessThanLengthCheck(body, 16777215)) {
				return res.status(400).send({
					error: true,
					displayMessage: "Body cannot be longer than 16,777,215 characters",
					code: "INVALID_PARAMETER"
				});
			}

			// mediaToDelete should be provided in a format of ID,ID,ID
			if (validationHelper.presenceCheck(mediaToDelete)) {
				try {
					mediaToDelete = mediaToDelete.split(",");
				} catch (err) {
					// Try catch statement to catch any errors (exception handling/error trapping)
					return res.status(400).send({
						error: true,
						displayMessage: "Malformed mediaToDelete parameter",
						code: "MALFORMED_BODY"
					});
				}
			}

			// Loop through each of the provided files
			for (let file of req.files) {
				// Generate a new UUID for the filename to prevent overwrites
				let newFilename = uuidv4();

				// Extract the extension of the file
				let matchedExtension = file.originalname.match(/\.([0-9a-z]+)(?=[?#])|(\.)(?:[\w]+)$/gmi);

				if (!matchedExtension) {
					return res.status(400).send({
						error: true,
						displayMessage: "The file must have an extension",
						code: "INVALID_FILE"
					});
				}

				// Remove leading .
				let extension = matchedExtension[0].substr(1).toLowerCase();

				if (!validExtensions.includes(extension)) {
					return res.status(400).send({
						error: true,
						displayMessage: "Invalid file format",
						code: "INVALID_FILE",
						supportedExtensions: validExtensions
					});
				}

				let originalFileName = file.originalname.substr(0,
					file.originalname.length - (extension.length + 1));
				originalFileName = originalFileName.substr(0, 16);

				// If the original filename is more than 16, the string has been trimmed
				if (file.originalname.length > 16)
					originalFileName += "...";

				// Write the file to the UserFiles directory and add to the database
				let filename = `${newFilename}.${extension}`;
				await fs.writeFile(`./UserFiles/${filename}`, file.buffer);
				let flashcardMedia = await flashcardMediaRepository
					.addFlashcardMediaAsync(mediaTypes[extension],
						filename, file.size, flashcard.flashcardId, originalFileName);
				flashcard.media.push(flashcardMedia);
			}

			if (mediaToDelete) {
				// If there are media to delete, loop through the IDs and remove them
				for (let mediaId of mediaToDelete) {
					await flashcardMediaRepository.deleteFlashcardMediaAsync(mediaId);
					let flashcardMediaIndex = flashcard.media.findIndex(x => x.flashcardMediaId === mediaId);

					/*
						If the element was not found, an invalid ID was provided,
						so just continue to the next element
					 */
					if (flashcardMediaIndex === -1)
						continue;

					flashcard.media.splice(flashcardMediaIndex, 1);
				}
			}

			// Set the new updated fields
			flashcard.body = body;
			flashcard.subjectId = subject;
			flashcard.confidenceLevel = confidenceLevelValues[confidenceLevel];
			flashcard.updatedAt = new Date().getTime();

			/*
				Clone the flashcard object so that any changes made
				inside the updateFlashcardAsync function is not made
				on the flashcard object returned in the response
			 */
			let clonedFlashcard = Object.assign({}, flashcard);
			await flashcardRepository.updateFlashcardAsync(clonedFlashcard);

			// Return 200 OK with the updated flashcard
			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				flashcard
			});
		} catch (err) {
			// Try catch statement to catch any errors (exception handling/error trapping)
			return next(err);
		}
	});
});

module.exports = router;
