/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();

// Repositories
const questionRepository = require("../../Repositories/QuestionRepository");
const questionAnswerRepository = require("../../Repositories/QuestionAnswerRepository");
const userSubjectsRepository = require("../../Repositories/UserSubjectsRepository");

// Helpers
const validationHelper = require("../../Helpers/ValidationHelper");

router.post("/", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		// Destructure the fields provided in the body object
		let {
			title,
			subject,
			answers
		} = req.body;

		if (!validationHelper.presenceCheck(title)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Title cannot be blank",
				code: "MISSING_PARAMETER"
			});
		}

		if (!validationHelper.lessThanLengthCheck(title, 128)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Title cannot be longer than 128 characters",
				code: "INVALID_PARAMETER"
			});
		}

		if (!Array.isArray(answers)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Answers must be an array of objects with \"text\" and \"correct\"",
				code: "INVALID_PARAMETER"
			});
		}

		if (answers.length > 10) {
			return res.status(400).send({
				error: true,
				displayMessage: "You may only have up to 10 answers per question",
				code: "INVALID_PARAMETER"
			});
		}

		let correctAnswers = 0;
		for (const answer of answers) {
			if (!validationHelper.presenceCheck(answer.text)) {
				res.status(400).send({
					error: true,
					displayMessage: "Answer cannot be blank",
					code: "MISSING_PARAMETER"
				});
			}

			if (answer.correct)
				correctAnswers += 1;
		}

		if (correctAnswers !== 1) {
			return res.status(400).send({
				error: true,
				displayMessage: "You must mark exactly 1 question as correct",
				code: "INVALID_PARAMETER"
			});
		}

		// Get the subject by the ID provided to check its valid
		let validSubject = await userSubjectsRepository.getUserSubjectByIdAsync(subject);

		if (!validSubject) {
			return res.status(400).send({
				error: true,
				displayMessage: "Invalid subject ID provided",
				code: "INVALID_PARAMETER"
			});
		}

		// Creates the new question in the db
		let question = await questionRepository.addQuestionAsync(title, subject, answers,
			req.session.uuid);

		// Return a 200 OK with the question object
		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			question
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.delete("/:id", async (req, res, next) => {
	try {
		/**
		 * @type {Question}
		 */
		let question = await questionRepository.getQuestionByIdAsync(req.params.id);

		if (!question) {
			return res.status(400).send({
				error: true,
				displayMessage: "A question with that ID could not be found",
				code: "QUESTION_NOT_FOUND"
			});
		}

		try {
			// Delete the question by its ID
			await questionRepository.deleteQuestionByIdAsync(question.questionId);
		} catch (err) {
			// Foreign key constraint
			if (err.errno === 1451) {
				return res.status(400).send({
					error: true,
					displayMessage: "This question is currently being used in a test and " +
						"therefore cannot be deleted",
					code: "USED_IN_TEST"
				});
			}
		}

		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			question
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.patch("/:id", async (req, res, next) => {
	try {
		// Fetch the question based on the ID provided in the URL
		/**
		 * @type {Question}
		 */
		let question = await questionRepository.getQuestionByIdAsync(req.params.id);

		if (!question) {
			return res.status(400).send({
				error: true,
				displayMessage: "A question with that ID could not be found",
				code: "QUESTION_NOT_FOUND"
			});
		}

		// Destructure each property provided by the user
		let {
			answers,
			subject
		} = req.body;

		// Validate user input
		if (!Array.isArray(answers)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Answers must be an array of objects with \"text\" and \"correct\"",
				code: "INVALID_PARAMETER"
			});
		}

		// Check the new answer count is valid
		if (answers.length < 2) {
			return res.status(400).send({
				error: true,
				displayMessage: "You must create at least 2 answers",
				code: "INVALID_PARAMETER"
			});
		}

		if (answers.length > 10) {
			return res.status(400).send({
				error: true,
				displayMessage: "You may only have up to 10 answers per question",
				code: "INVALID_PARAMETER"
			});
		}

		// Get the number of correct answers based on the new answers provided
		let correctAnswers = answers.filter(x => x.correct).length;

		// If there isn't 1 question marked as correct
		if (correctAnswers !== 1) {
			return res.status(400).send({
				error: true,
				displayMessage: "You must mark exactly 1 question as correct",
				code: "INVALID_PARAMETER"
			});
		}

		// Delete the existing answers before creating the newly provided ones
		await questionAnswerRepository.deleteAnswersByQuestionIdAsync(question.questionId);
		question.answers = [];

		// First validate all the answers before adding to the db
		for (const answer of answers) {
			// Validate text
			if (!validationHelper.presenceCheck(answer.text)) {
				res.status(400).send({
					error: true,
					displayMessage: "Answer cannot be blank",
					code: "MISSING_PARAMETER"
				});
			}
		}

		// Loop through each answer provided and create it
		for (const answer of answers) {
			// Add the question answer to the db
			let questionAnswer = await questionAnswerRepository.addAnswerAsync(answer.text,
				answer.correct, question.questionId);
			// Add the new answer to the answers array of the question
			question.answers.push(questionAnswer);
		}

		// Set the new updated fields
		question.subjectId = subject;

		/*
			Clone the question object so that any changes made
			inside the updateQuestionAsync function is not made
			on the question object returned in the response
		 */
		let clonedQuestion = Object.assign({}, question);
		await questionRepository.updateQuestionAsync(clonedQuestion);

		// Return 200 OK with the updated question
		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			question
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
