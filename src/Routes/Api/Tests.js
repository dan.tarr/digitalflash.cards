/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();
const moment = require("moment");

// Repositories
const questionRepository = require("../../Repositories/QuestionRepository");
const testRepository = require("../../Repositories/TestRepository");
const testQuestionRepository = require("../../Repositories/TestQuestionRepository");

// Helpers
const genericHelper = require("../../Helpers/GenericHelper");
const validationHelper = require("../../Helpers/ValidationHelper");

/**
 * @param {{
 *     weight: number,
 *     item: string
 * }[]} weights - The weighted values
 * @param {number} amount - The number to return
 * @param {string[]} selected - An array of selected IDs
 * @returns {string[]} - An array of selected question IDs
 */
function getRandomQuestions(weights, amount, selected) {
	// Get a new question ID
	let newId = genericHelper.getRandomWeightedItem(weights);

	// If the new ID does not already exist in the selected array, add it
	if (!selected.includes(newId))
		selected.push(newId);

	// If the number of selected items is the amount we want, return the selected items
	if (selected.length === amount)
		return selected;

	// Otherwise, call the function again to generate another
	return getRandomQuestions(weights, amount, selected);
}

router.post("/", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		/**
		 * @type {Question[]}
		 */
		let questions = await questionRepository.listUserQuestionsAsync(req.session.uuid);

		if (questions.length < 10) {
			return res.status(400).send({
				error: true,
				displayMessage: "You must have at least 10 questions before starting a test",
				code: "TOO_FEW_QUESTIONS"
			});
		}

		let weights = [];
		// Find the highest view count from the array of questions
		let maxViewCount = questions.reduce((prev, current) =>
			(prev.views > current.views) ? prev : current).views;
		// Find the latest creation date from the array of questions
		let latestCreatedAt = questions.reduce((prev, current) =>
			(prev.createdAt > current.createdAt) ? prev : current).createdAt;

		questions.forEach(question => {
			let weightedValue = 1;

			weightedValue += maxViewCount - question.views;
			/*
			 	Convert the created at from ms to days to prevent the weighted
			 	value being too high resulting in an insanely long loop
			 */
			weightedValue += Math.floor((latestCreatedAt - question.createdAt) / 86400000);

			/*
				Add the current questions's ID to the weighted object with the calculated weighted value
			 */
			weights.push({
				weight: weightedValue,
				item: question.questionId
			});
		});

		// Recursive function to get a number of random questions, ensuring unique IDs are selected
		let selectedIds = getRandomQuestions(weights, 10, []);

		// Get the full question objects from the array of IDs
		let selectedQuestions = selectedIds.map(id => {
			return questions.find(x => x.questionId === id);
		});

		// Loop through each of the selected questions and increase the view count by 1
		let updatePromises = [];
		selectedQuestions.forEach(question => {
			let clonedQuestion = Object.assign({}, question);
			clonedQuestion.views += 1;
			updatePromises.push(questionRepository.updateQuestionAsync(clonedQuestion));
		});

		// Update all the questions at once
		await Promise.all(updatePromises);

		selectedQuestions.forEach(question => {
			question.answers.forEach(answer => {
				// Remove the correctAnswer field from each of the questions so the user can't cheat
				delete answer.correctAnswer;
			});
		});

		let test = await testRepository.addTestAsync(req.session.uuid, selectedQuestions);

		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			test,
			questions: selectedQuestions
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.post("/mark", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		let {
			testId,
			questions
		} = req.body;

		if (!validationHelper.presenceCheck(testId)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Test ID cannot be empty",
				code: "MISSING_PARAMETER"
			});
		}

		if (!Array.isArray(questions)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Questions must be an array",
				code: "INVALID_PARAMETER"
			});
		}

		// Get the test by the test ID provided
		/**
		 * @type {Test}
		 */
		let test = await testRepository.getTestByIdAsync(testId);

		if (!test) {
			return res.status(400).send({
				error: true,
				displayMessage: "A test with that ID could not be found",
				code: "TEST_NOT_FOUND"
			});
		}

		let testUpdates = [];
		let correct = 0;
		let total = 0;
		questions.forEach(question => {
			// Find the test question with the question ID provided
			let testQuestion = test.questions.find(x => x.questionId === question.questionId);
			// Sets the correct answer ID
			question.correctAnswerId = testQuestion.questionAnswerId;

			// Indicates if the question was answered correctly
			question.correct = question.selectedAnswerId === testQuestion.questionAnswerId;

			// Add to the updates array to update the test question
			testUpdates.push(testQuestionRepository.updateTestAsync({
				testId: test.testId,
				testQuestionId: testQuestion.testQuestionId,
				questionId: question.questionId,
				answeredCorrectly: question.correct
			}));

			if (question.correct)
				correct += 1;

			total += 1;
		});

		// Mark the test as complete
		testUpdates.push(testRepository.updateTestAsync({
			testId: test.testId,
			userId: test.userId,
			takenAt: new Date().getTime(),
			complete: true
		}));

		await Promise.all(testUpdates);

		// Return 200 OK with the test results
		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			questions,
			stats: {
				correct,
				total
			}
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/history", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		// The ID of the test that tests should be returned before
		let {beforeThan} = req.query;
		let tests;

		// If the user provided an ID,
		if (beforeThan) {
			/**
			 * @type {Test}
			 */
			let test = await testRepository.getTestByIdAsync(beforeThan);

			if (!test) {
				return res.status(400).send({
					error: true,
					displayMessage: "A test with that ID could not be found",
					code: "TEST_NOT_FOUND"
				});
			}

			tests = await testRepository.listUserTestsAsync(req.session.uuid, test.takenAt);
		} else
			tests = await testRepository.listUserTestsAsync(req.session.uuid);

		let returnTests = [];

		tests.forEach(test => {
			// Calculate test scores
			let correct = test.questions.filter(x => x.answeredCorrectly === 1).length;
			let percentageScore = Math.round(correct / test.questions.length * 100);
			let takenAtDate = moment(test.takenAt).format("DD/MM/YYYY HH:MM");

			returnTests.push({
				takenAtDate,
				percentageScore
			});
		});

		// Return 200 OK with the tests
		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			tests: returnTests.reverse()
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
