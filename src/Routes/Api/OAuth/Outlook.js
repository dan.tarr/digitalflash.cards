/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();
const qs = require("querystring");

// Repositories
const outlookConnectionRepository = require("../../../Repositories/OutlookConnectionRepository");

// Helpers
const oauthHelper = require("../../../Helpers/OAuthHelper");
const outlookHelper = require("../../../Helpers/OutlookHelper");

// Variables
const clientId = process.env.OUTLOOK_OAUTH_CLIENT_ID;
const redirectUri = process.env.OUTLOOK_OAUTH_REDIRECT_URI;
const responseType = "code";
const scopes = [
	"https://graph.microsoft.com/.default",
	"offline_access"
];
const responseMode = "query";
const baseOAuthURL = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize";

router.get("/login", (req, res) => {
	/* eslint-disable camelcase */
	let queryString = qs.stringify({
		client_id: clientId,
		redirect_uri: redirectUri,
		response_type: responseType,
		scope: scopes.join(" "),
		response_mode: responseMode
	});
	/* eslint-enable camelcase */

	return res.redirect(`${baseOAuthURL}?${queryString}`);
});

router.get("/callback", async (req, res) => {
	try {
		// Destructures the code and error from the query parameter object
		let {code, error} = req.query;

		// If there was an error, or no code was provided, notify the user
		if (error || !code)
			return res.redirect("/profile?notify=Failed to connect Outlook account&notifyType=error");

		// The user must be logged in to connect their Outlook account
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		/**
		 * @type {OutlookAccessToken}
		 */
		let tokenData = await oauthHelper.getOutlookAccessTokenAsync(code);
		/**
		 * @type {OutlookUser}
		 */
		let userData = await outlookHelper.getUserInfoAsync(tokenData.accessToken);

		// Adds the outlook connection to the database
		await outlookConnectionRepository.addOutlookConnectionAsync(req.session.uuid, userData.id,
			userData.displayName, tokenData.accessToken, tokenData.refreshToken, tokenData.expiresIn);

		return res.redirect("/profile?notify=Successfully connected Outlook account");
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return res.redirect("/profile?notify=Something went wrong connecting your" +
			" Outlook account&notifyType=error");
	}
});

router.get("/disconnect", async (req, res) => {
	try {
		// The user must be logged in to disconnect their Outlook account
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		/**
		 * @type {OutlookConnection}
		 */
		let outlookConnection = await outlookConnectionRepository
			.getOutlookConnectionByUserIdAsync(req.session.uuid);

		if (!outlookConnection)
			return res.redirect("/profile");

		// Removes the outlook connection from the database
		await outlookConnectionRepository
			.deleteOutlookConnectionAsync(outlookConnection.outlookConnectionId);

		return res.redirect("/profile?notify=Successfully disconnected Outlook account");
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return res.redirect("/profile?notify=Something went wrong removing " +
			"your Outlook account&notifyType=error");
	}
});

module.exports = router;
