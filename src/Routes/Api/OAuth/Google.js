/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();
const qs = require("querystring");

// Repositories
const googleConnectionRepository = require("../../../Repositories/GoogleConnectionRepository");

// Helpers
const oauthHelper = require("../../../Helpers/OAuthHelper");
const googleHelper = require("../../../Helpers/GoogleHelper");

// Variables
const clientId = process.env.GOOGLE_OAUTH_CLIENT_ID;
const redirectUri = process.env.GOOGLE_OAUTH_REDIRECT_URI;
const responseType = "code";
const scopes = [
	"https://www.googleapis.com/auth/calendar",
	"https://www.googleapis.com/auth/calendar.events",
	"https://www.googleapis.com/auth/calendar.events.readonly",
	"https://www.googleapis.com/auth/calendar.readonly",
	"https://www.googleapis.com/auth/userinfo.email",
	"https://www.googleapis.com/auth/userinfo.profile",
	"openid"
];
const accessType = "offline";
const baseOAuthURL = "https://accounts.google.com/o/oauth2/v2/auth";

router.get("/login", (req, res) => {
	// Return action is an action name that should occur when the user is returned here
	let {returnAction} = req.query;

	/* eslint-disable camelcase */
	let queryString = qs.stringify({
		client_id: clientId,
		redirect_uri: redirectUri,
		response_type: responseType,
		scope: scopes.join(" "),
		access_type: accessType,
		state: returnAction
	});
	/* eslint-enable camelcase */

	return res.redirect(`${baseOAuthURL}?${queryString}`);
});

router.get("/callback", async (req, res) => {
	try {
		// Destructure the code, state and error from the query parameter object
		let {code, state, error} = req.query;

		// If there was an error, or there was no code, redirect to profile page
		if (error || !code)
			return res.redirect("/profile?notify=Failed to connect Google account&notifyType=error");

		// The user must be logged in to link their Google account
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		// Convert the code into an access token
		/**
		 * @type {GoogleAccessToken}
		 */
		let tokenData = await oauthHelper.getGoogleAccessTokenAsync(code);
		// Clone the scopes array
		let requiredScopes = scopes.slice(0);
		let authorizedScopes = tokenData.scope.split(" ");

		authorizedScopes.forEach(scope => {
			requiredScopes.splice(requiredScopes.indexOf(scope), 1);
		});

		// Check all the requested scopes were approved by the user
		if (requiredScopes.length > 0) {
			return res.redirect("/profile?notify=You did not authorize all " +
				"the required Google scopes&notifyType=error");
		}

		/**
		 * @type {GoogleUser}
		 */
		let userData = await googleHelper.getUserInfoAsync(tokenData.accessToken);

		// Adds the new google connection to the database for the user
		await googleConnectionRepository.addGoogleConnectionAsync(req.session.uuid, userData.id,
			userData.email, tokenData.accessToken, tokenData.refreshToken, tokenData.expiresIn);

		/*
			If the state returned is an action to export the google calendar,
			redirect to revision timetable page with the same action
		 */
		if (state === "exportGoogleCalendar")
			return res.redirect("/revisiontimetable?action=exportGoogleCalendar");

		return res.redirect("/profile?notify=Successfully connected Google account");
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return res.redirect("/profile?notify=Something went wrong connecting your" +
			" Google account&notifyType=error");
	}
});

router.get("/disconnect", async (req, res) => {
	try {
		// The user must be logged in to disconnect their Google account
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		/**
		 * @type {GoogleConnection}
		 */
		let googleConnection = await googleConnectionRepository
			.getGoogleConnectionByUserIdAsync(req.session.uuid);

		if (!googleConnection)
			return res.redirect("/profile");

		await googleConnectionRepository
			.deleteGoogleConnectionAsync(googleConnection.googleConnectionId);
		googleHelper.revokeTokenAsync(googleConnection.accessToken).catch(() => {});

		// Notify the user that the disconnection was successful
		return res.redirect("/profile?notify=Successfully disconnected Google account");
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		// Catch any errors that occure and notify the user
		return res.redirect("/profile?notify=Something went wrong removing " +
			"your Google account&notifyType=error");
	}
});

module.exports = router;
