/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();

// Repositories
const userRepository = require("../../Repositories/UserRepository");
const defaultSubjectRepository = require("../../Repositories/DefaultSubjectRepository");
const userSubjectsRepository = require("../../Repositories/UserSubjectsRepository");
const flashcardRepository = require("../../Repositories/FlashcardRepository");
const googleConnectionRepository = require("../../Repositories/GoogleConnectionRepository");
const outlookConnectionRepository = require("../../Repositories/OutlookConnectionRepository");
const questionRepository = require("../../Repositories/QuestionRepository");
const testRepository = require("../../Repositories/TestRepository");
const revisionTimetableRepository = require("../../Repositories/RevisionTimetableRepository");

// Helpers
const jwtHelper = require("../../Helpers/JwtHelper");
const emailHelper = require("../../Helpers/EmailHelper");
const validationHelper = require("../../Helpers/ValidationHelper");
const authenticationHelper = require("../../Helpers/AuthenticationHelper");

router.post("/setup", async (req, res, next) => {
	try {
		// The user must require account setup in order to use this endpoint
		if (req.session.authState !== "REQUIRES_ACCOUNT_SETUP") {
			return res.status(400).send({
				error: true,
				displayMessage: "Your account does not require setup",
				code: "INVALID_AUTH_STATE"
			});
		}

		// Create a variable for the data provided by the user in the request body
		let selectedDefaultSubjectIds = req.body.defaultSubjectIds;

		// Check that the provided data is an array
		if (!Array.isArray(selectedDefaultSubjectIds)) {
			return res.status(400).send({
				error: true,
				displayMessage: "defaultSubjectIds must be an array of strings",
				code: "INVALID_PARAMETER"
			});
		}

		// Check the provided array is of the minimum length of 3
		if (selectedDefaultSubjectIds.length < 3) {
			return res.status(400).send({
				error: true,
				displayMessage: "You must choose at least 3 subjects",
				code: "INVALID_PARAMETER"
			});
		}

		/**
		 * @type {DefaultSubject[]}
		 */
		let defaultSubjects = await defaultSubjectRepository.listDefaultSubjects();
		// Returns an array of valid IDs from the default subject list
		let defaultSubjectIds = defaultSubjects.map(subject => {
			return subject.defaultSubjectId;
		});

		let invalidIds = [];
		let selectedSubjects = [];

		// Loop the provided IDs and check that they are valid IDs
		selectedDefaultSubjectIds.forEach(id => {
			if (!defaultSubjectIds.includes(id))
				invalidIds.push(id);
			else
				selectedSubjects.push(defaultSubjects.find(x => x.defaultSubjectId === id));
		});

		// Return an error stating there were invalid ID(s) provided
		if (invalidIds.length > 0) {
			return res.status(400).send({
				error: true,
				displayMessage: `Default subjects with ID(s) ${invalidIds.join(", ")} could not be found`,
				code: "INVALID_PARAMETER"
			});
		}

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);
		user.finishedAccountSetup = true;

		/*
			Add multiple user subjects from default subjects to the user
			Complete the account setup to prevent the user returning to this page in the future

			Promise.all returns an array of resolved promise values, so use array destructuring
			to retrieve the value of the first promise
		 */
		let [userSubjects] = await Promise.all([
			userSubjectsRepository.addManyUserSubjectsAsync(selectedSubjects,
				req.session.uuid),
			userRepository.updateUserAsync(user)
		]);

		// Generate a new JWT token with the updated authState
		let token = await jwtHelper.signAsync({
			uuid: req.session.uuid,
			authState: "LOGGED_IN"
		}, "24h");
		jwtHelper.setJwtCookie(res, token, "24h");

		// Return a 200 OK with the user subjects
		return res.status(200).send({
			error: false,
			code: "SUCCESS",
			userSubjects
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.post("/download", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		let date = new Date();
		date.setDate(date.getDate() - 14);

		// Check if the user has downloaded their data in the last 14 days
		if (user.lastDataDownload) {
			if (user.lastDataDownload > date.getTime()) {
				return res.status(400).send({
					error: true,
					displayMessage: "You have already requested a data download in the last 14 days",
					code: "DATA_DOWNLOAD_UNAVAILABLE"
				});
			}
		}

		// Sets the users last download date to now
		user.lastDataDownload = new Date().getTime();
		await userRepository.updateUserAsync(user);

		// Fetch user data
		let [
			flashcards,
			userSubjects,
			questions,
			tests,
			googleConnection,
			outlookConnection,
			revisionTimetable
		] = await Promise.all([
			flashcardRepository.listUserFlashcardsAsync(user.userId),
			userSubjectsRepository.listUserSubjectsAsync(user.userId),
			questionRepository.listUserQuestionsAsync(user.userId),
			testRepository.listUserTestsAsync(user.userId),
			googleConnectionRepository.getGoogleConnectionByUserIdAsync(user.userId),
			outlookConnectionRepository.getOutlookConnectionByUserIdAsync(user.userId),
			revisionTimetableRepository.getRevisionTimetableByUserIdAsync(user.userId)
		]);

		// Remove sensitive info
		delete user.password;
		delete user.twoFactorSecret;
		delete user.twoFactorBackupCodes;

		if (googleConnection) {
			delete googleConnection.accessToken;
			delete googleConnection.refreshToken;
		}

		if (outlookConnection) {
			delete outlookConnection.accessToken;
			delete outlookConnection.refreshToken;
		}

		// If the user has a revision timetable, parse it
		if (revisionTimetable)
			revisionTimetable.timetableJson = JSON.parse(revisionTimetable.timetableJson);

		let userData = {
			user,
			userSubjects,
			flashcards,
			questions,
			tests,
			googleConnection,
			outlookConnection,
			revisionTimetable
		};

		// Send an email to the user with their data download
		await emailHelper.sendDataDownloadEmailAsync(user.email, JSON.stringify(userData));

		return res.status(200).send({
			error: false,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.delete("/", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		// Destructure the password from the body object
		let {password} = req.body;

		// Check a password was provided
		if (!validationHelper.presenceCheck(password)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Password cannot be blank",
				code: "MISSING_PARAMETER"
			});
		}

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		// Check if the provided password is correct
		let correctPassword = await authenticationHelper
			.compareBcryptPasswordAsync(password, user.password);

		// Inform the user if the password is incorrect
		if (!correctPassword) {
			return res.status(401).send({
				error: true,
				displayMessage: "Your password is incorrect",
				code: "INVALID_CREDENTIALS"
			});
		}

		/**
		 * @type {Flashcard[]}
		 */
		let flashcards = await flashcardRepository.listUserFlashcardsAsync(user.userId);

		// Delete all the flashcards
		await Promise.all(flashcards.map(x => {
			return flashcardRepository.deleteFlashcardByIdAsync(x.flashcardId);
		}));

		/**
		 * @type {Test[]}
		 */
		let tests = await testRepository.listAllUserTestsAsync(user.userId);

		// Delete all the tests
		await Promise.all(tests.map(x => {
			return testRepository.deleteTestByIdAsync(x.testId);
		}));

		/**
		 * @type {Question[]}
		 */
		let questions = await questionRepository.listUserQuestionsAsync(user.userId);

		// Delete all the questions
		await Promise.all(questions.map(x => {
			return questionRepository.deleteQuestionByIdAsync(x.questionId);
		}));

		/**
		 * @type {UserSubject[]}
		 */
		let userSubjects = await userSubjectsRepository.listUserSubjectsAsync(user.userId);

		// Delete all the userSubjects
		await Promise.all(userSubjects.map(x => {
			return userSubjectsRepository.deleteUserSubjectByIdAsync(x.userSubjectId);
		}));

		/**
		 * @type {GoogleConnection}
		 */
		let googleConnection = await googleConnectionRepository
			.getGoogleConnectionByUserIdAsync(user.userId);
		/**
		 * @type {OutlookConnection}
		 */
		let outlookConnection = await outlookConnectionRepository
			.getOutlookConnectionByUserIdAsync(user.userId);

		if (googleConnection) {
			await googleConnectionRepository
				.deleteGoogleConnectionAsync(googleConnection.googleConnectionId);
		}

		if (outlookConnection) {
			await outlookConnectionRepository
				.deleteOutlookConnectionAsync(outlookConnection.outlookConnectionId);
		}

		/**
		 * @type {RevisionTimetable}
		 */
		let revisionTimetable = await revisionTimetableRepository
			.getRevisionTimetableByUserIdAsync(user.userId);

		if (revisionTimetable) {
			await revisionTimetableRepository
				.deleteRevisionTimetableAsync(revisionTimetable.revisionTimetableId);
		}

		await userRepository.deleteUserByIdAsync(user.userId);
		await emailHelper.sendAccountDeletionEmailAsync(user.email, user.username);

		// Once all the data has been deleted, log the user out
		jwtHelper.removeJwtCookie(res);

		return res.status(200).send({
			error: false,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
