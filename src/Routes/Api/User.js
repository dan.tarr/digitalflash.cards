/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();

// Repositories
const userRepository = require("../../Repositories/UserRepository");

// Helpers
const validationHelper = require("../../Helpers/ValidationHelper");
const authenticationHelper = require("../../Helpers/AuthenticationHelper");

router.post("/changepassword", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		let {
			oldPassword,
			newPassword,
			confirmPassword
		} = req.body;

		if (!validationHelper.presenceCheck(oldPassword)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Old password cannot be blank",
				code: "MISSING_PARAMETER"
			});
		}

		if (!validationHelper.presenceCheck(newPassword)) {
			return res.status(400).send({
				error: true,
				displayMessage: "New password cannot be blank",
				code: "MISSING_PARAMETER"
			});
		}

		if (!validationHelper.presenceCheck(confirmPassword)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Confirm password cannot be blank",
				code: "MISSING_PARAMETER"
			});
		}

		// Fetch the user object based on their session
		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		// Check the oldPassword provided is correct
		let correctPassword = await authenticationHelper
			.compareBcryptPasswordAsync(oldPassword, user.password);

		if (!correctPassword) {
			return res.status(400).send({
				error: true,
				displayMessage: "Your old password is incorrect",
				code: "INVALID_CREDENTIALS"
			});
		}

		if (!authenticationHelper.validatePassword(newPassword)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Your new password doesn't meet security requirements",
				code: "INVALID_PARAMETER"
			});
		}

		if (newPassword !== confirmPassword) {
			return res.status(400).send({
				error: true,
				displayMessage: "Passwords do not match",
				code: "INVALID_PARAMETER"
			});
		}

		user.password = await authenticationHelper.hashPasswordAsync(newPassword);
		await userRepository.updateUserAsync(user);

		return res.status(200).send({
			error: false,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.put("/username", async (req, res, next) => {
	try {
		// Check that the user is logged in
		if (req.session.authState !== "LOGGED_IN") {
			return res.status(401).send({
				error: true,
				displayMessage: "You must be logged in to use this endpoint",
				code: "INVALID_AUTH_STATE"
			});
		}

		let {username} = req.body;

		if (!validationHelper.presenceCheck(username)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Username cannot be blank",
				code: "MISSING_PARAMETER"
			});
		}

		if (!validationHelper.lessThanLengthCheck(username, 32)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Username must be less than 32 characters",
				code: "INVALID_PARAMETER"
			});
		}

		if (!/^[A-Za-z][A-Za-z0-9]*$/.test(username)) {
			return res.status(400).send({
				error: true,
				displayMessage: "Username must only contain English alphanumeric characters",
				code: "INVALID_PARAMETER"
			});
		}

		let existingUser = await userRepository.getUserByUsernameAsync(username);

		if (existingUser) {
			return res.status(400).send({
				error: true,
				displayMessage: "A user with that username already exists",
				code: "USERNAME_TAKEN"
			});
		}

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);
		user.username = username;
		await userRepository.updateUserAsync(user);

		return res.status(200).send({
			error: false,
			code: "SUCCESS"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
