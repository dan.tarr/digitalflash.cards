/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();

// Repositories
const userRepository = require("../Repositories/UserRepository");
const googleConnectionRepository = require("../Repositories/GoogleConnectionRepository");
const outlookConnectionRepository = require("../Repositories/OutlookConnectionRepository");

// Variables
const environment = process.env.ENVIRONMENT;

router.get("/", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		let [user, googleConnection, outlookConnection] = await Promise.all([
			userRepository.getUserByUuidAsync(req.session.uuid),
			googleConnectionRepository.getGoogleConnectionByUserIdAsync(req.session.uuid),
			outlookConnectionRepository.getOutlookConnectionByUserIdAsync(req.session.uuid)
		]);

		return res.render("Profile/Profile", {
			title: "Profile Settings",
			environment,
			page: "profile",
			user,
			googleConnection,
			outlookConnection
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/security", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		return res.render("Profile/Security", {
			title: "Security Settings",
			environment,
			page: "security",
			user
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/data", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		return res.render("Profile/Data", {
			title: "Data Settings",
			environment,
			page: "data"
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
