/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const express = require("express");
const router = express.Router();

// Repositories
const userRepository = require("../Repositories/UserRepository");
const flashcardRepository = require("../Repositories/FlashcardRepository");
const questionRepository = require("../Repositories/QuestionRepository");
const userSubjectsRepository = require("../Repositories/UserSubjectsRepository");
const testRepository = require("../Repositories/TestRepository");

// Helpers
const testHelper = require("../Helpers/TestHelper");

// Variables
const environment = process.env.ENVIRONMENT;
const reCaptchaSiteKey = process.env.RECAPTCHA_SITE;

router.get("/", (req, res) => {
	return res.render("Landing", {
		title: "digitalflash.cards | Home",
		environment
	});
});

router.get("/login", async (req, res) => {
	if (req.session.authState === "LOGGED_IN")
		return res.redirect("/home");
	else if (req.session.authState === "PENDING_EMAIL_VERIFICATION")
		return res.redirect("/emailverification");

	return res.render("Login", {
		title: "digitalflash.cards | Login",
		environment,
		reCaptchaSiteKey
	});
});

router.get("/signup", async (req, res) => {
	if (req.session.authState === "LOGGED_IN")
		return res.redirect("/home");
	else if (req.session.authState === "PENDING_EMAIL_VERIFICATION")
		return res.redirect("/emailverification");

	return res.render("SignUp", {
		title: "digitalflash.cards | Sign Up",
		environment,
		reCaptchaSiteKey
	});
});

router.get("/emailverification", async (req, res, next) => {
	try {
		if (req.session.authState !== "PENDING_EMAIL_VERIFICATION")
			return res.redirect("/signup");

		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		return res.render("EmailVerification", {
			title: "digitalflash.cards | Email Verification",
			environment,
			user
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/accountsetup", async (req, res, next) => {
	try {
		// The user is only able to access this page if their account requires setup
		if (req.session.authState !== "REQUIRES_ACCOUNT_SETUP")
			return res.redirect("/home");

		return res.render("AccountSetup", {
			title: "digitalflash.cards | Account Setup",
			environment
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/home", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		/**
		 * @type {User}
		 */
		let user = await userRepository.getUserByUuidAsync(req.session.uuid);

		/**
		 * @type {Flashcard[]}
		 */
		let [flashcards, questions, subjects, tests] = await Promise.all([
			flashcardRepository.listUserFlashcardsAsync(user.userId),
			questionRepository.listUserQuestionsAsync(user.userId),
			userSubjectsRepository.listUserSubjectsAsync(user.userId),
			testRepository.listUserTestsAsync(user.userId)
		]);

		let highConfidenceFlashcards = flashcards.filter(x => x.confidenceLevel === 2).length;
		let mediumConfidenceFlashcards = flashcards.filter(x => x.confidenceLevel === 1).length;
		let lowConfidenceFlashcards = flashcards.filter(x => x.confidenceLevel === 0).length;

		let detailedTests = await Promise.all(tests.map(x => {
			return testRepository.getTestByIdAsync(x.testId);
		}));

		let strongestSubjects;
		let weakestSubjects;

		if (detailedTests.length > 0) {
			([strongestSubjects, weakestSubjects] = await testHelper
				.getWeakestStrongestSubjectsAsync(detailedTests));
		}

		// Add up the total percentage of all tests
		let totalPercent = 0;
		tests.forEach(test => {
			// Calculate test scores
			let correct = test.questions.filter(x => x.answeredCorrectly === 1).length;
			totalPercent += correct / test.questions.length * 100;
		});

		// Divide by the number of tests to get the average
		let averagePercentScore = Math.floor(totalPercent / tests.length);

		return res.render("Home", {
			title: "digitalflash.cards | Home",
			environment,
			user,
			flashcards,
			questions,
			subjects,
			tests,
			strongestSubjects,
			weakestSubjects,
			averagePercentScore,
			highConfidenceFlashcards,
			mediumConfidenceFlashcards,
			lowConfidenceFlashcards
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/revision", async (req, res, next) => {
	try {
		return res.render("Revision", {
			title: "digitalflash.cards | Revision",
			environment
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
