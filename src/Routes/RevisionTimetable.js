/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();

// Repositories
const revisionTimetableRepository = require("../Repositories/RevisionTimetableRepository");
const userSubjectsRepository = require("../Repositories/UserSubjectsRepository");
const googleConnectionRepository = require("../Repositories/GoogleConnectionRepository");
const outlookConnectionRepository = require("../Repositories/OutlookConnectionRepository");

// Helpers
const genericHelper = require("../Helpers/GenericHelper");

// Variables
const environment = process.env.ENVIRONMENT;

router.get("/", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		/**
		 * @type {RevisionTimetable}
		 */
		let revisionTimetable = await revisionTimetableRepository
			.getRevisionTimetableByUserIdAsync(req.session.uuid);

		// If the user doesn't have a revision timetable, redirect to the creation page
		if (!revisionTimetable)
			return res.redirect("/revisiontimetable/new");

		// Parse the stringified table as JSON
		let timetable = JSON.parse(revisionTimetable.timetableJson);

		for (let day of timetable) {
			for (let slot of day) {
				slot.subject = await userSubjectsRepository.getUserSubjectByIdAsync(slot.subject);
				slot.subject.textColour = genericHelper.pickTextColour(slot.subject.colour);
			}
		}

		let [googleConnection, outlookConnection] = await Promise.all([
			googleConnectionRepository.getGoogleConnectionByUserIdAsync(req.session.uuid),
			outlookConnectionRepository.getOutlookConnectionByUserIdAsync(req.session.uuid)
		]);

		return res.render("RevisionTimetable", {
			title: "digitalflash.cards",
			environment,
			timetable,
			googleConnection,
			outlookConnection
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/new", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		let revisionTimetable = await revisionTimetableRepository
			.getRevisionTimetableByUserIdAsync(req.session.uuid);

		// If the user already has a revision timetable, redirect to the overview page
		if (revisionTimetable)
			return res.redirect("/revisiontimetable");

		return res.render("RevisionTimetable/New", {
			title: "digitalflash.cards | Create Revision Timetable",
			environment
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
