/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

const express = require("express");
const router = express.Router();
const moment = require("moment");

// Repositories
const testRepository = require("../Repositories/TestRepository");

// Helpers
const testHelper = require("../Helpers/TestHelper");

// Variables
const environment = process.env.ENVIRONMENT;

router.get("/", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		/**
		 * @type {Test[]}
		 */
		let tests = await testRepository.listUserTestsAsync(req.session.uuid);

		tests.forEach(test => {
			let correct = test.questions.filter(x => x.answeredCorrectly === 1).length;
			let percentageScore = Math.round(correct / test.questions.length * 100);

			test.takenAtRelative = moment(test.takenAt).fromNow();
			test.score = `${correct}/${test.questions.length}`;
			test.percentage = percentageScore;

			/*
				Determine the colour of the score text depending on percentage correct.
				Low scores in red, medium in yellow/orange and high in green.
			 */
			if (percentageScore < 50)
				test.scoreTextColour = "text-red";
			else if (percentageScore >= 50 && percentageScore < 75)
				test.scoreTextColour = "text-yellow";
			else if (percentageScore >= 75)
				test.scoreTextColour = "text-green";
		});

		return res.render("Tests/Tests", {
			title: "digitalflash.cards | Tests",
			environment,
			tests
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/new", (req, res) => {
	// User must be logged in to view this page
	if (req.session.authState !== "LOGGED_IN")
		return res.redirect("/");

	return res.render("Tests/New", {
		title: "digitalflash.cards | New Tests",
		environment
	});
});

router.get("/:id/stats", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		let {id} = req.params;

		/**
		 * @type {Test}
		 */
		let test = await testRepository.getTestByIdAsync(id);
		// Object to store the test statistics that have been calculated
		let testStatistics = {};

		// Calculate test scores
		let correct = test.questions.filter(x => x.answeredCorrectly === 1).length;
		let percentageScore = Math.round(correct / test.questions.length * 100);

		// Add stats to object
		testStatistics.takenAtRelative = moment(test.takenAt).fromNow();
		testStatistics.score = `${correct}/${test.questions.length}`;
		testStatistics.percentage = percentageScore;

		/*
			Determine the colour of the score text depending on percentage correct.
			Low scores in red, medium in yellow/orange and high in green.
		 */
		if (percentageScore < 50)
			testStatistics.scoreTextColour = "text-red";
		else if (percentageScore >= 50 && percentageScore < 75)
			testStatistics.scoreTextColour = "text-yellow";
		else if (percentageScore >= 75)
			testStatistics.scoreTextColour = "text-green";

		// Get the strongest and weakest subjects
		let [strongestSubjects, weakestSubjects] = await testHelper
			.getWeakestStrongestSubjectsAsync([test]);

		testStatistics.strongestSubjects = strongestSubjects;
		testStatistics.weakestSubjects = weakestSubjects;

		return res.render("Tests/TestStats", {
			title: "digitalflash.cards | Test Statistics",
			environment,
			test,
			testStatistics
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
