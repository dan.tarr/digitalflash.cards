/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const express = require("express");
const router = express.Router();
const moment = require("moment");

// Repositories
const userSubjectsRepository = require("../Repositories/UserSubjectsRepository");
const flashcardRepository = require("../Repositories/FlashcardRepository");

// Helpers
const genericHelper = require("../Helpers/GenericHelper");

// Variables
const environment = process.env.ENVIRONMENT;
const confidenceLevels = ["Low", "Medium", "High"];

/*
 	Splits an array into "chunks" of a certain number
 	Used to split an array into rows and columns
 */
/**
 * @param {[]} array - The array to chunk
 * @param {number} value - The number of values in each chunk
 * @return {[][]} - An array of arrays
 */
function chunkArray(array, value) {
	return Array.from(Array(Math.ceil(array.length / value)),
		(_, i) => array.slice(i * value, i * value + value));
}

router.get("/", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		// List the users flashcards from the db
		/**
		 * @type {Flashcard[]}
		 */
		let flashcards = await flashcardRepository.listUserFlashcardsAsync(req.session.uuid);

		flashcards.forEach(flashcard => {
			flashcard.createdAt = moment(flashcard.createdAt);
			flashcard.updatedAt = moment(flashcard.updatedAt);
		});

		// Chunk the array into chunks of 3, for 3 cards per row
		let flashcardRows = chunkArray(flashcards, 3);

		return res.render("Flashcards", {
			title: "digitalflash.cards | Flashcard Management",
			environment,
			flashcardRows,
			confidenceLevels
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/new", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		let userSubjects = await userSubjectsRepository.listUserSubjectsAsync(req.session.uuid);

		return res.render("Flashcards/New", {
			title: "digitalflash.cards | New Flashcard",
			environment,
			userSubjects
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

router.get("/:id/edit", async (req, res, next) => {
	try {
		// User must be logged in to view this page
		if (req.session.authState !== "LOGGED_IN")
			return res.redirect("/");

		// Fetch the current flashcard and users subjects
		let [flashcard, userSubjects] = await Promise.all([
			flashcardRepository.getFlashcardByIdAsync(req.params.id),
			userSubjectsRepository.listUserSubjectsAsync(req.session.uuid)
		]);

		// The flashcard ID wasn't found, show the 404 page
		if (!flashcard)
			return next();

		return res.render("Flashcards/Edit", {
			title: "digitalflash.cards | Edit Flashcard",
			environment,
			flashcard,
			userSubjects,
			formatBytes: genericHelper.formatBytes
		});
	} catch (err) {
		// Try catch statement to catch any errors (exception handling/error trapping)
		return next(err);
	}
});

module.exports = router;
