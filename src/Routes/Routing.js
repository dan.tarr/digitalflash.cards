/*
 * Copyright (c) 2021 digitalflash.cards
 * By Dan Tarr
 * All rights reserved
 */

// Modules
const express = require("express");
const router = express.Router();
const {sep, resolve} = require("path");
const {readdir} = require("fs").promises;

// Variables
const environment = process.env.ENVIRONMENT;

async function registerRoutes(dir) {
	let directoryFiles = await readdir(dir, {withFileTypes: true});
	await Promise.all(directoryFiles.map((file) => {
		// Get the full file path of the current file
		let fullFilePath = resolve(dir, file.name);

		// If the current path is a directory, call the function again to enter the directory
		if (file.isDirectory())
			return registerRoutes(fullFilePath);

		let routePath = fullFilePath.replace(__dirname, "").split(sep).join("/");

		// Ensure the current file is not this Routing.js file
		if (fullFilePath !== __filename) {
			let routePaths = routePath.split("/");
			let fileName = routePaths.pop();
			let fileNameWithoutExtension = fileName.substr(0, fileName.lastIndexOf("."));
			let baseUrl = routePaths.join("/");

			if (fileNameWithoutExtension !== "Index")
				baseUrl += "/" + fileNameWithoutExtension;

			// Register the endpoints
			router.use(baseUrl, require("." + routePath));
		}

		return null;
	}));

	router.use(function (req, res) {
		res.status(404);

		if (req.url.startsWith("/api/")) {
			return res.send({
				error: true,
				displayMessage: "Endpoint not found",
				code: "NOT_FOUND"
			});
		}

		let filename = "Error";

		if (req.session.authState === "LOGGED_IN")
			filename = "LoggedInError";

		return res.render(filename, {
			title: "digitalflash.cards | Not Found",
			code: 404,
			description: "Sorry, the page you are looking for can " +
				"not be found, go <a href='/home'>home</a>",
			environment
		});
	});

	router.use(
		/**
		 * @param {Error} err - The error thrown
		 * @param {Object} req - The request object
		 * @param {Object} res - The response object
		 * @param {function} next - The next function
		 * @returns {void}
		 */
		// eslint-disable-next-line no-unused-vars
		async (err, req, res, next) => {
			res.status(500);

			if (environment === "development")
				console.log(err);

			if (req.url.startsWith("/api/")) {
				return res.send({
					error: true,
					displayMessage: "Something went wrong on our end, try again later",
					code: "INTERNAL_SERVER_ERROR"
				});
			}

			return res.render("Error", {
				title: "pays.host | Internal Server Error",
				environment: environment,
				code: 500,
				description: "Uh oh, something went wrong on our end. Please try again later.",
				stack: err.stack
			});
		});
}

(async () => {
	await registerRoutes("./src/Routes");
})();

module.exports = router;
