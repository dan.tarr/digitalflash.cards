# digitalflash.cards

[![wakatime](https://wakatime.com/badge/user/134d8831-a75e-46b4-80ff-d330f34e449a/project/2d4fc6b7-d086-4d62-9660-31f088f02faa.svg)](https://wakatime.com/badge/user/134d8831-a75e-46b4-80ff-d330f34e449a/project/2d4fc6b7-d086-4d62-9660-31f088f02faa)

A-Level Computer Science coursework website based around digital flashcards.

## The Project

For my A-Level Computer Science course, I had multiple units I had to complete. This project
was the Unit 5 section where I had to plan, design, create and evaluate a system on behalf of
a client. This coursework, along with other written exams earnt me an A*.

You can find the written sections within the [Documents directory](Documents).

## Reasoning

Most people use physical paper based flashcards currently as their main method of revision.
This project aims to be a computational alternative to this method of revision. Due to the
limited amount of time to complete this project, and the hundreds of hundreds of pages of
written documents required, the functionality of the site is less than would be desired for
a full production site. The site contains general functionality that could be used to revise,
however, may lack some features that would be desirable to properly revise to the best of
your ability. More details regarding the project's specifics can be found in the
[Discussion Document](Documents/Discussion.docx).

## The Stack

This project was created using the [NodeJS](https://nodejs.org/) (v14) as the backend and API
using [Express.js](https://github.com/expressjs/express). The frontend is served by the same
Express backend, using [Vash](https://github.com/kirbysayshi/vash) as the templating engine.
At the time of creating this, I had not worked with React, otherwise I would have used React
as the frontend framework for this application.

MySQL 5.7 is the database used for data storage. Google reCAPTCHA is used to protect user
login and registration from spam/botting. JSON Web Tokens (JWTs) are used for user authentication
so that the API is completely stateless, which allows for scaling up to multiple
processes/machines if needed. 

## The Exam Board :neutral_face:

If you look through the code, you may see an exessive amount of pointless, self-explanatory
comments. Blame the exam board who made it a requirement to exessively comment your code
for no apparent reason (there are a few useful comments however). Additionally, if I had full
control over which technologies/methods I could use, I'd prefer to use an ORM, or a query
builder to manage the data fetching, saving and updating from the database, however, the exam
board required SQL queries to be written manually.

Since the exam board requires us to demonstate "sufficient evidence of processing", this project
also includes the ability to generate a revision timetable based on the subjects the user has
added. Again, due to the lack of time, this timetable is simply generated by randomly
placing subjects in it. No intelligent, personalised, or smart way to generating it.
To further meet this extremely vague requirement of "sufficient evidence", I had also added
the ability to export this generated timetable to either Google Calendar or Outlook Calendar.

## Gallery

A few images of the site and how it works.

![](docs/images/Landing.png)
![](docs/images/Homepage.png)
![](docs/images/Flashcards.png)
![](docs/images/Questions.png)

## Setup

Should you wish to set this project up for yourself, for whatever reason, the steps to take
can be found below.

### Requirements

- NodeJS (I used v14)
- MySQL 5.7 Database
- Email Account (That Email can be sent from)
- [Google OAuth Application](https://console.cloud.google.com/apis/credentials)
- [Outlook OAuth Application](https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationsListBlade)
- [Google reCAPTCHA Site](https://www.google.com/recaptcha/admin) (v2 Invisible)

Clone the example.env file into .env. Follow the steps below to gather and enter all
the required environment variables.

### Google OAuth Application

When creating the credentials on the Google Cloud Console, create an OAuth client ID, as shown
below.

![](docs/images/GoogleOAuth.png)

Set the Application type to `Web application`, giving it whatever name you wish and adding
your domain (or localhost) to the Authorized redirect URIs with the path
`/api/oauth/google/callback`. For example, `http://localhost/api/oauth/google/callback`.
This URI should be set as the `GOOGLE_OAUTH_REDIRECT_URI` environment variable.

You can then find the Client ID and Client Secret values, which can be put into the
`GOOGLE_OAUTH_CLIENT_ID` and `GOOGLE_OAUTH_CLIENT_SECRET` environment variables respectively.

![](docs/images/GoogleOAuthCreds.png)

### Outlook OAuth Application

Creating an OAauth Application for Outlook is slightly more complicated and more involved than
Google. First, you'll need an [Azure](https://azure.microsoft.com) account. Once created, you'll
want to go to the [Azure Portal](https://portal.azure) and search for `App registrations` in the
search bar at the top of the page.

![](docs/images/AzureAppRegistrationsSearch.png)

Once on this page, click "New registation" in the top left. The name can be whatever you like.
To support all Microsoft accounts, select the `Accounts in any organizational directory
(Any Azure AD directory - Multitenant) and personal Microsoft accounts (e.g. Skype, Xbox)`
option for "Supported account types". The Redirect URI should be of type `Web` and the URL
should be your domain (or localhost) with the path `/api/oauth/outlook/callback`.
For example, `http://localhost/api/oauth/outlook/callback`. This URI should be set as the
`OUTLOOK_OAUTH_REDIRECT_URI` environment variable.

The `OUTLOOK_OAUTH_CLIENT_ID` should be set to the `Application (client) ID` found under the
"Essentials" section on the application Overview page.

![](docs/images/OutlookOAuthClientID.png)

To generate the Client Secret, head to the `Certificates & secrets` tab on the left-hand side.
On this page, select the `Client Secrets` tab and click `New client secret`. Give the secret
whatever description you want and select when it should expire. While it would be best practice
to set this to the recommended value of 6 months, I set mine to a custom date very far in the
future. Once created, copy the Value column and set the `OUTLOOK_OAUTH_CLIENT_SECRET`
variable to that value.

### reCAPTCHA

Create a reCAPTCHA "site" on the
[reCAPTCHA Admin Console](https://www.google.com/recaptcha/admin/create). Set the label to
something that describes the app, or something to help you remember what it's for.
Set the reCAPTCHA type to `reCAPTCHA v2`, then `Invisible reCAPTCHA badge`.
Add your domain(s) that the site will be hosted on (localhost can be added) and click submit.
You will then be given a site key and a secret key. `RECAPTCHA_SITE` should be set to the
site key given, and `RECAPTCHA_SECRET` should be set to the secret key given.

## Deploy

Before deploying the app, ensure you have installed the dependencies using `npm install`. When
developing, you can run `npm run start_dev` to automatically restart the app when changes
are detected (using nodemon). In production, you can run `npm run start` to run the application.
In production, I recommend using a process manager such as [PM2](https://npmjs.com/package/pm2)
to manage the node process, ensuring it stays running in the event it crashes for whatever
reason.
